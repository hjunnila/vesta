TEMPLATE = subdirs
TARGET   = daemon

# Prevent Qt from attempting to strip the shell script files,
# which would produce a harmless but annoying error.
QMAKE_STRIP = echo

daemon.path   = /etc/init.d
daemon.files += vesta_pricepoller
INSTALLS += daemon

service.path   = /lib/systemd/system
service.files += vesta_pricepoller.service
#service.extra  = ln -sf /lib/systemd/system/vesta_pricepoller.service /etc/systemd/system/multi-user.target.wants
INSTALLS += service
