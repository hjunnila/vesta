TEMPLATE     = app
LANGUAGE     = C++
TARGET       = vesta_pricepoller
QT          += core dbus
QT          -= gui
CONFIG      += debug
CONFIG      -= app_bundle

OBJECTS_DIR  = obj

LIBS        += -L../../lib/src -lvesta
INCLUDEPATH += ../../lib/src

# Comment this line if you get whining about C++11
DEFINES += _GLIBCXX_USE_CXX11_ABI=0

SOURCES     += main.cpp

INSTALLS    += target
target.path  = /usr/bin
