#include <QtCore>
#include <signal.h>

#include "nordpool.h"
#include "elpricedb.h"
#include "elpricedb_adaptor.h"

void signal_handler(int sig)
{
    qDebug() << Q_FUNC_INFO << sig;
    QCoreApplication::exit();
}

int main(int argc, char** argv)
{
    signal(SIGINT, signal_handler);
    signal(SIGTERM, signal_handler);

    QCoreApplication app(argc, argv);
    QCoreApplication::setApplicationName("Vesta Electric Price Poller");
    QCoreApplication::setApplicationVersion("0.1");

    // Create the database object for storing data
    ElPriceDB* db = new ElPriceDB;
    if (!db) {
        qFatal("Unable to create an electric price database accessor instance!");
    }

    // Create the nordpool poller object
    NordPool* np = NordPool::createWithDefaultSettings();
    if (!np) {
        qFatal("Unable to create NordPool object!");
    }

    // Create the DBus adaptor for electric price update signalling.
    // The adaptor is deleted automagically by Qt because db is its parent.
    new ElpricedbAdaptor(db);
    QDBusConnection::systemBus().registerObject("/", db);

    // Make database listen to price data updates from nordpool
    QObject::connect(np, SIGNAL(priceDataUpdated(const PriceData&)), db, SLOT(onPriceDataUpdated(const PriceData&)));

    // Start polling for price data
    np->startPoll();

    // Start the main loop. Execution halts here until QCoreApplication::exit() is called.
    int result = app.exec();

    qDebug() << "Terminating process.";

    // Cleanup
    delete np;
    np = NULL;
    delete db;
    db = NULL;

    return result;
}
