TEMPLATE = subdirs
TARGET   = config

config.path   = /etc/vesta
config.files += nordpool.json
config.files += database.json
config.files += server.json
config.files += devicemanager.json

options.path   = /var/run
options.files += options.xml

INSTALLS += config options
