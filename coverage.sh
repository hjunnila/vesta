#!/bin/bash
#
# To measure unit test coverage, perform these steps:
# 0. export CCACHE_DISABLE=1        # (only if you use compiler cache)
# 1. qmake
# 2. make distclean
# 3. qmake CONFIG+=coverage
# 4. make
# 5. ./coverage.sh
#
# Human-readable HTML results are written under utcoverage/html.
#

ARCH=`uname`

#############################################################################
# Test directories to find coverage measurements from
#############################################################################

test[0]="lib/src"

# Number of tests
tlen=${#test[@]}

#############################################################################
# Functions
#############################################################################

# arg1:srcdir arg2:testname
function prepare {
    lcov -d ${1} -z
    lcov -d $1 -c -i -o utcoverage/${2}base.info
}

# arg1:srcdir arg2:testname
function gather_data {
    lcov -d ${1} -c -o utcoverage/${2}test.info
    lcov -a utcoverage/${2}base.info -a utcoverage/${2}test.info \
         -o utcoverage/${2}merge.info
}

#############################################################################
# Initialization
#############################################################################

# Check if lcov is installed
if [ -z `which lcov` ]; then
    echo "Unable to produce coverage results; can't find lcov."
fi

# Remove previous data
if [ -d utcoverage ]; then
    rm -rf utcoverage
fi

# Create directories for new coverage data
mkdir -p utcoverage/html

#############################################################################
# Preparation
#############################################################################

for ((i = 0; i < ${tlen}; i++))
do
    prepare ${test[i]} $i
done

#############################################################################
# Run unit tests
#############################################################################

VESTA_CONFIG=lib/test LD_LIBRARY_PATH=$LD_LIBRARY_PATH:lib/src lib/test/test
FAILED=$?
if [ ${FAILED} != 0 ]; then
    echo "Will not measure coverage because ${FAILED} unit tests failed."
    exit ${FAILED}
fi

#############################################################################
# Gather results
#############################################################################

for ((i = 0; i < ${tlen}; i++))
do
    gather_data ${test[i]} $i
done

#############################################################################
# All combined and HTMLized
#############################################################################

for ((i = 0; i < ${tlen}; i++))
do
    mergeargs="${mergeargs} -a utcoverage/${i}merge.info"
done

lcov ${mergeargs} -o utcoverage/coverage.info

# Remove stuff that isn't part of QLC sources
lcov -r utcoverage/coverage.info *.h -o utcoverage/coverage.info # Q_OBJECT etc.
lcov -r utcoverage/coverage.info moc_* -o utcoverage/coverage.info
lcov -r utcoverage/coverage.info *usr* -o utcoverage/coverage.info
lcov -r utcoverage/coverage.info *_test* -o utcoverage/coverage.info
lcov -r utcoverage/coverage.info ui_* -o utcoverage/coverage.info
lcov -r utcoverage/coverage.info *_adaptor.* -o utcoverage/coverage.info
lcov -r utcoverage/coverage.info *_interface.* -o utcoverage/coverage.info
lcov -r utcoverage/coverage.info *Library* -o utcoverage/coverage.info # OSX

# Generate HTML report
genhtml -o utcoverage/html utcoverage/coverage.info
