# Vesta

Vesta is the goddess of the hearth, home, and family in Roman religion. Therefore, it is
only suitable to name a home heating energy manager "Vesta".

Vesta downloads daily electricity rates from Nordpool, adds energy transfer price to the
total and compares, whether one kWh (kilowatt-hour) worth of heating is achieved cheaper
by using oil or electricity. Oil price needs to be input by the user according to the
cost of oil per litre in the tank currently.

Based on the input on whether oil or electricity is cheaper, Vesta triggers associated
Z-wave network switches on/off to enable/disable the use of electricity for heating. Oil
is never triggered off to prevent the system from disabling all methods of heating in case
of a malfunction with the electric heating elements. Then again, in case Vesta itself
fails to operate correctly (for example no price data available), electricity is turned
on as a fail-safe procedure; you can run out of oil but in theory you never run out of
electricity.

## Components

Vesta consists of several components and each of them does their specific duty:
* libvesta is the base library that provides all Vesta functionality
* vesta-pricepoller daemon downloads electricity prices from Nordpool daily and inserts them to the vesta database
* vesta daemon looks up price data from the vesta database, chooses between heating methods and commands Z-Wave nodes to switch on/off based on this information
* /etc/vesta contains all configuration files, including database passwords and other sensitive data, so make sure only vesta has access to these files

## Requirements

* Qt5 (core, network, sql and -dev)
* libopenzwave
* MySQL
* libstdc++
* g++

### Ubuntu package installation

Run the following command to install all available dependencies needed for compiling Vesta:
```
sudo apt-get install g++ make qt5-qmake qtchooser qtbase5-dev libqt5core5a libqt5network5a
sudo apt-get install libqt5sql5 libqt5sql5-mysql mysql-common mysql-client mysql-server
```

You might need to set the QT_SELECT environment variable to point to your installed Qt
version. At the time of writing the latest version is 5, so saying something like this
should work:
```
export QT_SELECT=qt5
```
To make the setting permanent, you should put that line to your ~/.profile file or
similar that gets executed when you start your terminal session.

You need to acquire libopenzwave from http://www.openzwave.com/downloads/
Download the latest stable releases (amd64 for 64bit or i386 for 32bit installation)
of the following packages from the xUbuntu section:
* openzwave-1.x.x
* libopenzwave1.x-dev
* libopenzwave1.x

Once you have downloaded the files, just install them with dpkg, something along the
lines of this, given that the files are in the current working directory:
```
sudo dpkg -i openzwave*.deb libopenzwave*.deb
```

## Compiling

1. Change directory to where you have cloned the vesta project from git.
2. Type "qmake" to generate intermediate Makefiles
3. Type "make" to compile the sources
4. Type "sudo make install" to install the compiled binaries and sample config files (this
   will overwrite anything you have previous edited in /etc/vesta so be careful!)

## Setup

Running "make install" in the root source folder (or config/) will always overwrite
existing files in /etc/vesta so be careful with it if you are editing the files in place.
Vesta configuration files are in JSON format, which is easily parsed by computers and
just as easy to read and edit for humans. There is also one options.xml file which contains
options for openzwave and in general do not necessarily need to be modified. If you know
what you're doing, you can edit this file, too, but it's not necessary and will not be
covered any further. Just make sure it's there.

See Configuration topic below for more info on the configuration files.

### MySQL

Vesta needs MySQL for keeping essential data in permanent memory (namely prices and energy
plans). To create a user and a database into your otherwise properly set-up MySQL
installation, you need to run the mysql command-line client as admin:
```
    sudo mysql -u root -p
```
then, run these commands to create a user and a database and give the user access to the
database:
```
    CREATE USER 'vesta'@'localhost' IDENTIFIED BY 'type a good password here';
    CREATE DATABASE vesta;
    GRANT ALL PRIVILEGES on vesta.* TO 'vesta'@'localhost';
```
to leave the MySQL command-line tool, just type "quit".

When these have been created successfully, remember to put them to your elpricedb.json
and energyplandb.json configuration files to make it possible for Vesta to access the
database. These files are kept separate in case you want to have different users and/or
database for the electricity prices fetched from NordPool (elpricedb.json) and the energy
plans created by Vesta based on oil & electricity price comparisons (energyplandb.json).
If you want to have different users for these, just repeat the procedure described above.
Once the database access is working, Vesta is able to create and manage all the tables it
needs.

### Create the Vesta user

Vesta is expected to run with user (==non-root) privileges, although it is started thru
systemd. The account should also be such that cannot be used to login, (i.e. unknown
password, no login shell) since systemd merely changes the uid when vesta is started. To
create a user for vesta you should run the following command:

```
    sudo adduser --system --group nogroup vesta
```

You might also need to add the vesta user to the dialout group (or whatever group owns the
serial device node used to access the Z-Wave controller). On ubuntu machines the Aeon Labs
Z-Stick Gen5 appears at /dev/ttyACM0 and is owned by the dialout group. So, to add user vesta
to the dialout group, run the following command:

```
    sudo usermod -G dialout vesta
```

### SSL

Vesta control interface operates on top of an SSL-encrypted HTTPS connection so first of
all you either need to create your certificate and private key files or, tell Vesta where
it can find them. This can be done by editing the server.json file. By default, the SSL
certificate and private key are set to reside in /etc/ssl/certs. If you do not have a
trusted certificate signed by a trusted authority (as the vast majority of us don't), you
can create a self-signed certificate with the openssl suite. Bear in mind though, that
self-signed certificates need to be accepted separately in your browser when accessing
the control interface and they are generally not trustworthy for anything but private use.

1. Generate a private key:
```
    openssl genrsa -des3 -out server.key 1024
```

2. Generate a CSR (Certificate Signing Request) and answer the questions as you see fit:
```
    openssl req -new -key server.key -out server.csr
```

3. Remove passphrase from the key:
```
    cp server.key server.key.org
    openssl rsa -in server.key.org -out server.key
```

4. Generate a self-signed certificate:
```
    openssl x509 -req -days 365 -in server.csr -signkey server.key -out server.crt
```

Then, you need to copy these files to a location that is accessible by Vesta, by default it
is the system-wide SSL certificate folder in /etc/ssl/certs and make them readable by root
only:
```
    sudo cp server.key /etc/ssl/certs/
    sudo cp server.crt /etc/ssl/certs/
    sudo chmod 400 /etc/ssl/certs/server.key
    sudo chmod 400 /etc/ssl/certs/server.crt
```

## Configuration

As mentioned before, /etc/vesta contains all the global configuration files that Vesta uses.
The Vesta file format is JSON, which will be unreadable if you fsck things up. Bear in mind
that the files cannot contain comments so you can't try out different things by commenting
something out. There are free tools available to check the validity of JSON documents, for
example jsonlint.com; just paste the whole file's contents there to see, if it checks out.

Static price data for electricity and oil, as well as Z-Wave node names, switch names etc.
are configured run-time thru the web interface.

If you do not like to use /etc/vesta, you can configure an environment variable VESTA_CONFIG
to point to a directory of your liking.

### database.json

This file specifies the credentials for accessing the database that contains Vesta data.
See the MySQL topic above as these should match with what you used when creating them.
Note that these do not necessarily match with any of your system's user accounts; MySQL users
are a completely different thing compared to them.
* user: The user name that has access to the database.
* password: The password of the user. For the love of God, please use something else than the default.
* host: The name of the host that runs the MySQL instance.
* dbname: The database name reserved for Vesta.

### devicemanager.json

This file is used to configure details related to Z-Wave devices and the OpenZWave library.
NOTE: Make sure the device node is read-write accessible for the user you are using for Vesta!
Otherwise you might get an error trying to open the device node.
* device: The device node in /dev/ that is used to access your OZW-compliant controller. On ubuntu variants Aeon Labs Z-Stick Gen 5 appears at /dev/ttyACM0. On OSX/brew it could be /dev/cu.usbmodemFA131.
* configPath: The path for openzwave device descriptions. With OSX/brew it might be /usr/local/opt/open-zwave/etc/openzwave.

### nordpool.json

This file tells the details needed to acquire electricity prices thru Nordpool.
* url: The URL that is used to fetch the prices daily.
* area: Tells Vesta to store electricity prices only for the given area. If empty, all areas are stored. Possible values: "FI", "SE", "DK", "NO", ""
* time The time of day when electricity prices are fetched. Fetching will occur as long as the service answers successfully. Nordpool announces prices for the next day usually in the afternoon, so 15:00 is usually a good time at least for FI. Too early times will result in you fetching prices only for the current day.

### server.json

This file tells Vesta some details on how to run the management server.
* host: The hostname of the Vesta server
* port: The TCP port Vesta server should listen to.
* sslCertificate: The SSL certificate file. If you don't have a real one, see above topic SSL for creating a self-signed certificate.
* sslPrivateKey: The private key for the server. Ditto.

### options.xml

This is the configuration file for the OpenZWave library that controls your Z-Wave connected
devices. Basically you shouldn't need to tamper with it, unless you really want to. The file
itself contains some comments and more can be found at http://www.openzwave.net

When the Vesta daemon starts, a symbolic link pointing to /etc/vesta/options.xml is created to
the vesta user's home directory.
