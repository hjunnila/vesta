TEMPLATE = subdirs
CONFIG  += ordered

SUBDIRS += lib
SUBDIRS += main
SUBDIRS += pricepoller
SUBDIRS += config
SUBDIRS += monitor

# Unit test coverage measurement
coverage.target     = lcov
QMAKE_EXTRA_TARGETS += coverage
coverage.commands   += ./coverage.sh
