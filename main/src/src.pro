TEMPLATE         = app
TARGET           = vesta
QT              += network sql dbus
QT              -= gui
CONFIG          += precompile_header debug
CONFIG          -= app_bundle

PRECOMPILED_HEADER   = vesta-main-pc.h
OBJECTS_DIR          = obj
INCLUDEPATH         += ../../lib/src

# Linux open-zwave location. Adjust if necessary.
INCLUDEPATH         += /usr/include/openzwave /usr/include/openzwave/command_classes /usr/include/openzwave/value_classes

# Homebrew open-zwave location. Adjust if necessary.
macx:LIBS           += -L/usr/local/Cellar/open-zwave/1.2.919/lib
macx:INCLUDEPATH    += /usr/local/Cellar/open-zwave/1.2.919/include/openzwave
macx:INCLUDEPATH    += /usr/local/Cellar/open-zwave/1.2.919/include/openzwave/command_classes

# Comment this line if you get whining about C++11
DEFINES += _GLIBCXX_USE_CXX11_ABI=0

LIBS    += -lopenzwave
LIBS    += -lstdc++
LIBS    += -L../../lib/src -lvesta

HEADERS += shell.h

SOURCES += main.cpp \
           shell.cpp

target.path = /usr/bin
INSTALLS   += target
