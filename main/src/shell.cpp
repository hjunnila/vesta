#include <unistd.h>
#include <stdio.h>

#include "devicemanager.h"
#include "devicenode.h"
#include "nodevalue.h"
#include "shell.h"

Shell::Shell(DeviceManager* manager)
    : QObject()
    , m_manager(manager)
    , m_in(stdin)
    , m_out(stdout)
    , m_inNotifier(STDIN_FILENO, QSocketNotifier::Read)
{
    qDebug() << Q_FUNC_INFO;

    connect(&m_inNotifier, SIGNAL(activated(int)), this, SLOT(onReadyRead()));
    prompt();
}

Shell::~Shell()
{
    qDebug() << Q_FUNC_INFO;
}

void Shell::parseLine(const QStringList& line)
{
    QString cmd = line[0];

    if (cmd == "help") {
        print(helpText());
    } else if (cmd == "add") {
        m_manager->addDevice();
    } else if (cmd == "remove") {
        m_manager->removeDevice();
    } else if (cmd == "heal") {
        m_manager->healNetwork();
    } else if (cmd == "devices") {
        printList(deviceList());
    } else if (cmd == "isAwake") {
        quint8 node = line[1].toInt();
        isAwake(node);
    } else if (cmd == "refresh") {
        quint8 node = line[1].toInt();
        refreshInfo(node);
    } else if (cmd == "state") {
        quint8 node = line[1].toInt();
        requestState(node);
    } else if (cmd == "values") {
        quint8 node = line[1].toInt();
        printList(deviceValues(node));
    } else if (cmd == "items") {
        quint8 node = line[1].toInt();
        int index = line[2].toInt();
        printList(valueItems(node, index));
    } else if (cmd == "on") {
        quint8 node = line[1].toInt();
        int index = line[2].toInt();
        turnOn(node, index);
    } else if (cmd == "off") {
        quint8 node = line[1].toInt();
        int index = line[2].toInt();
        turnOff(node, index);
    } else if (cmd == "set") {
        quint8 node = line[1].toInt();
        int index = line[2].toInt();
        QString value = line[3];
        setValue(node, index, value);
    } else if (cmd == "start") {
        m_manager->start();
    } else if (cmd == "stop") {
        m_manager->stop();
    } else if (cmd == "rename") {
        quint8 node = line[1].toInt();
        QString name = line[2];
        rename(node, name);
    } else if (cmd == "reset") {
        QString how = line[1];
        if (how == "hard") {
            m_manager->reset(true);
        } else {
            m_manager->reset();
        }
    } else if (cmd == "quit") {
        QCoreApplication::quit();
        return;
    }

    prompt();
}

void Shell::prompt()
{
    print(QString("%1> ").arg(QCoreApplication::applicationName()));
}

void Shell::onReadyRead()
{
    QString line;
    m_in.readLineInto(&line);
    parseLine(line.split(" "));
}

void Shell::print(const QString& text)
{
    m_out << text;
    m_out.flush();
}

void Shell::printList(const QStringList& list)
{
    foreach (QString item, list) {
        print(item);
        print("\n");
    }
}

QString Shell::helpText() const
{
    QString txt;
    txt.append("help        Print this help text\n");
    txt.append("add         Add a new device\n");
    txt.append("remove      Remove device by its id\n");
    txt.append("heal        Heal the network\n");
    txt.append("devices     Print a list of known devices\n");
    txt.append("isAwake x   Check if the given node is awake\n");
    txt.append("refresh x   Refresh static info for node x\n");
    txt.append("state x     Request dynamic data from node x\n");
    txt.append("rename x y  Set the name of node x to \"y\"\n");
    txt.append("values x    Print a list of known values in device id x\n");
    txt.append("items x y   Print a list of possible values for device x list value y\n");
    txt.append("on x y      Switch on value y in device x\n");
    txt.append("off x y     Switch off value y in device x\n");
    txt.append("set x y z   Set value y in device x to be z\n");
    txt.append("start       Start the Z-Wave manager\n");
    txt.append("stop        Stop the Z-Wave manager\n");
    txt.append("reset [hard] Reset the controller. If 'hard' is specified, removes all known devices\n");
    txt.append("quit        Quit the application\n");

    return txt;
}

QStringList Shell::deviceList() const
{
    qDebug() << Q_FUNC_INFO;
    QStringList list;

    QMap <quint8,DeviceNode*> devices(m_manager->devices());
    for (QMap <quint8,DeviceNode*>::const_iterator it = devices.begin(); it != devices.end(); ++it) {
        DeviceNode* dn = *it;
        QString name = QString("%1: %2 %3 (%4)").arg(dn->node()).arg(dn->manufacturer()).arg(dn->productName()).arg(dn->name());
        list << name;
    }

    return list;
}

void Shell::isAwake(quint8 node)
{
    DeviceNode* dn = m_manager->deviceNode(node);
    if (!dn) {
        print(QString("No such device\n"));
    }

    if (dn->isAwake()) {
        print(QString("Node is awake.\n"));
    } else {
        print(QString("Node is not awake.\n"));
    }
}

void Shell::refreshInfo(quint8 node)
{
    DeviceNode* dn = m_manager->deviceNode(node);
    if (!dn) {
        print(QString("No such device\n"));
    }

    if (dn->refreshInfo()) {
        print(QString("Node refresh initiated.\n"));
    } else {
        print(QString("Failed to initiate node refresh.\n"));
    }
}

void Shell::requestState(quint8 node)
{
    DeviceNode* dn = m_manager->deviceNode(node);
    if (!dn) {
        print(QString("No such device\n"));
    }

    if (dn->requestState()) {
        print(QString("Requested node dynamic data.\n"));
    } else {
        print(QString("Failed to request node dynamic data.\n"));
    }
}

void Shell::rename(quint8 node, const QString& name)
{
    DeviceNode* dn = m_manager->deviceNode(node);
    if (!dn) {
        print(QString("No such device"));
    }

    dn->setName(name);
}

QStringList Shell::deviceValues(quint8 node) const
{
    DeviceNode* dn = m_manager->deviceNode(node);
    if (!dn) {
        return QStringList(QString("No such device"));
    }

    QStringList list;
    QList <NodeValue*> values(dn->values());
    for (int i = 0; i < values.count(); i++) {
        NodeValue* nv(values[i]);
        QString current;
        QVariant var = nv->currentValue();
        if (var.canConvert(QMetaType::QString)) {
            current = var.toString();
        } else {
            current = QString("%1").arg(var.toInt());
        }
        QString val = QString("[%1] %2(%3): %4%5").arg(i).arg(nv->type()).arg(nv->label()).arg(current).arg(nv->units());
        list.append(val);
    }

    return list;
}

QStringList Shell::valueItems(quint8 node, int index) const
{
    DeviceNode* dn = m_manager->devices()[node];
    if (!dn) {
        return QStringList("No such node!");
    }

    if (dn->values().count() < index + 1) {
        return QStringList("No such value!");
    }

    NodeValue* nv(dn->values()[index]);
    return nv->listItems();
}

void Shell::turnOn(quint8 node, int index)
{
    DeviceNode* dn = m_manager->devices()[node];
    if (!dn) {
        print("No such node!");
        return;
    }

    if (dn->values().count() < index + 1) {
        print("No such value!");
        return;
    }

    NodeValue* nv(dn->values()[index]);
    nv->turnOn();
}

void Shell::turnOff(quint8 node, int index)
{
    DeviceNode* dn = m_manager->devices()[node];
    if (!dn) {
        print("No such node!");
        return;
    }

    if (dn->values().count() < index + 1) {
        print("No such value!");
        return;
    }

    NodeValue* nv(dn->values()[index]);
    nv->turnOff();
}

void Shell::setValue(quint8 node, int index, const QString& value)
{
    DeviceNode* dn = m_manager->devices()[node];
    if (!dn) {
        print("No such node!");
        return;
    }

    if (dn->values().count() < index + 1) {
        print("No such value!");
        return;
    }

    NodeValue* nv(dn->values()[index]);
    nv->setValue(value);
}
