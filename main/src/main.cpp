#include <QtCore>
#include <signal.h>

#include "devicemanagerservice.h"
#include "energymanagerservice.h"
#include "elpricedb_interface.h"
#include "devicemanager.h"
#include "energymanager.h"
#include "fileservice.h"
#include "server.h"
#include "shell.h"

void signalHandler(int sig)
{
    qWarning() << "Signal received.";
    QCoreApplication::exit(sig);
}

int main(int argc, char** argv)
{
    signal(SIGINT, signalHandler);
    signal(SIGTERM, signalHandler);

    QCoreApplication app(argc, argv);
    QCoreApplication::setApplicationName("vesta");
    QCoreApplication::setApplicationVersion("0.1");

    QCommandLineParser parser;
    parser.setApplicationDescription("Home heating manager");
    parser.addHelpOption();
    parser.addVersionOption();
    QCommandLineOption interactive(QStringList() << "i" << "interactive",
                                   "Start in interactive mode where you can manage devices and settings.");
    parser.addOption(interactive);
    parser.process(app);

    Server* server = new Server;
    DeviceManager* dm = new DeviceManager;
    EnergyManager* em = new EnergyManager;

    QObject::connect(em, SIGNAL(energySourcePriorityChanged(EnergySource*,EnergySourcePriority)),
                     dm, SLOT(onEnergySourcePriorityChanged(EnergySource*,EnergySourcePriority)));
    QObject::connect(dm, SIGNAL(deviceNodeValueChanged(const QString&,const QString&,const QVariant&)),
                     em, SLOT(onDeviceNodeValueChanged(const QString&,const QString&,const QVariant&)));

    // Register HTTPS services
    server->registerService(new DeviceManagerService(dm));
    server->registerService(new EnergyManagerService(em));
    server->registerService(new FileService);

    // Start the server when DeviceManager is ready
    QObject::connect(dm, SIGNAL(ready()), server, SLOT(start()));

    // Start EnergyManager when DeviceManager is ready
    QObject::connect(dm, SIGNAL(ready()), em, SLOT(start()));

    Shell* shell = NULL;
    if (parser.isSet(interactive)) {
        shell = new Shell(dm);
    } else {
        org::vesta::elpricedb* iface = new org::vesta::elpricedb(QString(), QString(), QDBusConnection::systemBus(), em);
        QObject::connect(iface, SIGNAL(newPriceDataAvailable(const QString&, const QString&)),
                         em, SLOT(onNewPriceDataAvailable(const QString&, const QString&)));

        dm->start();
    }

    int result = app.exec();

    qWarning() << "Terminating process";

    delete server;
    delete em;
    delete dm;
    delete shell;

    return result;
}
