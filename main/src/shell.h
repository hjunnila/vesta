#pragma once

class DeviceManager;

class Shell : public QObject
{
    Q_OBJECT
public:
    Shell(DeviceManager* manager);
    ~Shell();

    void parseLine(const QStringList& line);
    QString helpText() const;
    void print(const QString& text);
    void printList(const QStringList& list);
    void prompt();

    QStringList deviceList() const;
    void rename(quint8 node, const QString& name);
    void isAwake(quint8 node);
    void refreshInfo(quint8 node);
    void requestState(quint8 node);
    QStringList deviceValues(quint8 node) const;
    QStringList valueItems(quint8 node, int index) const;

    void turnOn(quint8 node, int index);
    void turnOff(quint8 node, int index);
    void setValue(quint8 node, int index, const QString& value);

private slots:
    void onReadyRead();

private:
    DeviceManager* m_manager;
    QTextStream m_in;
    QTextStream m_out;
    QSocketNotifier m_inNotifier;
};
