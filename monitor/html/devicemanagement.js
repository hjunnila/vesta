   function renameDevice(deviceNode, oldName) {
        var newName = prompt("Enter device name", oldName);
        if (newName != null && newName != oldName) {
            var myURL = getServerUrl() + "/devices/" + deviceNode;
            $.ajax({
                contentType: 'text/plain',
                data: "{ \"name\": \"" + newName + "\" }",
                success: function(data) {
                    console.log("Device rename succeeded");
                    getDevices();
                },
                error: function() {
                    console.log("Device rename failed");
                },
                processData: false,
                type: 'POST',
                url: myURL
            });
        }
    }

    function getDevices() {
        var url = getServerUrl() + "/devices";
        $.get(url, function(result, status) {
            $( "#accordion" ).accordion('destroy');
            var content = "";
            var deviceArray = result.devices;
            for (var i = 0; i < deviceArray.length; i++) {
                var dev = deviceArray[i];
                var devid = "device-" + dev.node;
                content += "<h3>" + dev.node + ": " + (dev.name.length ? dev.name : "Unnamed") + " (" + dev.productName + ")</h3>";
                content += "<div id='" + devid + "'>";
                content += "<table border='0'>";
                content += "<tr><td>Manufacturer:</td><td>" + dev.manufacturer + "</td></tr>";
                content += "<tr><td>Model:</td><td>" + dev.productName + "</td></tr>";
                content += "<tr><td>Name:</td><td><input type='text' value='" + dev.name + "' disabled></input>";
                if (dev.node > 1) {
                    content += "<button onclick='renameDevice(" + dev.node + ", \"" + dev.name + "\")'>Rename</button>";
                }
                content += "</td></tr>";
                content += "<tr><td>Awake:</td><td>" + dev.isAwake + "</td></tr>";
                content += "</table>"
                content += "<br>";

                var valueArray = dev.values;
                if (valueArray.length > 0) {
                    content += "<table border='0'>";
                    content += "<tr><td><B>Values</B></td><td></td></tr>";

                    for (var j = 0; j < valueArray.length; j++) {
                        var value = valueArray[j];
                        var ro = "";
                        if (value.readonly) {
                            ro = "disabled ";
                        }

                        content += "<tr><td>" + value.label + ":</td><td>";
                        if (value.type === "List") {
                            content += "<select id='" + value.id + "'" + ro + ">";
                                var itemArray = value.items;
                                for (var k = 0; k < itemArray.length; k++) {
                                    var selected = "";
                                    if (value.current == itemArray[k]) {
                                        selected = "selected";
                                    }
                                    content += "<option " + selected + ">" + itemArray[k] + "</option>";
                                }
                            content += "</select>";
                        } else if (value.type === "Bool" || value.type === "Switch" || value.type === "Button") {
                            var checked = "";
                            if (value.current === true || value.current === "true") {
                                checked = "checked ";
                            }
                            content += "<input id='" + value.id + "' type='checkbox' " + checked + ro + ">" + value.units + "</input>";
                        } else if (value.type === "Byte") {
                            content += "<input id='" + value.id + "' type='number' min='0' max='255' value='" + value.current + "'" + ro + ">" + value.units + "</input>";
                        } else {
                            content += "<input id='" + value.id + "' type='text' value='" + value.current + "'" + ro + ">" + value.units + "</input>";
                        }

                        content += "</td>";
                        if (!value.readonly) {
                            content += "<td><button onClick=\"setNodeValue('" + dev.node + "', '" + value.id + "')\">Set</button></td>";
                        }
                        content += "</tr>";
                    }
                }

                content += "</table>";

                if (dev.node <= 1) {
                    content += "<p>This is your Z-Wave master controller device</p>";
                }
                content += "</div>";
            }

            document.getElementById("accordion").innerHTML = content;
            $('#accordion').html(content).accordion({collapsible: true});
        });
    }

    function setNodeValue(deviceNode, valueid) {
        var element = document.getElementById(valueid);
        var valueToSet = "";
        if (element.type == "checkbox") {
            valueToSet = element.checked;
        } else {
            valueToSet = element.value;
        }
        
        console.log("Devicenode:" + deviceNode + ", valueid:" + valueid + ", new value:" + valueToSet);

        var myURL = getServerUrl() + "/devices/" + deviceNode + "/value/" + valueid;
        $.ajax({
            contentType: 'text/plain',
            data: "{ \"current\": \"" + valueToSet + "\" }",
            success: function(data, status) {
                console.log("Setting device node value succeeded:" + status);
            },
            error: function() {
                console.log("Setting device node value failed");
            },
            processData: false,
            type: 'POST',
            url: myURL
        });
    }

    function addDevice() {
        if (confirm("Click 'Ok' to start the new device inclusion process.")) {
            var url = getServerUrl() + "/devices/add";
            $.post(url, function(result, status) {
                if (status === "success") {
                    alert("Device inclusion process started. Press the 'connect' button on your Z-Wave device to include it to the system. Then wait a while and click 'Refresh devices' to see if the device was recognized successfully.");
                } else {
                    alert("Unable to start device inclusion process. Status: " + status);
                }
            });
        }
    }

    function removeDevice() {
        if (confirm("Click 'Ok' to start the device removal process.")) {
            var url = getServerUrl() + "/devices/remove";
            $.post(url, function(result, status) {
                if (status === "success") {
                    alert("Device removal procedure started. Press the 'connect' button on your Z-Wave device to remove it from the system. Then wait a while and click 'Refresh devices' to see if the removal was completed successfully.");
                } else {
                    alert("Unable to start device removal process. Status: " + status);
                }
            });
        }
    }
