function getStaticElectricData() {
    var url = getServerUrl() + "/static_data?type=electric";
    var jqXHR = $.get(url, function(result, status) {
        var content = "<table border=\"0\">";

        content += "<tr>";
        content += "<td>Energy tax</td> <td><input size=\"5\" type=\"text\" id=\"energyTax\" value=\"" + (result.energyTax * 100) + "\"/> %</td>";
        content += "</tr>";

        content += "<tr>";
        content += "<td>Transfer</td> <td><input size=\"5\" type=\"text\" id=\"transfer\" value=\"" + result.transfer + "\"/> c/kWh</td>";
        content += "</tr>";

        content += "<tr>";
        content += "<td>Transfer tax</td> <td><input size=\"5\" type=\"text\" id=\"transferTax\" value=\"" + (result.transferTax * 100) + "\"/> %</td>";
        content += "</tr>";

        content += "<tr>";
        content += "<td>Premium</td> <td><input size=\"5\" type=\"text\" id=\"premium\" value=\"" + result.premium + "\"/> c/kWh</td>";
        content += "</tr>";

        content += "<tr>";
        content += "<td>Premium tax</td> <td><input size=\"5\" type=\"text\" id=\"premiumTax\" value=\"" + (result.premiumTax * 100) + "\"/> %</td>";
        content += "</tr>";

        content += "<tr>";
        content += "<td>Switch device</td> <td><input size=\"20\" type=\"text\" id=\"elSwitchDevice\" value=\"" + result.switchDevice + "\"/> </td>";
        content += "</tr>";

        content += "<tr>";
        content += "<td>Switch value</td> <td><input size=\"20\" type=\"text\" id=\"elSwitchValue\" value=\"" + result.switchValue + "\"/> </td>";
        content += "</tr>";

        content += "<tr>";
        content += "<td>Power device</td> <td><input size=\"20\" type=\"text\" id=\"elPowerDevice\" value=\"" + result.powerDevice + "\"/> </td>";
        content += "</tr>";

        content += "<tr>";
        content += "<td>Power value</td> <td><input size=\"20\" type=\"text\" id=\"elPowerValue\" value=\"" + result.powerValue + "\"/> </td>";
        content += "</tr>";

        content += "<tr>";
        content += "<td>Geographic area</td> <td><input size=\"5\" type=\"text\" id=\"elArea\" value=\"" + result.area + "\"/> </td>";
        content += "</tr>";

        content += "</table>";

        content += "<button id=\"send_staticElectricData\" onClick=\"sendStaticElectricData();\">Write data</button>";

        document.getElementById("staticElectricData").innerHTML = content;
    });
}

function sendStaticElectricData() {
    var energyTax = document.getElementById("energyTax").value;
    var transfer = document.getElementById("transfer").value;
    var transferTax = document.getElementById("transferTax").value;
    var premium = document.getElementById("premium").value;
    var premiumTax = document.getElementById("premiumTax").value;

    var switchDevice = document.getElementById("elSwitchDevice").value;
    var switchValue = document.getElementById("elSwitchValue").value;

    var powerDevice = document.getElementById("elPowerDevice").value;
    var powerValue = document.getElementById("elPowerValue").value;

    var area = document.getElementById("elArea").value;

    var url = getServerUrl() + "/static_data";
    $.ajax({
        contentType: 'text/plain',
        data: "{ \"electric\": { \"switchDevice\": \"" + switchDevice + "\"," +
                                "\"switchValue\": \"" + switchValue + "\"," +
                                "\"powerDevice\": \"" + powerDevice + "\"," +
                                "\"powerValue\": \"" + powerValue + "\"," +
                                "\"area\": \"" + area + "\"," +
                                "\"energyTax\": \"" + (energyTax / 100) + "\"," +
                                "\"transfer\": \"" + transfer + "\"," +
                                "\"transferTax\": \"" + (transferTax / 100) + "\"," +
                                "\"premium\": \"" + premium + "\"," +
                                "\"premiumTax\": \"" + (premiumTax / 100) + "\"" +
                               "}" +
              "}",
        success: function(data) {
            console.log("Static electric data update OK");
        },
        error: function() {
            console.log("Static electric data update failed");
        },
        processData: false,
        type: 'POST',
        url: url
    });
}

function getStaticOilData() {
    var url = getServerUrl() + "/static_data?type=oil";
    var jqXHR = $.get(url, function(result, status) {
        var content = "<table border=\"0\">";

        content += "<tr>";
        content += "<td>Price</td> <td><input size=\"5\" type=\"text\" id=\"litrePrice\" value=\"" + result.litrePrice + "\"/> c/l</td>";
        content += "</tr>";

        content += "<tr>";
        content += "<td>Efficiency</td> <td><input size=\"5\" type=\"text\" id=\"efficiency\" value=\"" + (result.efficiency * 100) + "\"/> %</td>";
        content += "</tr>";

        content += "<tr>";
        content += "<td>Energy content</td> <td><input size=\"5\" type=\"text\" id=\"kWhPerLitre\" value=\"" + result.kWhPerLitre + "\"/> kWh/l</td>";
        content += "</tr>";

        content += "<tr>";
        content += "<td>Switch device</td> <td><input size=\"20\" type=\"text\" id=\"oilSwitchDevice\" value=\"" + result.switchDevice + "\"/> </td>";
        content += "</tr>";

        content += "<tr>";
        content += "<td>Switch value</td> <td><input size=\"20\" type=\"text\" id=\"oilSwitchValue\" value=\"" + result.switchValue + "\"/> </td>";
        content += "</tr>";

        content += "<tr>";
        content += "<td>Power device</td> <td><input size=\"20\" type=\"text\" id=\"oilPowerDevice\" value=\"" + result.powerDevice + "\"/> </td>";
        content += "</tr>";

        content += "<tr>";
        content += "<td>Power value</td> <td><input size=\"20\" type=\"text\" id=\"oilPowerValue\" value=\"" + result.powerValue + "\"/> </td>";
        content += "</tr>";

        content += "</table>";

        content += "<button id=\"send_staticOilData\" onClick=\"sendStaticOilData();\">Write data</button>";

        document.getElementById("staticOilData").innerHTML = content;
    });
}

function sendStaticOilData() {
    var litrePrice = document.getElementById("litrePrice").value;
    var efficiency = document.getElementById("efficiency").value;
    var kWhPerLitre = document.getElementById("kWhPerLitre").value;

    var switchDevice = document.getElementById("oilSwitchDevice").value;
    var switchValue = document.getElementById("oilSwitchValue").value;

    var powerDevice = document.getElementById("oilPowerDevice").value;
    var powerValue = document.getElementById("oilPowerValue").value;

    var url = getServerUrl() + "/static_data";
    $.ajax({
        contentType: 'text/plain',
        data: "{ \"oil\": { \"litrePrice\": \"" + litrePrice + "\"," +
                           "\"efficiency\": \"" + (efficiency / 100) + "\"," +
                           "\"kWhPerLitre\": \"" + kWhPerLitre + "\"," +
                           "\"switchDevice\": \"" + switchDevice + "\"," +
                           "\"switchValue\": \"" + switchValue + "\"," +
                           "\"powerDevice\": \"" + powerDevice + "\"," +
                           "\"powerValue\": \"" + powerValue + "\"" +
                         "}" +
              "}",
        success: function(data) {
            console.log("Static oil data update OK");
        },
        error: function() {
            console.log("Static oil data update failed");
        },
        processData: false,
        type: 'POST',
        url: url
    });
}
