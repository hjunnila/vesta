    function currentPlanHighlight(serverDate, dataSetDate) {
        if (serverDate.getFullYear() == dataSetDate.getFullYear()) {
            if (serverDate.getMonth() == dataSetDate.getMonth()) {
                if (serverDate.getDate() == dataSetDate.getDate()) {
                    var start = serverDate.getHours() - 0.5;
                    var end = serverDate.getHours() + 0.5;
                    var stripLine = {
                        startValue: start,
                        endValue: end,
                        color:"#BDA998",
                        label: "Current",
                        labelFontColor: "#BDA998"
                    };

                    return stripLine;
                }
            }
        }

        return {};
    }

    var currentDate;
    function getEnergyPlanData(date) {
        currentDate = date;
        var url = getServerUrl() + "/energyPlan";
        url += "?date=" + date;
        url += "&format=canvasjs";
        var jqXHR = $.get(url, function(result, status) {
            let prices = result;
            let serverDate = new Date(Date.parse(jqXHR.getResponseHeader("Date")));
            let dataSetDate = new Date(Date.parse(prices.start));
            let hilite = currentPlanHighlight(serverDate, dataSetDate);

            var chart = new CanvasJS.Chart("chartContainer", {
                title: {
                    text: "Energy prices on " + date
                },

                animationEnabled: true,
                animationDuration: 300,

                toolTip: {
                    shared: true,
                    contentFormatter: function(e) {
                        var str = "";
                        for (var i = 0; i < e.entries.length; i++) {
                            var temp = e.entries[i].dataSeries.name + ": "+ e.entries[i].dataPoint.y + "c/kWh<br/>";
                            str = str.concat(temp);
                        }
                        return (str);
                    }
                },

                axisY: {
                    title:"c/kWh",
                    valueFormatString: "###.0",
                    maximum: 20,
                },
                axisX: {
                    title: "Hourly plan",
                    gridThickness: 0,
                    interval: 1,
                    intervalType: "number",
                    valueFormatString: "##",
                    labelAngle: 0,
                    labelFontSize: 12,
                    titleFontSize: 18,
                    stripLines:[hilite]
                },
                animationEnabled: true,
                data: [
                    {
                        name: "Oil",
                        type: "stepArea",
                        legendText: "Oil",
                        fillOpacity: .5,
                        color: "#998E8E",
                        showInLegend: true,
                        markerType: "none",
                        lineThickness: 1,
                        dataPoints: prices.oil
                    },
                    {
                        name: "Transfer",
                        type: "stackedColumn",
                        legendText: "Transfer",
                        color: "#6C5656",
                        showInLegend: true,
                        dataPoints: prices.transfer
                    },
                    {
                        name: "Premium",
                        type: "stackedColumn",
                        legendText: "Premium",
                        showInLegend: true,
                        color: "#897A7A",
                        dataPoints: prices.premium
                    },
                    {
                        name: "Energy",
                        indexLabel: "#total c/kWh",
                        indexLabelFontSize: 11,
                        indexLabelFontColor: "black",
                        yValueFormatString: "###.##",
                        indexLabelPlacement: "outside",
                        indexLabelOrientation: "vertical",
                        bevelEnabled: true,
                        type: "stackedColumn",
                        legendText: "Energy",
                        color: "#BD9898",
                        showInLegend: true,
                        dataPoints: prices.energy
                    }
                ]
            });

            chart.render();
        });
    }

    function pad(n, width, z) {
        z = z || '0';
        n = n + '';
        return n.length >= width ? n : new Array(width - n.length + 1).join(z) + n;
    }

    function getNextDate() {
        var current = Date.parse(currentDate);
        var next = new Date(current + (24 * 60 * 60 * 1000));
        var dateString = ('' + next.getFullYear() + '-' + pad((next.getMonth() + 1), 2) + '-' + pad(next.getDate(), 2));
        return dateString;
    }

    function getPreviousDate() {
        var current = Date.parse(currentDate);
        var prev = new Date(current - (24 * 60 * 60 * 1000));
        return ('' + prev.getFullYear() + '-' + pad((prev.getMonth() + 1), 2) + '-' + pad(prev.getDate(), 2));
    }

    function getTodaysDate() {
        var date = $.url().param('date');
        if (!date) {
            var n = new Date(Date.now());
            date = '' + n.getFullYear() + '-' + pad((n.getMonth() + 1), 2) + '-' + pad(n.getDate(), 2);
        }
        return date;
    }

    function updateManualElectricState() {
        var url = getServerUrl() + "/manualElectricPlan";
        var jqXHR = $.get(url, function(result, status) {
            var text;
            if (result.manualElectricOverride === true) {
                text = "Manual electric is ON <input type='checkbox' id='manuele' hidden checked>";
            } else {
                text = "Manual electric is OFF <input type='checkbox' id='manuele' hidden>";
            }
            document.getElementById("manualElectricState").innerHTML = text;
        });
    }

    function switchManualElectricState() {
        var url = getServerUrl() + "/manualElectricPlan";
        let checked = document.getElementById("manuele").checked;
        $.ajax({
            contentType: 'text/plain',
            data: "{ \"manualElectricOverride\":" + ((checked) ? "false" : "true") + " }",
            success: function(data) {
                console.log("Manual electric state switch OK");
                updateManualElectricState();
            },
            error: function() {
                console.log("Manual electric state switch failed");
                updateManualElectricState();
            },
            processData: false,
            type: 'POST',
            url: url
        });
    }
