TEMPLATE = subdirs
CONFIG  += ordered

html.files += index.html
html.files += purl.js
html.files += jquery.canvasjs.min.js
html.files += jquery-1.11.1.min.js
html.files += energyplan.js
html.files += devicemanagement.js
html.files += staticdata.js
html.files += serverconfig.js
html.path   = /var/www/html/vesta
INSTALLS += html
