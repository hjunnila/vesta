const serverConfig = {
    "host": "localhost",
    "port": 8002
};

function getServerUrl() {
    var url = "https://" + serverConfig.host + ":" + serverConfig.port;
    return url;
}
