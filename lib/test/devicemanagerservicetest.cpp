#include "devicemanagerservicetest.h"
#include "devicemanagerservice_p.h"
#include "devicemanagerservice.h"
#include "mockdevicemanager.h"
#include "devicemanager_p.h"
#include "mockdevicenode.h"
#include "mocknodevalue.h"
#include "devicemanager.h"
#include "devicenode_p.h"
#include "service_p.h"
#include "response.h"
#include "service.h"
#include "request.h"

void DeviceManagerServiceTest::testConstruction()
{
    DeviceManager dm;
    DeviceManagerService dms(&dm);
    QVERIFY(dms.d_ptr != NULL);
    QCOMPARE(dms.d_ptr->deviceManager, &dm);
}

void DeviceManagerServiceTest::testGetDevices()
{
    DeviceManager dm;
    DeviceManagerService dms(&dm);

    MockDeviceNode* dn = new MockDeviceNode(123, 1);
    dn->d_ptr->values.append(new MockNodeValue(1, 123));
    dn->d_ptr->values.append(new MockNodeValue(2, 123));
    dm.d_ptr->devices.insert(1, dn);

    dn = new MockDeviceNode(123, 2);
    dn->d_ptr->values.append(new MockNodeValue(3, 123));
    dn->d_ptr->values.append(new MockNodeValue(4, 123));
    dm.d_ptr->devices.insert(2, dn);

    Request req(QByteArray("GET /devices HTTP/1.1\r\nContent-Length: 0\r\n\r\n"));
    Response resp;
    QVERIFY(dms.handleRequest(req, resp));
    QCOMPARE(resp.status(), 200);

    QJsonParseError error;
    QJsonDocument doc = QJsonDocument::fromJson(resp.body().toUtf8(), &error);
    QCOMPARE(error.error, QJsonParseError::NoError);

    QJsonObject root = doc.object();

    QJsonValue val = root["devices"];
    QVERIFY(val.isArray());
    QJsonArray devices = val.toArray();
    QCOMPARE(devices.size(), 2);
    QCOMPARE(devices[0].toObject()["node"].toInt(), 1);
    QCOMPARE(devices[0].toObject()["isAwake"].toBool(), true);
    QCOMPARE(devices[0].toObject()["manufacturer"].toString(), QString("acme"));
    QCOMPARE(devices[0].toObject()["productName"].toString(), QString("booboo b gone"));
    QCOMPARE(devices[0].toObject()["name"].toString(), QString("ouch"));
    QJsonArray values = devices[0].toObject()["values"].toArray();
    QCOMPARE(values[0].toObject()["id"].toString(), QString("1"));
    QCOMPARE(values[0].toObject()["current"].toString(), QString("bar"));
    QCOMPARE(values[0].toObject()["label"].toString(), QString("foo"));
    QCOMPARE(values[0].toObject()["readonly"].toBool(), false);
    QCOMPARE(values[0].toObject()["type"].toString(), QString("String"));
    QCOMPARE(values[0].toObject()["units"].toString(), QString("watts"));
    QCOMPARE(values[1].toObject()["id"].toString(), QString("2"));
    QCOMPARE(values[1].toObject()["current"].toString(), QString("bar"));
    QCOMPARE(values[1].toObject()["label"].toString(), QString("foo"));
    QCOMPARE(values[1].toObject()["readonly"].toBool(), false);
    QCOMPARE(values[1].toObject()["type"].toString(), QString("String"));
    QCOMPARE(values[1].toObject()["units"].toString(), QString("watts"));

    QCOMPARE(devices[1].toObject()["node"].toInt(), 2);
    QCOMPARE(devices[1].toObject()["isAwake"].toBool(), true);
    QCOMPARE(devices[1].toObject()["manufacturer"].toString(), QString("acme"));
    QCOMPARE(devices[1].toObject()["productName"].toString(), QString("booboo b gone"));
    QCOMPARE(devices[1].toObject()["name"].toString(), QString("ouch"));
    values = devices[1].toObject()["values"].toArray();
    QCOMPARE(values[0].toObject()["id"].toString(), QString("3"));
    QCOMPARE(values[0].toObject()["current"].toString(), QString("bar"));
    QCOMPARE(values[0].toObject()["label"].toString(), QString("foo"));
    QCOMPARE(values[0].toObject()["readonly"].toBool(), false);
    QCOMPARE(values[0].toObject()["type"].toString(), QString("String"));
    QCOMPARE(values[0].toObject()["units"].toString(), QString("watts"));
    QCOMPARE(values[1].toObject()["id"].toString(), QString("4"));
    QCOMPARE(values[1].toObject()["current"].toString(), QString("bar"));
    QCOMPARE(values[1].toObject()["label"].toString(), QString("foo"));
    QCOMPARE(values[1].toObject()["readonly"].toBool(), false);
    QCOMPARE(values[1].toObject()["type"].toString(), QString("String"));
    QCOMPARE(values[1].toObject()["units"].toString(), QString("watts"));
}

void DeviceManagerServiceTest::testGetDevice1()
{
    DeviceManager dm;
    DeviceManagerService dms(&dm);

    MockDeviceNode* dn = new MockDeviceNode(123, 1);
    dn->d_ptr->values.append(new MockNodeValue(1, 123));
    dn->d_ptr->values.append(new MockNodeValue(2, 123));
    dm.d_ptr->devices.insert(1, dn);

    dn = new MockDeviceNode(123, 2);
    dn->d_ptr->values.append(new MockNodeValue(3, 123));
    dn->d_ptr->values.append(new MockNodeValue(4, 123));
    dm.d_ptr->devices.insert(2, dn);

    Request req(QByteArray("GET /devices/1 HTTP/1.1\r\nContent-Length: 0\r\n\r\n"));
    Response resp;
    QVERIFY(dms.handleRequest(req, resp));
    QCOMPARE(resp.status(), 200);

    QJsonParseError error;
    QJsonDocument doc = QJsonDocument::fromJson(resp.body().toUtf8(), &error);
    QCOMPARE(error.error, QJsonParseError::NoError);

    QJsonObject root = doc.object();
    QCOMPARE(root["node"].toInt(), 1);
    QCOMPARE(root["isAwake"].toBool(), true);
    QCOMPARE(root["manufacturer"].toString(), QString("acme"));
    QCOMPARE(root["productName"].toString(), QString("booboo b gone"));
    QCOMPARE(root["name"].toString(), QString("ouch"));
    QJsonArray values = root["values"].toArray();
    QCOMPARE(values[0].toObject()["id"].toString(), QString("1"));
    QCOMPARE(values[0].toObject()["current"].toString(), QString("bar"));
    QCOMPARE(values[0].toObject()["label"].toString(), QString("foo"));
    QCOMPARE(values[0].toObject()["readonly"].toBool(), false);
    QCOMPARE(values[0].toObject()["type"].toString(), QString("String"));
    QCOMPARE(values[0].toObject()["units"].toString(), QString("watts"));
    QCOMPARE(values[1].toObject()["id"].toString(), QString("2"));
    QCOMPARE(values[1].toObject()["current"].toString(), QString("bar"));
    QCOMPARE(values[1].toObject()["label"].toString(), QString("foo"));
    QCOMPARE(values[1].toObject()["readonly"].toBool(), false);
    QCOMPARE(values[1].toObject()["type"].toString(), QString("String"));
    QCOMPARE(values[1].toObject()["units"].toString(), QString("watts"));
}

void DeviceManagerServiceTest::testGetDevice2()
{
    DeviceManager dm;
    DeviceManagerService dms(&dm);

    MockDeviceNode* dn = new MockDeviceNode(123, 1);
    dn->d_ptr->values.append(new MockNodeValue(1, 123));
    dn->d_ptr->values.append(new MockNodeValue(2, 123));
    dm.d_ptr->devices.insert(1, dn);

    dn = new MockDeviceNode(123, 2);
    dn->d_ptr->values.append(new MockNodeValue(3, 123));
    dn->d_ptr->values.append(new MockNodeValue(4, 123));
    dm.d_ptr->devices.insert(2, dn);

    Request req(QByteArray("GET /devices/2 HTTP/1.1\r\nContent-Length: 0\r\n\r\n"));
    Response resp;
    QVERIFY(dms.handleRequest(req, resp));
    QCOMPARE(resp.status(), 200);

    QJsonParseError error;
    QJsonDocument doc = QJsonDocument::fromJson(resp.body().toUtf8(), &error);
    QCOMPARE(error.error, QJsonParseError::NoError);

    QJsonObject root = doc.object();
    QCOMPARE(root["node"].toInt(), 2);
    QCOMPARE(root["isAwake"].toBool(), true);
    QCOMPARE(root["manufacturer"].toString(), QString("acme"));
    QCOMPARE(root["productName"].toString(), QString("booboo b gone"));
    QCOMPARE(root["name"].toString(), QString("ouch"));
    QJsonArray values = root["values"].toArray();
    QCOMPARE(values[0].toObject()["id"].toString(), QString("3"));
    QCOMPARE(values[0].toObject()["current"].toString(), QString("bar"));
    QCOMPARE(values[0].toObject()["label"].toString(), QString("foo"));
    QCOMPARE(values[0].toObject()["readonly"].toBool(), false);
    QCOMPARE(values[0].toObject()["type"].toString(), QString("String"));
    QCOMPARE(values[0].toObject()["units"].toString(), QString("watts"));
    QCOMPARE(values[1].toObject()["id"].toString(), QString("4"));
    QCOMPARE(values[1].toObject()["current"].toString(), QString("bar"));
    QCOMPARE(values[1].toObject()["label"].toString(), QString("foo"));
    QCOMPARE(values[1].toObject()["readonly"].toBool(), false);
    QCOMPARE(values[1].toObject()["type"].toString(), QString("String"));
    QCOMPARE(values[1].toObject()["units"].toString(), QString("watts"));
}

void DeviceManagerServiceTest::testAddDevice()
{
    MockDeviceManager dm;
    DeviceManagerService dms(&dm);

    Request req(QByteArray("POST /devices/add HTTP/1.1\r\nContent-Length: 0\r\n\r\n"));
    Response resp;
    QVERIFY(dms.handleRequest(req, resp));
    QCOMPARE(resp.status(), 200);

    QCOMPARE(dm.m_addDeviceCalled, 1);
}
