#pragma once

class DatabaseInfo;
class OilTest : public FileSpawningTestCase
{
    Q_OBJECT

    DatabaseInfo dbinfo();
    QStringList filesToClean() const;

private slots:
    void testConstruction();
    void testStaticOil();
};
