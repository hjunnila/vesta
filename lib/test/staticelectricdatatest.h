#pragma once

class StaticElectricDataTest : public QObject
{
    Q_OBJECT

private slots:
    void testNormalConstructor();
    void testSetConstructor();
    void testCopyConstructor();
    void testAssignmentOperator();
    void testSetters();
};
