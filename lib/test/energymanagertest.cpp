#include "energymanagertest.h"
#include "energymanager_p.h"
#include "energymanager.h"
#include "mockelectric.h"
#include "mockoil.h"

void EnergyManagerTest::testConstruction()
{
    EnergyManager em;
    QCOMPARE(em.running(), false);

    QVERIFY(em.d_ptr != NULL);
    QVERIFY(em.d_ptr->plandb != NULL);
    QVERIFY(em.d_ptr->usagedb != NULL);
    QVERIFY(em.d_ptr->planTimer == NULL);
    QVERIFY(em.oil() != NULL);
    QVERIFY(em.electric() != NULL);
}

void EnergyManagerTest::testFailSafePlan()
{
    EnergyPlanEntry e = EnergyManagerP::failSafePlan(EnergyPlanEntry(EnergyPlanEntry::currentHourStart(), EnergyPlanEntry::currentHourEnd(), 9.45, 10, 1, 2));
    QDateTime start = QDateTime::currentDateTime();
    start.setTime(QTime(start.time().hour(), 0, 0, 0));
    QCOMPARE(e.start(), start);
    QCOMPARE(e.end(), start.addSecs(3600));
    QCOMPARE(e.oil(), 9.45);
    QCOMPARE(e.energy(), 10.0);
    QCOMPARE(e.transfer(), 1.0);
    QCOMPARE(e.premium(), 2.0);
    QCOMPARE(e.planType(), EnergyPlanEntry::FailSafePlan);
}

void EnergyManagerTest::testManualElectricPlan()
{
    EnergyPlanEntry e = EnergyManagerP::manualElectricPlan(EnergyPlanEntry(EnergyPlanEntry::currentHourStart(), EnergyPlanEntry::currentHourEnd(), 9.45, 12, 1, 2));
    QDateTime start = QDateTime::currentDateTime();
    start.setTime(QTime(start.time().hour(), 0, 0, 0));
    QCOMPARE(e.start(), start);
    QCOMPARE(e.end(), start.addSecs(3600));
    QCOMPARE(e.oil(), 9.45);
    QCOMPARE(e.energy(), 12.0);
    QCOMPARE(e.transfer(), 1.0);
    QCOMPARE(e.premium(), 2.0);
    QCOMPARE(e.planType(), EnergyPlanEntry::ManualElectricPlan);
}

void EnergyManagerTest::testElectricPriorityForPlan()
{
    EnergyPlanEntry e = EnergyManagerP::failSafePlan(EnergyPlanEntry(EnergyPlanEntry::currentHourStart(), EnergyPlanEntry::currentHourEnd(), 9.45, 10, 1, 2));
    QCOMPARE(EnergyManagerP::electricPriorityForPlan(e), EnergySourcePriorityHigh);

    e = EnergyManagerP::manualElectricPlan(EnergyPlanEntry(EnergyPlanEntry::currentHourStart(), EnergyPlanEntry::currentHourEnd(), 9.45, 10, 1, 2));
    QCOMPARE(EnergyManagerP::electricPriorityForPlan(e), EnergySourcePriorityHigh);

    e = EnergyPlanEntry(QDateTime(), QDateTime(), 0, 0, 0, 0);
    QCOMPARE(EnergyManagerP::electricPriorityForPlan(e), EnergySourcePriorityHigh);

    e = EnergyPlanEntry(QDateTime(), QDateTime(), 1, 0, 0, 0);
    QCOMPARE(EnergyManagerP::electricPriorityForPlan(e), EnergySourcePriorityHigh);

    e = EnergyPlanEntry(QDateTime(), QDateTime(), 0, 1, 0, 0);
    QCOMPARE(EnergyManagerP::electricPriorityForPlan(e), EnergySourcePriorityLow);

    e = EnergyPlanEntry(QDateTime(), QDateTime(), 0, 1, 2, 3);
    QCOMPARE(EnergyManagerP::electricPriorityForPlan(e), EnergySourcePriorityLow);
}

