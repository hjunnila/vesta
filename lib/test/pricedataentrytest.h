#pragma once

class PriceDataEntryTest : public QObject
{
    Q_OBJECT

private slots:
    void testNormalConstructor();
    void testNordPoolConstructor();
    void testCopyConstructor();
    void testAssignmentOperator();
    void testCalculations();
};
