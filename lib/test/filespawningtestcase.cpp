#include "filespawningtestcase.h"

void FileSpawningTestCase::cleanFiles()
{
    QStringList fileList = filesToClean();
    foreach (QString fileName, fileList) {
        QFile file(fileName);
        if (file.exists()) {
            file.remove();
        }
    }
}

void FileSpawningTestCase::initTestCase()
{
    cleanFiles();
}

void FileSpawningTestCase::cleanupTestCase()
{
    cleanFiles();
}
