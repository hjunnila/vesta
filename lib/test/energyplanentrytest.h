#pragma once

class EnergyPlanEntryTest : public QObject
{
    Q_OBJECT

private slots:
    void testNormalConstructor();
    void testCopyConstructor();
    void testPlanConstructor();
    void testAssignmentOperator();
    void testComparisonOperator();
    void testCalculations();
    void testIsValid();
};
