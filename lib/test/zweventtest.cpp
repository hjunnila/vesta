#include "zweventtest.h"
#include "zwevent.h"

void ZWEventTest::testNormalEvent()
{
    OpenZWave::Notification ntf(OpenZWave::Notification::Type_ValueAdded);
    ntf.SetHomeNodeIdAndInstance(12345678, 1, 0x31337);

    ZWEvent ev(&ntf);
    QVERIFY(ev.home() == 12345678);
    QVERIFY(ev.node() == 1);
    QVERIFY(ev.vid() == 3963167672102813696);
    QVERIFY(ev.type() == OpenZWave::Notification::Type_ValueAdded);
    QVERIFY(ev.string().length() > 0);
    QVERIFY(ev.code() == 0);
}

void ZWEventTest::testControllerCommand()
{
    OpenZWave::Notification ntf(OpenZWave::Notification::Type_ControllerCommand);
    ntf.SetHomeNodeIdAndInstance(12345678, 1, 0x31337);
    ntf.SetNotification(69);

    ZWEvent ev(&ntf);
    QVERIFY(ev.home() == 12345678);
    QVERIFY(ev.node() == 1);
    QVERIFY(ev.vid() == 3963167672102813696);
    QVERIFY(ev.type() == OpenZWave::Notification::Type_ControllerCommand);
    QVERIFY(ev.string().length() > 0);
    QVERIFY(ev.code() == 69);
}

void ZWEventTest::testEventType()
{
    ZWEvent::registerZWEventType();
    QVERIFY(ZWEvent::ZWEventType() != -1);
}
