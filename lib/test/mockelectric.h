#pragma once
#include "electric.h"

class MockElectric : public Electric
{
    Q_OBJECT
    Q_DISABLE_COPY(MockElectric)

public:
    MockElectric(double price) : Electric(), m_price(price) { }
    virtual ~MockElectric() { }

    double pricePerKWh() const { return m_price; }

    double m_price;
};

