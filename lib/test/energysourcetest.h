#pragma once

class EnergySourceTest : public QObject
{
    Q_OBJECT
private slots:
    void testConstruction();
    void testUsageTime();
    void testPowerUsage();
};
