#pragma once

class DatabaseInfo;
class ElPriceDBTest : public FileSpawningTestCase
{
    Q_OBJECT

    DatabaseInfo dbinfo();
    QStringList filesToClean() const;

private slots:
    void testInit();
    void testInsert();
};
