#include "mockservice.h"
#include "servertest.h"
#include "server_p.h"
#include "server.h"

#define SETTINGS_FILE "servertest.json"

void ServerTest::testConstruction()
{
    Server s(Settings(SETTINGS_FILE));
    QCOMPARE(s.d_ptr->port, (quint16) 8002);
    QCOMPARE(s.d_ptr->certFile, QString("servertest.crt"));
    QCOMPARE(s.d_ptr->pKeyFile, QString("servertest.key"));

    int req = QMetaType::type("Request");
    QVERIFY(req != 0);
    QVERIFY(QMetaType::isRegistered(req));
    QCOMPARE(QMetaType::typeName(req), "Request");

    int res = QMetaType::type("Response");
    QVERIFY(req != 0);
    QVERIFY(QMetaType::isRegistered(res));
    QCOMPARE(QMetaType::typeName(res), "Response");
}

void ServerTest::testRegister()
{
    Server s(Settings(SETTINGS_FILE));

    MockService ms1;
    s.registerService(&ms1);
    QCOMPARE(s.d_ptr->services.size(), 1);

    MockService ms2;
    s.registerService(&ms2);
    QCOMPARE(s.d_ptr->services.size(), 2);
    s.registerService(&ms2);
    QCOMPARE(s.d_ptr->services.size(), 2);

    s.unregisterService(&ms1);
    QCOMPARE(s.d_ptr->services.size(), 1);
    s.unregisterService(&ms1);
    QCOMPARE(s.d_ptr->services.size(), 1);
    s.unregisterService(&ms2);
    QCOMPARE(s.d_ptr->services.size(), 0);
}
