QT                 += testlib sql dbus network
QT                 -= gui
TEMPLATE            = app
TARGET              = test
CONFIG             += precompile_header debug

PRECOMPILED_HEADER  = test-pc.h
OBJECTS_DIR         = obj

INCLUDEPATH        += .
INCLUDEPATH        += ../src

# Linux open-zwave location. Adjust if necessary.
INCLUDEPATH     += /usr/include/openzwave /usr/include/openzwave/command_classes

# Homebrew open-zwave location. Adjust if necessary.
macx:LIBS           += -L/usr/local/Cellar/open-zwave/1.2.919/lib
macx:INCLUDEPATH    += /usr/local/Cellar/open-zwave/1.2.919/include/openzwave
macx:INCLUDEPATH    += /usr/local/Cellar/open-zwave/1.2.919/include/openzwave/command_cla$

LIBS            += -lopenzwave
LIBS            += -lstdc++
LIBS            += -lvesta -L../src

# Comment this line if you get whining about C++11
DEFINES += _GLIBCXX_USE_CXX11_ABI=0

HEADERS += \
            devicemanagerservicetest.h \
            devicenodetest.h \
            electrictest.h \
            elpricedbtest.h \
            energymanagertest.h \
            energyplandbtest.h \
            energyplanentrytest.h \
            energysourcetest.h \
            energyusageentrytest.h \
            filespawningtestcase.h \
            mockelectric.h \
            mockdevicemanager.h \
            mockdevicenode.h \
            mocknodevalue.h \
            mockoil.h \
            mockservice.h \
            oiltest.h \
            pricedatatest.h \
            pricedataentrytest.h \
            requesttest.h \
            responsetest.h \
            servertest.h \
            servicetest.h \
            staticelectricdatatest.h \
            staticoildatatest.h \
            zweventtest.h

SOURCES += \
            devicemanagerservicetest.cpp \
            devicenodetest.cpp \
            electrictest.cpp \
            elpricedbtest.cpp \
            energymanagertest.cpp \
            energyplandbtest.cpp \
            energyplanentrytest.cpp \
            energysourcetest.cpp \
            energyusageentrytest.cpp \
            filespawningtestcase.cpp \
            main.cpp \
            mockdevicemanager.cpp \
            mockdevicenode.cpp \
            mocknodevalue.cpp \
            oiltest.cpp \
            pricedatatest.cpp \
            pricedataentrytest.cpp \
            requesttest.cpp \
            responsetest.cpp \
            servertest.cpp \
            servicetest.cpp \
            staticelectricdatatest.cpp \
            staticoildatatest.cpp \
            zweventtest.cpp
