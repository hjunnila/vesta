#pragma once

class DatabaseInfo;
class ElectricTest : public FileSpawningTestCase
{
    Q_OBJECT

    DatabaseInfo dbinfo();
    QStringList filesToClean() const;

private slots:
    void testConstruction();
    void testStaticElectric();
};
