#pragma once

class EnergyUsageEntryTest : public QObject
{
    Q_OBJECT

private slots:
    void testConstruction();
    void testCopyConstruction();
    void testAssignmentOperator();
};
