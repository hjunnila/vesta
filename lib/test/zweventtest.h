#pragma once

class ZWEventTest : public QObject
{
    Q_OBJECT

private slots:
    void testNormalEvent();
    void testControllerCommand();
    void testEventType();
};
