#pragma once
#include "devicenode.h"

class MockDeviceNode : public DeviceNode
{
    Q_OBJECT
    Q_DISABLE_COPY(MockDeviceNode)

public:
    MockDeviceNode(quint32 home, quint8 node);
    ~MockDeviceNode();

    QString manufacturer() const { return "acme"; }
    QString productName() const { return "booboo b gone"; }
    QString name() const { return "ouch"; }
    bool isAwake() const { return true; }
};
