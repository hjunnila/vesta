#pragma once

class StaticOilDataTest : public QObject
{
    Q_OBJECT

private slots:
    void testNormalConstructor();
    void testSetConstructor();
    void testCopyConstructor();
    void testAssignmentOperator();
    void testSetters();
};
