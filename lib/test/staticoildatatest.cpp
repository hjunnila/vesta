#include "staticoildatatest.h"
#include "staticoildata.h"

void StaticOilDataTest::testNormalConstructor()
{
    StaticOilData e;
    QVERIFY(e.litrePrice() == "0");
    QVERIFY(e.efficiency() == "0");
    QVERIFY(e.kWhPerLitre() == "0");
    QVERIFY(e.switchDevice() == "");
    QVERIFY(e.switchValue() == "");
    QVERIFY(e.powerDevice() == "");
    QVERIFY(e.powerValue() == "");
}

void StaticOilDataTest::testSetConstructor()
{
    StaticOilData e("1.0", "2.0", "3.0", "device", "switch", "monitor", "power");

    QVERIFY(e.litrePrice() == "1.0");
    QVERIFY(e.efficiency() == "2.0");
    QVERIFY(e.kWhPerLitre() == "3.0");
    QVERIFY(e.switchDevice() == "device");
    QVERIFY(e.switchValue() == "switch");
    QVERIFY(e.powerDevice() == "monitor");
    QVERIFY(e.powerValue() == "power");
}

void StaticOilDataTest::testCopyConstructor()
{
    StaticOilData e("1.0", "2.0", "3.0", "device", "switch", "monitor", "power");

    StaticOilData e2(e);
    QVERIFY(e2.litrePrice() == "1.0");
    QVERIFY(e2.efficiency() == "2.0");
    QVERIFY(e2.kWhPerLitre() == "3.0");
    QVERIFY(e2.switchDevice() == "device");
    QVERIFY(e2.switchValue() == "switch");
    QVERIFY(e2.powerDevice() == "monitor");
    QVERIFY(e2.powerValue() == "power");
}

void StaticOilDataTest::testAssignmentOperator()
{
    StaticOilData e("1.0", "2.0", "3.0", "device", "switch", "monitor", "power");

    StaticOilData e2;
    e2 = e;
    QVERIFY(e2.litrePrice() == "1.0");
    QVERIFY(e2.efficiency() == "2.0");
    QVERIFY(e2.kWhPerLitre() == "3.0");
    QVERIFY(e2.switchDevice() == "device");
    QVERIFY(e2.switchValue() == "switch");
    QVERIFY(e2.powerDevice() == "monitor");
    QVERIFY(e2.powerValue() == "power");
}

void StaticOilDataTest::testSetters()
{
    StaticOilData e;
    e.setLitrePrice("1.0");
    e.setEfficiency("2.0");
    e.setKWhPerLitre("3.0");
    e.setSwitchDevice("device");
    e.setSwitchValue("switch");
    e.setPowerDevice("monitor");
    e.setPowerValue("power");

    QVERIFY(e.litrePrice() == "1.0");
    QVERIFY(e.litrePriceDouble() == 1.0);
    QVERIFY(e.efficiency() == "2.0");
    QVERIFY(e.efficiencyDouble() == 2.0);
    QVERIFY(e.kWhPerLitre() == "3.0");
    QVERIFY(e.kWhPerLitreDouble() == 3.0);
    QVERIFY(e.switchDevice() == "device");
    QVERIFY(e.switchValue() == "switch");
    QVERIFY(e.powerDevice() == "monitor");
    QVERIFY(e.powerValue() == "power");
}
