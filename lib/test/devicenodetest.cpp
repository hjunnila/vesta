#include "devicenodetest.h"
#include "devicenode_p.h"
#include "devicenode.h"

void DeviceNodeTest::testConstruction()
{
    DeviceNode dn(31337, 123, this);
    QCOMPARE(dn.home(), (uint32) 31337);
    QCOMPARE(dn.node(), (uint8) 123);
    QVERIFY(dn.d_ptr != NULL);
}

void DeviceNodeTest::testComparison()
{
    DeviceNode dn1(31337, 123, this);
    DeviceNode dn2(31337, 123, this);
    DeviceNode dn3(80085, 123, this);
    DeviceNode dn4(31337, 5, this);
    DeviceNode dn5(31337, 123, NULL);

    QVERIFY(dn1 == dn2);
    QVERIFY(!(dn1 == dn3));
    QVERIFY(!(dn1 == dn4));
    QVERIFY(dn1 == dn5);
}

void DeviceNodeTest::testValues()
{
    uint64 vid = 7628736428736487;
    uint64 invalid = 1234567890123456;

    DeviceNode dn(31337, 123, this);
    QCOMPARE(dn.values().size(), 0);

    dn.d_ptr->addValue(vid);
    QCOMPARE(dn.values().size(), 1);
    NodeValue* nv = dn.value(invalid);
    QVERIFY(nv == NULL);
    nv = dn.value(vid);
    QVERIFY(nv != NULL);

    dn.d_ptr->removeValue(invalid);
    QCOMPARE(dn.values().size(), 1);
    dn.d_ptr->removeValue(vid);
    QCOMPARE(dn.values().size(), 0);

    dn.d_ptr->addValue(vid);
    QCOMPARE(dn.values().size(), 1);
}
