#pragma once
#include "devicemanager.h"

class MockDeviceManager : public DeviceManager
{
    Q_OBJECT
public:
    MockDeviceManager();
    ~MockDeviceManager();

    void addDevice();
    int m_addDeviceCalled;
};
