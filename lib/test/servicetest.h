#pragma once
#include "service.h"

class ServiceTest : public QObject
{
    Q_OBJECT

private slots:
    void testAddService();
    void testHandleRequestNoHandler();
    void testHandleRequest();
    void testHandleRequestInvalidPathMethod();
};

class FooSvc : public Service
{
    Q_OBJECT

public:
    FooSvc(QObject* parent = 0);
    ~FooSvc();

public slots:
    Response get(const Request& request);
    Response post(const Request& request);

public:
    int getCalled;
    int postCalled;
};

