#pragma once

class RequestTest : public QObject
{
    Q_OBJECT

private slots:
    void testNormalConstructor();
    void testNetworkConstructor();
    void testCopyConstructor();
    void testAssignmentOperator();
};
