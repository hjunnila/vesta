#include "pricedataentry.h"
#include "elpricedbtest.h"
#include "elpricedb_p.h"
#include "elpricedb.h"
#include "pricedata.h"

#define DBFILE "elpricedbtest.db"

DatabaseInfo ElPriceDBTest::dbinfo()
{
    DatabaseInfo info;
    info.dbName = DBFILE;
    info.dbDriver = "QSQLITE";
    return info;
}

QStringList ElPriceDBTest::filesToClean() const
{
    return QStringList() << DBFILE;
}

void ElPriceDBTest::testInit()
{
    ElPriceDB db(dbinfo());
    QVERIFY(db.d_ptr != NULL);
    QVERIFY(db.d_ptr->parent == &db);

    QStringList tables = db.essentialTables();
    QVERIFY(tables.size() == 1);
    QVERIFY(tables[0] == "electric_price");
    QVERIFY(db.open());
}

void ElPriceDBTest::testInsert()
{
    QList <PriceDataEntry> entries;
    for (int i = 0; i < 24; i++) {
        if (i < 23) {
            PriceDataEntry e(QDateTime(QDate(2017, 5, 15), QTime(i, 0, 0)),
                             QDateTime(QDate(2017, 5, 15), QTime(i + 1, 0, 0)), i + 1);
            entries << e;
        } else {
            PriceDataEntry e(QDateTime(QDate(2017, 5, 15), QTime(i, 0, 0)),
                             QDateTime(QDate(2017, 5, 16), QTime(0, 0, 0)), i + 1);
            entries << e;
        }
    }

    PriceData pd("FI", // area
                 QDateTime(QDate(2017, 5, 15), QTime(0, 0, 0)), // start
                 QDateTime(QDate(2017, 5, 16), QTime(0, 0, 0)), // end
                 QDateTime(QDate(2017, 5, 15), QTime(15, 1, 0)), // updated
                 entries);

    ElPriceDB db(dbinfo());
    db.onPriceDataUpdated(pd);

    for (int i = 0; i < 24; i++) {
        PriceDataEntry e = db.priceDataEntry("FI", QDateTime(QDate(2017, 5, 15), QTime(i, 5, 0)));
        QVERIFY(e.energy() == i + 1);
    }
}
