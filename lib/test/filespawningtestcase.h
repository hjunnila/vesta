#pragma once

class FileSpawningTestCase : public QObject
{
    Q_OBJECT

    virtual QStringList filesToClean() const = 0;
    void cleanFiles();

private slots:
    void initTestCase();
    void cleanupTestCase();
};
