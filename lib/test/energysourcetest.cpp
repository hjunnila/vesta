#include "energysourcetest.h"
#include "energysource_p.h"
#include "energysource.h"
#include "oil.h"

void EnergySourceTest::testConstruction()
{
    EnergySource* es = new Oil;
    QCOMPARE(es->d_ptr->currentState, false);
    QCOMPARE(es->d_ptr->secondsOn, 0);
    QCOMPARE(es->d_ptr->currentPower, 0);
    QCOMPARE(es->d_ptr->cumulativePower, 0);
    QCOMPARE(es->d_ptr->numberOfSamples, 0);
    delete es;
}

void EnergySourceTest::testUsageTime()
{
    EnergySource* es = new Oil;

    es->logUsageChange(true);
    QTest::qWait(1001);
    es->logUsageChange(false);
    QTest::qWait(500);
    es->logUsageChange(true);
    QTest::qWait(1001);
    es->logUsageChange(false);

    QDateTime beforeReset = QDateTime::currentDateTime();
    QTest::qWait(1);
    QCOMPARE(es->cumulativeOnTimeAndReset(), 2);
    QCOMPARE(es->d_ptr->secondsOn, 0);
    QVERIFY(es->d_ptr->lastStateChange > beforeReset);
    QVERIFY(es->d_ptr->logStartDate > beforeReset);
    QVERIFY(es->logStartDate() > beforeReset);

    delete es;
}

void EnergySourceTest::testPowerUsage()
{
    EnergySource* es = new Oil;

    QCOMPARE(es->averagePowerConsumptionWhileOnAndReset(), 0);

    es->logPowerUsage(100);
    es->logPowerUsage(200);
    es->logPowerUsage(300);
    es->logPowerUsage(400);
    es->logPowerUsage(500);
    es->logPowerUsage(600);
    es->logPowerUsage(700);
    es->logPowerUsage(800);
    es->logPowerUsage(900);
    es->logPowerUsage(1000);
    QCOMPARE(es->averagePowerConsumptionWhileOnAndReset(), 0);

    es->logUsageChange(true);
    es->logPowerUsage(100);
    es->logPowerUsage(200);
    es->logPowerUsage(300);
    es->logPowerUsage(400);
    es->logPowerUsage(500);
    es->logPowerUsage(600);
    es->logPowerUsage(700);
    es->logPowerUsage(800);
    es->logPowerUsage(900);
    es->logPowerUsage(1000);
    QCOMPARE(es->averagePowerConsumptionWhileOnAndReset(), 550);
    QCOMPARE(es->d_ptr->cumulativePower, 1000);
    QCOMPARE(es->d_ptr->numberOfSamples, 1);

    delete es;
}
