#include "energyusageentrytest.h"
#include "energyusageentry.h"

void EnergyUsageEntryTest::testConstruction()
{
    QDateTime start = QDateTime::currentDateTime();
    QDateTime end = start.addSecs(60 * 60);
    EnergyUsageEntry eue(start, end, 120, 5000, 300, 150, "kaboom");
    QCOMPARE(eue.start(), start);
    QCOMPARE(eue.end(), end);
    QCOMPARE(eue.elSeconds(), 120);
    QCOMPARE(eue.elAveragePower(), 5000);
    QCOMPARE(eue.oilSeconds(), 300);
    QCOMPARE(eue.oilAveragePower(), 150);
    QCOMPARE(eue.error(), QString("kaboom"));
}

void EnergyUsageEntryTest::testCopyConstruction()
{
    QDateTime start = QDateTime::currentDateTime();
    QDateTime end = start.addSecs(60 * 60);
    EnergyUsageEntry eue(start, end, 120, 5000, 300, 150, "kaboom");

    EnergyUsageEntry eue2(eue);
    QCOMPARE(eue2.start(), start);
    QCOMPARE(eue2.end(), end);
    QCOMPARE(eue2.elSeconds(), 120);
    QCOMPARE(eue2.elAveragePower(), 5000);
    QCOMPARE(eue2.oilSeconds(), 300);
    QCOMPARE(eue2.oilAveragePower(), 150);
    QCOMPARE(eue2.error(), QString("kaboom"));
}

void EnergyUsageEntryTest::testAssignmentOperator()
{
    QDateTime start = QDateTime::currentDateTime();
    QDateTime end = start.addSecs(60 * 60);
    EnergyUsageEntry eue(start, end, 120, 5000, 300, 150, "kaboom");

    EnergyUsageEntry eue2;
    eue2 = eue;
    QCOMPARE(eue2.start(), start);
    QCOMPARE(eue2.end(), end);
    QCOMPARE(eue2.elSeconds(), 120);
    QCOMPARE(eue2.elAveragePower(), 5000);
    QCOMPARE(eue2.oilSeconds(), 300);
    QCOMPARE(eue2.oilAveragePower(), 150);
    QCOMPARE(eue2.error(), QString("kaboom"));
}
