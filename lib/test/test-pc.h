// Expose private members for unit tests to tamper with
#define private public
#define protected public

#include <QtTest/QtTest>
#include "../src/vesta-pc.h"
#include "filespawningtestcase.h"
