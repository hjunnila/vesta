#include "staticelectricdatatest.h"
#include "staticelectricdata.h"

void StaticElectricDataTest::testNormalConstructor()
{
    StaticElectricData e;
    QVERIFY(e.energyTax() == "0");
    QVERIFY(e.transfer() == "0");
    QVERIFY(e.transferTax() == "0");
    QVERIFY(e.premium() == "0");
    QVERIFY(e.premiumTax() == "0");
    QVERIFY(e.switchDevice() == "");
    QVERIFY(e.switchValue() == "");
    QVERIFY(e.powerDevice() == "");
    QVERIFY(e.powerValue() == "");
    QVERIFY(e.area() == "");
}

void StaticElectricDataTest::testSetConstructor()
{
    StaticElectricData e("1.0", "2.0", "3.0", "4.0", "5.0", "device", "switch", "monitor", "power", "fi");
    QVERIFY(e.energyTax() == "1.0");
    QVERIFY(e.transfer() == "2.0");
    QVERIFY(e.transferTax() == "3.0");
    QVERIFY(e.premium() == "4.0");
    QVERIFY(e.premiumTax() == "5.0");
    QVERIFY(e.switchDevice() == "device");
    QVERIFY(e.switchValue() == "switch");
    QVERIFY(e.powerDevice() == "monitor");
    QVERIFY(e.powerValue() == "power");
    QVERIFY(e.area() == "fi");
}

void StaticElectricDataTest::testCopyConstructor()
{
    StaticElectricData e("1.0", "2.0", "3.0", "4.0", "5.0", "device", "switch", "monitor", "power", "fi");

    StaticElectricData e2(e);
    QVERIFY(e2.energyTax() == "1.0");
    QVERIFY(e2.transfer() == "2.0");
    QVERIFY(e2.transferTax() == "3.0");
    QVERIFY(e2.premium() == "4.0");
    QVERIFY(e2.premiumTax() == "5.0");
    QVERIFY(e2.switchDevice() == "device");
    QVERIFY(e2.switchValue() == "switch");
    QVERIFY(e2.powerDevice() == "monitor");
    QVERIFY(e2.powerValue() == "power");
    QVERIFY(e2.area() == "fi");
}

void StaticElectricDataTest::testAssignmentOperator()
{
    StaticElectricData e("1.0", "2.0", "3.0", "4.0", "5.0", "device", "switch", "monitor", "power", "fi");

    StaticElectricData e2;
    e2 = e;
    QVERIFY(e2.energyTax() == "1.0");
    QVERIFY(e2.transfer() == "2.0");
    QVERIFY(e2.transferTax() == "3.0");
    QVERIFY(e2.premium() == "4.0");
    QVERIFY(e2.premiumTax() == "5.0");
    QVERIFY(e2.switchDevice() == "device");
    QVERIFY(e2.switchValue() == "switch");
    QVERIFY(e2.powerDevice() == "monitor");
    QVERIFY(e2.powerValue() == "power");
    QVERIFY(e2.area() == "fi");
}

void StaticElectricDataTest::testSetters()
{
    StaticElectricData e;
    e.setEnergyTax("1.0");
    e.setTransfer("2.0");
    e.setTransferTax("3.0");
    e.setPremium("4.0");
    e.setPremiumTax("5.0");
    e.setSwitchDevice("foo");
    e.setSwitchValue("bar");
    e.setPowerDevice("foobar");
    e.setPowerValue("xyzzy");
    e.setArea("se");

    QVERIFY(e.energyTax() == "1.0");
    QVERIFY(e.energyTaxDouble() == 1.0);
    QVERIFY(e.transfer() == "2.0");
    QVERIFY(e.transferDouble() == 2.0);
    QVERIFY(e.transferTax() == "3.0");
    QVERIFY(e.transferTaxDouble() == 3.0);
    QVERIFY(e.premium() == "4.0");
    QVERIFY(e.premiumDouble() == 4.0);
    QVERIFY(e.premiumTax() == "5.0");
    QVERIFY(e.premiumTaxDouble() == 5.0);
    QVERIFY(e.switchDevice() == "foo");
    QVERIFY(e.switchValue() == "bar");
    QVERIFY(e.powerDevice() == "foobar");
    QVERIFY(e.powerValue() == "xyzzy");
    QVERIFY(e.area() == "se");
}
