#include "devicemanagerservicetest.h"
#include "devicenodetest.h"
#include "electrictest.h"
#include "elpricedbtest.h"
#include "energymanagertest.h"
#include "energyplandbtest.h"
#include "energyplanentrytest.h"
#include "energyusageentrytest.h"
#include "energysourcetest.h"
#include "oiltest.h"
#include "pricedatatest.h"
#include "pricedataentrytest.h"
#include "requesttest.h"
#include "responsetest.h"
#include "servertest.h"
#include "servicetest.h"
#include "staticelectricdatatest.h"
#include "staticoildatatest.h"
#include "zweventtest.h"

int main(int argc, char *argv[])
{
    QList <QObject*> tests;
    tests << new DeviceManagerServiceTest;
    tests << new DeviceNodeTest;
    tests << new ElectricTest;
    tests << new ElPriceDBTest;
    tests << new EnergyManagerTest;
    tests << new EnergyPlanDBTest;
    tests << new EnergyPlanEntryTest;
    tests << new EnergyUsageEntryTest;
    tests << new EnergySourceTest;
    tests << new OilTest;
    tests << new PriceDataTest;
    tests << new PriceDataEntryTest;
    tests << new RequestTest;
    tests << new ResponseTest;
    tests << new ServerTest;
    tests << new ServiceTest;
    tests << new StaticElectricDataTest;
    tests << new StaticOilDataTest;
    tests << new ZWEventTest;

    QCoreApplication coreApp(argc, argv);

    while (tests.count()) {
        QObject* test = tests.takeFirst();
        int result = QTest::qExec(test, argc, argv);
        if (result != 0) {
            return result;
        }

        delete test;
    }

    return 0;
}
