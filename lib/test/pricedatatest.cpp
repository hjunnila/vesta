#include "pricedatatest.h"
#include "pricedata.h"

void PriceDataTest::testNormalConstructor()
{
    PriceData pd;
    QVERIFY(pd.area() == QString());
    QVERIFY(pd.prices().count() == 0);
    QVERIFY(pd.start().isValid() == false);
    QVERIFY(pd.end().isValid() == false);
    QVERIFY(pd.updated().isValid() == false);
    QVERIFY(pd.error() == QSqlError::NoError);
}

void PriceDataTest::testNordPoolConstructor()
{
    QList <PriceDataEntry> entries;
    entries << PriceDataEntry(QDateTime::fromTime_t(100), QDateTime::fromTime_t(101), 1.0);
    entries << PriceDataEntry(QDateTime::fromTime_t(101), QDateTime::fromTime_t(102), 2.0);
    entries << PriceDataEntry(QDateTime::fromTime_t(102), QDateTime::fromTime_t(103), 3.0);

    QDateTime now = QDateTime::currentDateTime();

    PriceData pd("FI", QDateTime::fromTime_t(100), QDateTime::fromTime_t(200), now, entries);
    QVERIFY(pd.area() == "FI");
    QVERIFY(pd.start() == QDateTime::fromTime_t(100));
    QVERIFY(pd.end() == QDateTime::fromTime_t(200));
    QVERIFY(pd.updated() == now);
    QVERIFY(pd.error() == QSqlError::NoError);
    QVERIFY(pd.prices().count() == 3);

    QVERIFY(pd.prices()[0].start() == QDateTime::fromTime_t(100));
    QVERIFY(pd.prices()[0].end() == QDateTime::fromTime_t(101));
    QVERIFY(pd.prices()[0].energy() == 1.0);

    QVERIFY(pd.prices()[1].start() == QDateTime::fromTime_t(101));
    QVERIFY(pd.prices()[1].end() == QDateTime::fromTime_t(102));
    QVERIFY(pd.prices()[1].energy() == 2.0);

    QVERIFY(pd.prices()[2].start() == QDateTime::fromTime_t(102));
    QVERIFY(pd.prices()[2].end() == QDateTime::fromTime_t(103));
    QVERIFY(pd.prices()[2].energy() == 3.0);
}

void PriceDataTest::testErrorConstructor()
{
    PriceData pd("FI", QSqlError("foo", "bar", QSqlError::TransactionError));
    QVERIFY(pd.area() == "FI");
    QVERIFY(pd.error() == QSqlError::TransactionError);
}

void PriceDataTest::testCopyConstructor()
{
    QList <PriceDataEntry> entries;
    entries << PriceDataEntry(QDateTime::fromTime_t(100), QDateTime::fromTime_t(101), 1.0);
    entries << PriceDataEntry(QDateTime::fromTime_t(101), QDateTime::fromTime_t(102), 2.0);
    entries << PriceDataEntry(QDateTime::fromTime_t(102), QDateTime::fromTime_t(103), 3.0);

    QDateTime now = QDateTime::currentDateTime();

    PriceData pd("NO", QDateTime::fromTime_t(100), QDateTime::fromTime_t(200), now, entries);
    PriceData pd2(pd);
    QVERIFY(pd2.area() == "NO");
    QVERIFY(pd2.start() == QDateTime::fromTime_t(100));
    QVERIFY(pd2.end() == QDateTime::fromTime_t(200));
    QVERIFY(pd2.updated() == now);
    QVERIFY(pd2.error() == QSqlError::NoError);
    QVERIFY(pd2.prices().count() == 3);

    QVERIFY(pd2.prices()[0].start() == QDateTime::fromTime_t(100));
    QVERIFY(pd2.prices()[0].end() == QDateTime::fromTime_t(101));
    QVERIFY(pd2.prices()[0].energy() == 1.0);

    QVERIFY(pd2.prices()[1].start() == QDateTime::fromTime_t(101));
    QVERIFY(pd2.prices()[1].end() == QDateTime::fromTime_t(102));
    QVERIFY(pd2.prices()[1].energy() == 2.0);

    QVERIFY(pd2.prices()[2].start() == QDateTime::fromTime_t(102));
    QVERIFY(pd2.prices()[2].end() == QDateTime::fromTime_t(103));
    QVERIFY(pd2.prices()[2].energy() == 3.0);
}

void PriceDataTest::testAssignmentOperator()
{
    QList <PriceDataEntry> entries;
    entries << PriceDataEntry(QDateTime::fromTime_t(100), QDateTime::fromTime_t(101), 1.0);
    entries << PriceDataEntry(QDateTime::fromTime_t(101), QDateTime::fromTime_t(102), 2.0);
    entries << PriceDataEntry(QDateTime::fromTime_t(102), QDateTime::fromTime_t(103), 3.0);

    QDateTime now = QDateTime::currentDateTime();

    PriceData pd("SE", QDateTime::fromTime_t(100), QDateTime::fromTime_t(200), now, entries);

    PriceData pd2;
    pd2 = pd;
    QVERIFY(pd2.area() == "SE");
    QVERIFY(pd2.start() == QDateTime::fromTime_t(100));
    QVERIFY(pd2.end() == QDateTime::fromTime_t(200));
    QVERIFY(pd2.updated() == now);
    QVERIFY(pd2.error() == QSqlError::NoError);
    QVERIFY(pd2.prices().count() == 3);

    QVERIFY(pd2.prices()[0].start() == QDateTime::fromTime_t(100));
    QVERIFY(pd2.prices()[0].end() == QDateTime::fromTime_t(101));
    QVERIFY(pd2.prices()[0].energy() == 1.0);

    QVERIFY(pd2.prices()[1].start() == QDateTime::fromTime_t(101));
    QVERIFY(pd2.prices()[1].end() == QDateTime::fromTime_t(102));
    QVERIFY(pd2.prices()[1].energy() == 2.0);

    QVERIFY(pd2.prices()[2].start() == QDateTime::fromTime_t(102));
    QVERIFY(pd2.prices()[2].end() == QDateTime::fromTime_t(103));
    QVERIFY(pd2.prices()[2].energy() == 3.0);
}

void PriceDataTest::testApplyCosts()
{
    QList <PriceDataEntry> entries;
    entries << PriceDataEntry(QDateTime::fromTime_t(100), QDateTime::fromTime_t(101), 1.0);
    entries << PriceDataEntry(QDateTime::fromTime_t(101), QDateTime::fromTime_t(102), 2.0);
    entries << PriceDataEntry(QDateTime::fromTime_t(102), QDateTime::fromTime_t(103), 3.0);

    QDateTime now = QDateTime::currentDateTime();
    PriceData pd("FI", QDateTime::fromTime_t(100), QDateTime::fromTime_t(200), now, entries);
    pd.applyCosts(4.0, 5.0, 6.0, 7.0, 8.0);

    QVERIFY(pd.prices()[0].energy() == 1.0);
    QVERIFY(pd.prices()[0].energyTax() == 4.0);
    QVERIFY(pd.prices()[0].transfer() == 5.0);
    QVERIFY(pd.prices()[0].transferTax() == 6.0);
    QVERIFY(pd.prices()[0].premium() == 7.0);
    QVERIFY(pd.prices()[0].premiumTax() == 8.0);

    QVERIFY(pd.prices()[1].energy() == 2.0);
    QVERIFY(pd.prices()[1].energyTax() == 4.0);
    QVERIFY(pd.prices()[1].transfer() == 5.0);
    QVERIFY(pd.prices()[1].transferTax() == 6.0);
    QVERIFY(pd.prices()[1].premium() == 7.0);
    QVERIFY(pd.prices()[1].premiumTax() == 8.0);

    QVERIFY(pd.prices()[2].energy() == 3.0);
    QVERIFY(pd.prices()[2].energyTax() == 4.0);
    QVERIFY(pd.prices()[2].transfer() == 5.0);
    QVERIFY(pd.prices()[2].transferTax() == 6.0);
    QVERIFY(pd.prices()[2].premium() == 7.0);
    QVERIFY(pd.prices()[2].premiumTax() == 8.0);
}
