#include "energyplandbtest.h"
#include "energyplanentry.h"
#include "energyplandb_p.h"
#include "energyplandb.h"

#define DBFILE "energyplandbtest.db"

DatabaseInfo EnergyPlanDBTest::dbinfo()
{
    DatabaseInfo info;
    info.dbName = DBFILE;
    info.dbDriver = "QSQLITE";
    return info;
}

QStringList EnergyPlanDBTest::filesToClean() const
{
    return QStringList() << DBFILE;
}

void EnergyPlanDBTest::testConstruction()
{
    EnergyPlanDB epdb(dbinfo());
    QVERIFY(epdb.d_ptr != NULL);
    QVERIFY(epdb.d_ptr->parent == &epdb);
    QCOMPARE(epdb.essentialTables().size(), 1);
    QCOMPARE(epdb.essentialTables()[0], QString("energy_plan"));
    QVERIFY(epdb.open());

    QStringList tables = epdb.database().tables();
    QCOMPARE(tables.size(), 1);
    QCOMPARE(tables[0], QString("energy_plan"));
}

void EnergyPlanDBTest::testEnergyPlanQueries()
{
    EnergyPlanDB epdb(dbinfo());

    QVERIFY(epdb.d_ptr->storeEnergyPlanEntry(EnergyPlanEntry()) == false);

    QDateTime todayStart = QDateTime::currentDateTime();
    todayStart.setTime(QTime(0, 0, 0));

    QList <EnergyPlanEntry> list;
    for (int i = 0; i < 24; i++) {
        QDateTime start = todayStart.addSecs(i * 60 * 60);
        QDateTime end = start.addSecs(60 * 60);
        EnergyPlanEntry e(start, end, 2 * i, 3 * i, 4 * i, 5 * i);
        list << e;
    }

    QVERIFY(epdb.storeEnergyPlanEntries(list));

    QList <EnergyPlanEntry> stored = epdb.energyPlanEntriesForDate(todayStart.date());
    QCOMPARE(stored.size(), 24);
    foreach (EnergyPlanEntry e, stored) {
        QVERIFY(list.contains(e));
    }

    EnergyPlanEntry e = epdb.energyPlanEntryForDate(todayStart);
    QCOMPARE(e, list[0]);
}

void EnergyPlanDBTest::testEnergyPlanEntryFromRecord()
{
    QSqlRecord rec;

    QDateTime start = QDateTime::currentDateTime();
    QDateTime end = start.addSecs(60 * 60);

    rec.append(QSqlField("start", QVariant::DateTime));
    rec.append(QSqlField("end", QVariant::DateTime));
    rec.append(QSqlField("oil", QVariant::String));
    rec.append(QSqlField("energy", QVariant::String));
    rec.append(QSqlField("transfer", QVariant::String));

    rec.setValue("start", start);
    rec.setValue("end", end);
    rec.setValue("oil", "10");
    rec.setValue("energy", "9");
    rec.setValue("transfer", "8");

    EnergyPlanEntry entry = EnergyPlanDBP::energyPlanEntryFromRecord(rec);
    QVERIFY(entry.isValid() == false);
    QCOMPARE(entry.oil(), (double) -1);
    QCOMPARE(entry.energy(), (double) -1);
    QCOMPARE(entry.premium(), (double) -1);

    rec.append(QSqlField("premium", QVariant::String));
    rec.setValue("premium", "7");
    entry = EnergyPlanDBP::energyPlanEntryFromRecord(rec);
    QVERIFY(entry.isValid() == true);
    QCOMPARE(entry.start(), start);
    QCOMPARE(entry.end(), end);
    QCOMPARE(entry.oil(), (double) 10);
    QCOMPARE(entry.energy(), (double) 9);
    QCOMPARE(entry.transfer(), (double) 8);
    QCOMPARE(entry.premium(), (double) 7);
}
