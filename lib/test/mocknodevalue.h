#pragma once
#include "nodevalue.h"

class MockNodeValue : public NodeValue
{
    Q_OBJECT
    Q_DISABLE_COPY(MockNodeValue)

public:
    MockNodeValue(quint64 vid, quint32 home);
    ~MockNodeValue();

    QVariant currentValue() const { return QString("bar"); }
    QString label() const { return "foo"; }
    bool readOnly() const { return false; }
    QString type() const { return "String"; }
    QString units() const { return "watts"; }
};
