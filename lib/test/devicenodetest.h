#pragma once

class DeviceNodeTest : public QObject
{
    Q_OBJECT
private slots:
    void testConstruction();
    void testComparison();
    void testValues();
};
