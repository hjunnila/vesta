#include "staticelectricdata.h"
#include "electrictest.h"
#include "electric.h"
#include "electric_p.h"

#define DBFILE "electrictest.db"

DatabaseInfo ElectricTest::dbinfo()
{
    DatabaseInfo info;
    info.dbName = DBFILE;
    info.dbDriver = "QSQLITE";
    return info;
}

QStringList ElectricTest::filesToClean() const
{
    return QStringList() << DBFILE;
}

void ElectricTest::testConstruction()
{
    Electric el(dbinfo());
    QVERIFY(el.d_ptr != NULL);
    QVERIFY(el.d_ptr->dynamicDB != NULL);
    QVERIFY(el.d_ptr->staticDB != NULL);
}

void ElectricTest::testStaticElectric()
{
    Electric el(dbinfo());
    StaticElectricData d1("0.1", "1.0", "0.2", "2.0", "0.3", "device", "switch", "monitor", "power", "fi");
    el.setStaticElectricData(d1);
    QCOMPARE(el.switchDevice(), QString("device"));
    QCOMPARE(el.switchValue(), QString("switch"));
    QCOMPARE(el.powerDevice(), QString("monitor"));
    QCOMPARE(el.powerValue(), QString("power"));

    StaticElectricData d2 = el.staticElectricData();
    QVERIFY(d2.energyTax() == "0.1");
    QVERIFY(d2.transfer() == "1.0");
    QVERIFY(d2.transferTax() == "0.2");
    QVERIFY(d2.premium() == "2.0");
    QVERIFY(d2.premiumTax() == "0.3");
    QVERIFY(d2.switchDevice() == "device");
    QVERIFY(d2.switchValue() == "switch");
    QVERIFY(d2.powerDevice() == "monitor");
    QVERIFY(d2.powerValue() == "power");
    QVERIFY(d2.area() == "fi");
}
