#pragma once

class ResponseTest : public QObject
{
    Q_OBJECT

private slots:
    void testNormalConstructor();
    void testCopyConstructor();
    void testAssignmentOperator();
    void testStaticResponses();
    void testSetJsonBody();
    void testResponseData();
};
