#pragma once

class PriceDataTest : public QObject
{
    Q_OBJECT

private slots:
    void testNormalConstructor();
    void testNordPoolConstructor();
    void testErrorConstructor();
    void testCopyConstructor();
    void testAssignmentOperator();
    void testApplyCosts();
};
