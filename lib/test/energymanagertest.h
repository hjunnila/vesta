#pragma once

class EnergyManagerTest : public QObject
{
    Q_OBJECT
private slots:
    void testConstruction();
    void testFailSafePlan();
    void testManualElectricPlan();
    void testElectricPriorityForPlan();
};
