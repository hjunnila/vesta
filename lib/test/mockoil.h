#pragma once
#include "oil.h"

class MockOil : public Oil
{
    Q_OBJECT
    Q_DISABLE_COPY(MockOil)

public:
    MockOil(double price) : Oil(), m_price(price) { }
    virtual ~MockOil() { }

    double pricePerKWh() const { return m_price; }

    double m_price;
};

