#pragma once
#include "service.h"

class MockService : public Service
{
    Q_OBJECT

public:
    MockService() : Service()
    {
    }

    virtual ~MockService()
    {
    }
};
