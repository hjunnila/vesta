#include "mockdevicemanager.h"

MockDeviceManager::MockDeviceManager()
    : DeviceManager()
    , m_addDeviceCalled(0)
{
}

MockDeviceManager::~MockDeviceManager()
{
}

void MockDeviceManager::addDevice()
{
    m_addDeviceCalled++;
}
