#include "energyplanentrytest.h"
#include "energyplanentry.h"

void EnergyPlanEntryTest::testNormalConstructor()
{
    EnergyPlanEntry e;
    QCOMPARE(e.isValid(), false);
    QCOMPARE(e.start().isValid(), true);
    QCOMPARE(e.end().isValid(), true);
    QCOMPARE(e.oil(), -1.0);
    QCOMPARE(e.energy(), -1.0);
    QCOMPARE(e.transfer(), -1.0);
    QCOMPARE(e.premium(), -1.0);
    QCOMPARE(e.planType(), EnergyPlanEntry::PriceBasedPlan);
}

void EnergyPlanEntryTest::testCopyConstructor()
{
    EnergyPlanEntry e(QDateTime::fromTime_t(100), QDateTime::fromTime_t(200), 1, 2, 3, 4, EnergyPlanEntry::FailSafePlan);

    EnergyPlanEntry e2(e);
    QCOMPARE(e2.start(), QDateTime::fromTime_t(100));
    QCOMPARE(e2.end(), QDateTime::fromTime_t(200));
    QCOMPARE(e2.oil(), 1.0);
    QCOMPARE(e2.energy(), 2.0);
    QCOMPARE(e2.transfer(), 3.0);
    QCOMPARE(e2.premium(), 4.0);
    QCOMPARE(e2.planType(), EnergyPlanEntry::FailSafePlan);
}

void EnergyPlanEntryTest::testPlanConstructor()
{
    EnergyPlanEntry e(QDateTime::fromTime_t(100), QDateTime::fromTime_t(200), 1, 2, 3, 4, EnergyPlanEntry::FailSafePlan);
    QCOMPARE(e.start(), QDateTime::fromTime_t(100));
    QCOMPARE(e.end(), QDateTime::fromTime_t(200));
    QCOMPARE(e.oil(), 1.0);
    QCOMPARE(e.energy(), 2.0);
    QCOMPARE(e.transfer(), 3.0);
    QCOMPARE(e.premium(), 4.0);
    QCOMPARE(e.planType(), EnergyPlanEntry::FailSafePlan);
}

void EnergyPlanEntryTest::testAssignmentOperator()
{
    EnergyPlanEntry e(QDateTime::fromTime_t(100), QDateTime::fromTime_t(200), 1, 2, 3, 4, EnergyPlanEntry::ManualElectricPlan);

    EnergyPlanEntry e2;
    e2 = e;
    QCOMPARE(e2.start(), QDateTime::fromTime_t(100));
    QCOMPARE(e2.end(), QDateTime::fromTime_t(200));
    QCOMPARE(e2.oil(), 1.0);
    QCOMPARE(e2.energy(), 2.0);
    QCOMPARE(e2.transfer(), 3.0);
    QCOMPARE(e2.premium(), 4.0);
    QCOMPARE(e2.planType(), EnergyPlanEntry::ManualElectricPlan);
}

void EnergyPlanEntryTest::testComparisonOperator()
{
    EnergyPlanEntry e(QDateTime::fromTime_t(100), QDateTime::fromTime_t(200), 1, 2, 3, 4);
    EnergyPlanEntry e1(QDateTime::fromTime_t(100), QDateTime::fromTime_t(200), 1, 2, 3, 4);
    EnergyPlanEntry e2(QDateTime::fromTime_t(99), QDateTime::fromTime_t(200), 1, 2, 3, 4);
    EnergyPlanEntry e3(QDateTime::fromTime_t(100), QDateTime::fromTime_t(201), 1, 2, 3, 4);
    EnergyPlanEntry e4(QDateTime::fromTime_t(100), QDateTime::fromTime_t(200), 10, 2, 3, 4);
    EnergyPlanEntry e5(QDateTime::fromTime_t(100), QDateTime::fromTime_t(200), 1, 20, 3, 4);
    EnergyPlanEntry e6(QDateTime::fromTime_t(100), QDateTime::fromTime_t(200), 1, 2, 30, 4);
    EnergyPlanEntry e7(QDateTime::fromTime_t(100), QDateTime::fromTime_t(200), 1, 2, 3, 40);

    QCOMPARE(e, e1);
    QCOMPARE((e == e2), false);
    QCOMPARE((e != e3), true);
    QCOMPARE((e == e4), false);
    QCOMPARE((e != e5), true);
    QCOMPARE((e == e6), false);
    QCOMPARE((e != e7), true);
}

void EnergyPlanEntryTest::testCalculations()
{
    EnergyPlanEntry e(QDateTime::fromTime_t(100), QDateTime::fromTime_t(200), 1, 2, 3, 4);
    QCOMPARE(e.oilPerKWh(), 1.0);
    QCOMPARE(e.electricPerKWh(), (2.0 + 3.0 + 4.0));
}

void EnergyPlanEntryTest::testIsValid()
{
    EnergyPlanEntry e;

    e = EnergyPlanEntry(QDateTime::fromTime_t(100), QDateTime(), 0, 0, 0, 0);
    QCOMPARE(e.isValid(), false);

    e = EnergyPlanEntry(QDateTime::fromTime_t(100), QDateTime::fromTime_t(200), -1, -1, -1, -1);
    QCOMPARE(e.isValid(), false);

    e = EnergyPlanEntry(QDateTime::fromTime_t(100), QDateTime::fromTime_t(200), 0, -1, -1, -1);
    QCOMPARE(e.isValid(), false);

    e = EnergyPlanEntry(QDateTime::fromTime_t(100), QDateTime::fromTime_t(200), 0, 0, -1, -1);
    QCOMPARE(e.isValid(), false);

    e = EnergyPlanEntry(QDateTime::fromTime_t(100), QDateTime::fromTime_t(200), 0, 0, 0, -1);
    QCOMPARE(e.isValid(), false);

    e = EnergyPlanEntry(QDateTime::fromTime_t(100), QDateTime::fromTime_t(200), 0, 0, 0, 0);
    QCOMPARE(e.isValid(), true);
}
