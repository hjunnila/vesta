#include "responsetest.h"
#include "response.h"
#include "http.h"

void ResponseTest::testNormalConstructor()
{
    Response r;
    QVERIFY(r.status() == 404);
    QVERIFY(r.body() == QString());
    QVERIFY(r.headers().count() == 1);
    QVERIFY(r.headers()[HTTP_HEADER_CONTENT_LENGTH] == "0");
}

void ResponseTest::testCopyConstructor()
{
    Response r;
    r.setStatus(200);
    r.setBody("xyzzy");
    r.setHeader("foo", "bar");

    Response r2(r);
    QVERIFY(r2.status() == 200);
    QVERIFY(r2.body() == "xyzzy");
    QVERIFY(r2.headers().count() == 2);
    QVERIFY(r2.headers()["foo"] == "bar");
    QVERIFY(r2.headers()[HTTP_HEADER_CONTENT_LENGTH] == "5");
}

void ResponseTest::testAssignmentOperator()
{
    Response r;
    r.setStatus(200);
    r.setBody("xyzzy");
    r.setHeader("foo", "bar");

    Response r2;
    r2 = r;
    QVERIFY(r2.status() == 200);
    QVERIFY(r2.body() == "xyzzy");
    QVERIFY(r2.headers().count() == 2);
    QVERIFY(r2.headers()["foo"] == "bar");
    QVERIFY(r2.headers()[HTTP_HEADER_CONTENT_LENGTH] == "5");
}

void ResponseTest::testStaticResponses()
{
    Response r;

    r = Response::notFound();
    QVERIFY(r.status() == 404);

    r = Response::badRequest();
    QVERIFY(r.status() == 400);
}

void ResponseTest::testSetJsonBody()
{
    Response r;
    QJsonObject root;
    root["foo"] = "bar";
    QJsonDocument doc(root);
    r.setJsonBody(doc);

    QVERIFY(r.headers()[HTTP_HEADER_CONTENT_TYPE] == HTTP_APPLICATION_JSON);
    QVERIFY(r.headers()[HTTP_HEADER_CONTENT_LENGTH] == "13");
    QVERIFY(r.body() == "{\"foo\":\"bar\"}");
}

void ResponseTest::testResponseData()
{
    Response r;
    r.setStatus(200);
    r.setBody("xyzzy");
    r.setHeader("foo", "bar");
    QVERIFY(r.responseData() == "HTTP/1.1 200 OK\r\nContent-Length: 5\r\nfoo: bar\r\n\r\nxyzzy");

    r.setStatus(400);
    QVERIFY(r.responseData() == "HTTP/1.1 400 Bad Request\r\nContent-Length: 5\r\nfoo: bar\r\n\r\nxyzzy");

    r.setStatus(404);
    QVERIFY(r.responseData() == "HTTP/1.1 404 Not Found\r\nContent-Length: 5\r\nfoo: bar\r\n\r\nxyzzy");

    r.setStatus(500);
    QVERIFY(r.responseData() == "HTTP/1.1 500 Internal Server Error\r\nContent-Length: 5\r\nfoo: bar\r\n\r\nxyzzy");
}
