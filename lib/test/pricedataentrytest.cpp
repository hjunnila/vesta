#include "pricedataentrytest.h"
#include "pricedataentry.h"

void PriceDataEntryTest::testNormalConstructor()
{
    PriceDataEntry e;
    QVERIFY(e.start().isValid() == false);
    QVERIFY(e.end().isValid() == false);
    QVERIFY(e.isValid() == false);
    QVERIFY(e.energy() == 0);
    QVERIFY(e.energyTax() == 0);
    QVERIFY(e.transfer() == 0);
    QVERIFY(e.transferTax() == 0);
    QVERIFY(e.premium() == 0);
    QVERIFY(e.premiumTax() == 0);
}

void PriceDataEntryTest::testNordPoolConstructor()
{
    QDateTime start = QDateTime::fromTime_t(100);
    QDateTime end = QDateTime::fromTime_t(200);

    PriceDataEntry e(start, end, 1.0);
    QVERIFY(e.start() == start);
    QVERIFY(e.end() == end);
    QVERIFY(e.isValid() == true);
    QVERIFY(e.energy() == 1.0);
    QVERIFY(e.energyTax() == 0);
    QVERIFY(e.transfer() == 0);
    QVERIFY(e.transferTax() == 0);
    QVERIFY(e.premium() == 0);
    QVERIFY(e.premiumTax() == 0);
}

void PriceDataEntryTest::testCopyConstructor()
{
    QDateTime start = QDateTime::fromTime_t(100);
    QDateTime end = QDateTime::fromTime_t(200);

    PriceDataEntry e(start, end, 1.0);
    e.setEnergyTax(2.0);
    e.setTransfer(3.0);
    e.setTransferTax(4.0);
    e.setPremium(5.0);
    e.setPremiumTax(6.0);

    PriceDataEntry e2(e);
    QVERIFY(e2.start() == start);
    QVERIFY(e2.end() == end);
    QVERIFY(e2.isValid() == true);
    QVERIFY(e2.energy() == 1.0);
    QVERIFY(e2.energyTax() == 2.0);
    QVERIFY(e2.transfer() == 3.0);
    QVERIFY(e2.transferTax() == 4.0);
    QVERIFY(e2.premium() == 5.0);
    QVERIFY(e2.premiumTax() == 6.0);
}

void PriceDataEntryTest::testAssignmentOperator()
{
    QDateTime start = QDateTime::fromTime_t(100);
    QDateTime end = QDateTime::fromTime_t(200);

    PriceDataEntry e(start, end, 1.0);
    e.setEnergyTax(2.0);
    e.setTransfer(3.0);
    e.setTransferTax(4.0);
    e.setPremium(5.0);
    e.setPremiumTax(6.0);

    PriceDataEntry e2;
    e2 = e;
    QVERIFY(e2.start() == start);
    QVERIFY(e2.end() == end);
    QVERIFY(e2.isValid() == true);
    QVERIFY(e2.energy() == 1.0);
    QVERIFY(e2.energyTax() == 2.0);
    QVERIFY(e2.transfer() == 3.0);
    QVERIFY(e2.transferTax() == 4.0);
    QVERIFY(e2.premium() == 5.0);
    QVERIFY(e2.premiumTax() == 6.0);
}

void PriceDataEntryTest::testCalculations()
{
    QDateTime start = QDateTime::fromTime_t(100);
    QDateTime end = QDateTime::fromTime_t(200);

    PriceDataEntry e;
    e.setEnergy(1.0);
    e.setEnergyTax(2.0);
    e.setTransfer(3.0);
    e.setTransferTax(4.0);
    e.setPremium(5.0);
    e.setPremiumTax(6.0);

    QVERIFY(e.energyWithTax() == 3.0);
    QVERIFY(e.transferWithTax() == 15.0);
    QVERIFY(e.premiumWithTax() == 35.0);
}
