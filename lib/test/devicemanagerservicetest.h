#pragma once

class DeviceManagerServiceTest : public QObject
{
    Q_OBJECT
private slots:
    void testConstruction();
    void testGetDevices();
    void testGetDevice1();
    void testGetDevice2();
    void testAddDevice();
};
