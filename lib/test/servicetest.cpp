#include "servicetest.h"
#include "service.h"
#include "service_p.h"
#include "request.h"

FooSvc::FooSvc(QObject* parent)
    : Service(parent)
{
    getCalled = 0;
    postCalled = 0;
}

FooSvc::~FooSvc()
{
}

Response FooSvc::get(const Request& request)
{
    Q_UNUSED(request);
    getCalled++;
    Response resp;
    resp.setStatus(200);
    return resp;
}

Response FooSvc::post(const Request& request)
{
    Q_UNUSED(request);
    postCalled++;
    Response resp;
    resp.setStatus(200);
    return resp;
}

void ServiceTest::testAddService()
{
    QRegExp foo("/foo");
    QRegExp bar("/bar");

    Service svc;
    QVERIFY(svc.d_ptr);
    QVERIFY(svc.d_ptr->services.count() == 0);

    svc.addService("GET", foo, "foo");
    QVERIFY(svc.d_ptr->services.count() == 1);
    QVERIFY(svc.d_ptr->services["GET"].count() == 1);

    svc.addService("GET", bar, "bar");
    QVERIFY(svc.d_ptr->services.count() == 1);
    QVERIFY(svc.d_ptr->services["GET"].count() == 2);

    svc.addService("POST", foo, "foo");
    QVERIFY(svc.d_ptr->services.count() == 2);
    QVERIFY(svc.d_ptr->services["POST"].count() == 1);
    QVERIFY(svc.d_ptr->services["GET"].count() == 2);
}

void ServiceTest::testHandleRequestNoHandler()
{
    QRegExp foo("/foo");

    Service svc;
    svc.addService("GET", foo, "foo");

    Request req(QByteArray("GET /foo HTTP1/1\r\n\r\n"));
    Response resp;
    QVERIFY(svc.handleRequest(req, resp) == false);
    QVERIFY(resp.status() == 404);
}

void ServiceTest::testHandleRequest()
{
    QRegExp foo("/foo");

    FooSvc svc;
    svc.addService("GET", foo, "get");
    svc.addService("POST", foo, "post");

    Request req(QByteArray("GET /foo HTTP1/1\r\n\r\n"));
    Response resp;
    QVERIFY(svc.handleRequest(req, resp) == true);
    QVERIFY(resp.status() == 200);
    QVERIFY(svc.getCalled == 1);
    QVERIFY(svc.postCalled == 0);

    req = Request(QByteArray("POST /foo HTTP1/1\r\nContent-Length: 0\r\n\r\n"));
    Response resp2;
    QVERIFY(svc.handleRequest(req, resp2) == true);
    QVERIFY(resp2.status() == 200);
    QVERIFY(svc.getCalled == 1);
    QVERIFY(svc.postCalled == 1);
}

void ServiceTest::testHandleRequestInvalidPathMethod()
{
    QRegExp foo("/foo");

    Service svc;
    svc.addService("GET", foo, "foo");

    Request req(QByteArray("GET /bar HTTP1/1\r\n\r\n"));
    Response resp;
    QVERIFY(svc.handleRequest(req, resp) == false);
    QVERIFY(resp.status() == 404);

    req = Request(QByteArray("POST /foo HTTP1/1\r\nContent-Length: 0\r\n\r\n"));
    Response resp2;
    QVERIFY(svc.handleRequest(req, resp2) == false);
    QVERIFY(resp2.status() == 404);
}
