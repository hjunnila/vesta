#pragma once
#include "server.h"

class ServerTest : public QObject
{
    Q_OBJECT
private slots:
    void testConstruction();
    void testRegister();
};
