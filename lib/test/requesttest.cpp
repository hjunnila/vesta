#include "requesttest.h"
#include "request.h"
#include "http.h"

void RequestTest::testNormalConstructor()
{
    Request r;
    QVERIFY(r.hasAllHeaders() == false);
    QVERIFY(r.method() == "");
    QVERIFY(r.path() == "");
    QVERIFY(r.protocol() == "");
    QVERIFY(r.formData().count() == 0);
    QVERIFY(r.headers().count() == 0);
    QVERIFY(r.body() == "");
}

void RequestTest::testNetworkConstructor()
{
    Request r("POST /foo/bar?xyzzy=baz HTTP/1.1\r\nContent-Length: 7\r\n\r\nYo dawg");
    QVERIFY(r.hasAllHeaders() == true);
    QVERIFY(r.method() == HTTP_POST);
    QVERIFY(r.path() == "/foo/bar");
    QVERIFY(r.query() == "xyzzy=baz");
    QVERIFY(r.formData().count() == 1);
    QVERIFY(r.formData()["xyzzy"] == "baz");
    QVERIFY(r.protocol() == "HTTP/1.1");
    QVERIFY(r.headers().count() == 1);
    QVERIFY(r.headers()[HTTP_HEADER_CONTENT_LENGTH] == "7");
    QVERIFY(r.body() == "Yo dawg");

    r = Request("POST /foo/bar?xyzzy HTTP/1.1\r\nContent-Length: 0\r\n\r\nYo dawg");
    QVERIFY(r.hasAllHeaders() == true);
    QVERIFY(r.method() == HTTP_POST);
    QVERIFY(r.path() == "/foo/bar");
    QVERIFY(r.query() == "xyzzy");
    QVERIFY(r.formData().count() == 1);
    QVERIFY(r.formData()["xyzzy"] == "");
    QVERIFY(r.protocol() == "HTTP/1.1");
    QVERIFY(r.headers().count() == 1);
    QVERIFY(r.headers()[HTTP_HEADER_CONTENT_LENGTH] == "0");
    QVERIFY(r.body() == "");

    r = Request("GET /foo HTTP/1.1\r\n\r\n");
    QVERIFY(r.hasAllHeaders() == true);
    QVERIFY(r.method() == "GET");
    QVERIFY(r.path() == "/foo");
    QVERIFY(r.protocol() == "HTTP/1.1");
    QVERIFY(r.formData().count() == 0);
    QVERIFY(r.headers().count() == 0);
    QVERIFY(r.body() == "");

    r = Request("GET /foo HTTP/1.1\r\n");
    QVERIFY(r.hasAllHeaders() == true);
    QVERIFY(r.method() == "GET");
    QVERIFY(r.path() == "/foo");
    QVERIFY(r.protocol() == "HTTP/1.1");
    QVERIFY(r.formData().count() == 0);
    QVERIFY(r.headers().count() == 0);
    QVERIFY(r.body() == "");

    r = Request("GET /foo");
    QVERIFY(r.hasAllHeaders() == false);
    QVERIFY(r.method() == "");
    QVERIFY(r.path() == "");
    QVERIFY(r.protocol() == "");
    QVERIFY(r.formData().count() == 0);
    QVERIFY(r.headers().count() == 0);
    QVERIFY(r.body() == "");
}

void RequestTest::testCopyConstructor()
{
    Request r("POST /foo/bar?xyzzy=baz HTTP/1.1\r\nContent-Length: 7\r\n\r\nYo dawg");
    Request r2(r);
    QVERIFY(r2.hasAllHeaders() == true);
    QVERIFY(r2.method() == HTTP_POST);
    QVERIFY(r2.path() == "/foo/bar");
    QVERIFY(r2.query() == "xyzzy=baz");
    QVERIFY(r2.formData().count() == 1);
    QVERIFY(r2.formData()["xyzzy"] == "baz");
    QVERIFY(r2.protocol() == "HTTP/1.1");
    QVERIFY(r2.headers().count() == 1);
    QVERIFY(r2.headers()[HTTP_HEADER_CONTENT_LENGTH] == "7");
    QVERIFY(r2.body() == "Yo dawg");
}

void RequestTest::testAssignmentOperator()
{
    Request r("POST /foo/bar?xyzzy=baz HTTP/1.1\r\nContent-Length: 7\r\n\r\nYo dawg");
    Request r2;
    r2 = r;
    QVERIFY(r2.hasAllHeaders() == true);
    QVERIFY(r2.method() == HTTP_POST);
    QVERIFY(r2.path() == "/foo/bar");
    QVERIFY(r2.query() == "xyzzy=baz");
    QVERIFY(r2.formData().count() == 1);
    QVERIFY(r2.formData()["xyzzy"] == "baz");
    QVERIFY(r2.protocol() == "HTTP/1.1");
    QVERIFY(r2.headers().count() == 1);
    QVERIFY(r2.headers()[HTTP_HEADER_CONTENT_LENGTH] == "7");
    QVERIFY(r2.body() == "Yo dawg");
}

