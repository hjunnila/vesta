#pragma once

#include "database.h"

class EnergyPlanDBTest : public FileSpawningTestCase
{
    Q_OBJECT

    QStringList filesToClean() const;
    DatabaseInfo dbinfo();

private slots:
    void testConstruction();
    void testEnergyPlanQueries();
    void testEnergyPlanEntryFromRecord();
};
