#include "staticoildata.h"
#include "oiltest.h"
#include "oil.h"
#include "oil_p.h"

#define DBFILE "oiltest.db"

DatabaseInfo OilTest::dbinfo()
{
    DatabaseInfo info;
    info.dbName = DBFILE;
    info.dbDriver = "QSQLITE";
    return info;
}

QStringList OilTest::filesToClean() const
{
    return QStringList() << DBFILE;
}

void OilTest::testConstruction()
{
    Oil oil(dbinfo());
    QVERIFY(oil.d_ptr != NULL);
    QVERIFY(oil.d_ptr->oilDataDB != NULL);
}

void OilTest::testStaticOil()
{
    Oil oil(dbinfo());

    StaticOilData d1("1.0", "0.5", "10.0", "device", "switch", "monitor", "power");
    oil.setStaticOilData(d1);

    StaticOilData d2 = oil.staticOilData();
    QVERIFY(d2.litrePrice() == "1.0");
    QVERIFY(d2.efficiency() == "0.5");
    QVERIFY(d2.kWhPerLitre() == "10.0");

    QVERIFY(oil.pricePerKWh() == 0.2);
    QVERIFY(oil.switchDevice() == "device");
    QVERIFY(oil.switchValue() == "switch");
    QVERIFY(oil.powerDevice() == "monitor");
    QVERIFY(oil.powerValue() == "power");
}
