#pragma once

class ElPriceDB;
class ElPriceDBP
{
public:
    // Properties
    ElPriceDB* parent;

public:
    // Methods
    PriceDataEntry priceDataEntryFromRecord(const QSqlRecord& record) const;
    bool isEntryAlreadyStored(const QString& area, const PriceDataEntry& entry) const;
    bool insertPriceDataEntry(const QString& area, const PriceDataEntry& entry);

};
