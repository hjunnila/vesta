#pragma once

class StaticOilData;
class StaticOilDataDB;
class StaticOilDataDBP
{
public:
    // Properties
    StaticOilDataDB* parent;

public:
    // Methods
    QSqlQuery staticOilDataQuery();
    StaticOilData staticOilDataFromRecord(const QSqlRecord& record) const;
    bool createStaticOilDataTable();
};
