#pragma once

class EnergyPlanDB;
class EnergyPlanDBP
{
public:
    // Properties
    EnergyPlanDB* parent;

public:
    // Methods
    bool storeEnergyPlanEntry(const EnergyPlanEntry& entry);
    QSqlQuery energyPlanQueryForDate(const QDate& date);
    QSqlRecord energyPlanRecordForDate(const QDateTime& date);
    static EnergyPlanEntry energyPlanEntryFromRecord(const QSqlRecord& record);
};
