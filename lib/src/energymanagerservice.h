#pragma once

#include "service.h"

class EnergyManagerServiceP;
class EnergyManager;
class Request;

class EnergyManagerService : public Service
{
    Q_OBJECT
    Q_DISABLE_COPY(EnergyManagerService)

public:
    EnergyManagerService(EnergyManager* energyManager);
    virtual ~EnergyManagerService();

public slots:
    Response getEnergyPlan(const Request& request);
    Response getStaticData(const Request& request);
    Response postStaticData(const Request& request);
    Response getManualElectricPlan(const Request& request);
    Response postManualElectricPlan(const Request& request);

private:
    EnergyManagerServiceP* d_ptr;
};
