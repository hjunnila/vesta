#include "zwevent_p.h"
#include "zwevent.h"

int ZWEventP::s_zwEventType = -1;

ZWEvent::ZWEvent(OpenZWave::Notification const* ntf)
    : QEvent((QEvent::Type)ZWEventP::s_zwEventType)
    , d_ptr(new ZWEventP)
{
    d_ptr->type = ntf->GetType();
    d_ptr->home = ntf->GetHomeId();
    d_ptr->node = ntf->GetNodeId();
    d_ptr->vid = ntf->GetValueID().GetId();
    d_ptr->string = QString::fromStdString(ntf->GetAsString());
    d_ptr->code = 0;
    d_ptr->event = 0;

    if (d_ptr->type == Notification::Type_Notification) {
        d_ptr->code = ntf->GetNotification();
    } else if (d_ptr->type == Notification::Type_ControllerCommand) {
        d_ptr->code = ntf->GetNotification();
        d_ptr->event = ntf->GetEvent();
    }
}

ZWEvent::~ZWEvent()
{
    delete d_ptr;
    d_ptr = NULL;
}

Notification::NotificationType ZWEvent::type() const
{
    return d_ptr->type;
}

uint32 ZWEvent::home() const
{
    return d_ptr->home;
}

uint8 ZWEvent::node() const
{
    return d_ptr->node;
}

uint64 ZWEvent::vid() const
{
    return d_ptr->vid;
}

QString ZWEvent::string() const
{
    return d_ptr->string;
}

uint8 ZWEvent::code() const
{
    return d_ptr->code;
}

uint8 ZWEvent::event() const
{
    return d_ptr->event;
}

int ZWEvent::ZWEventType()
{
    return ZWEventP::s_zwEventType;
}

void ZWEvent::registerZWEventType()
{
    if (ZWEventP::s_zwEventType == -1) {
        ZWEventP::s_zwEventType = QEvent::registerEventType();
    }
}
