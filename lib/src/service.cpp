#include "service_p.h"
#include "service.h"
#include "request.h"

Service::Service(QObject* parent)
    : QObject(parent)
    , d_ptr(new ServiceP)
{
}

Service::~Service()
{
    delete d_ptr;
    d_ptr = NULL;
}

void Service::addService(const QString& method, const QRegExp& path, const QString& member)
{
    d_ptr->services[method].append(QPair<QRegExp,QString>(path, member));
}

bool Service::handleRequest(const Request& request, Response& response)
{
    QList <QPair<QRegExp,QString> > list(d_ptr->services[request.method()]);

    for (QList <QPair<QRegExp,QString> >::const_iterator it = list.begin(); it != list.end(); ++it) {
        QPair <QRegExp,QString> pair = *it;
        QRegExp rx = pair.first;

        if (rx.indexIn(request.path()) >= 0) {
            if (rx.capturedTexts().count() == 1 && rx.capturedTexts()[0] == request.path()) {
                qDebug() << request.method() << rx.capturedTexts();
                return QMetaObject::invokeMethod(this,
                                                 pair.second.toUtf8().constData(),
                                                 Qt::DirectConnection,
                                                 Q_RETURN_ARG(Response, response),
                                                 Q_ARG(Request, request));
            }
        }
    }

    return false;
}
