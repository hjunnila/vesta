#pragma once

class ZWEventP
{
public:
    Notification::NotificationType type;
    uint32 home;
    uint8 node;
    uint64 vid;
    uint8 code;
    uint8 event;
    QString string;

    static int s_zwEventType;
};
