#include "staticoildata.h"
#include "staticoildatadb.h"
#include "macros.h"
#include "oil_p.h"
#include "oil.h"

Oil::Oil(const DatabaseInfo& info)
    : EnergySource()
    , d_ptr(new OilP)
{
    d_ptr->oilDataDB = new StaticOilDataDB(info);
}

Oil::~Oil()
{
    delete d_ptr->oilDataDB;
    d_ptr->oilDataDB = NULL;

    delete d_ptr;
    d_ptr = NULL;
}

QString Oil::switchDevice() const
{
    StaticOilData data = d_ptr->oilDataDB->staticOilData();
    return data.switchDevice();
}

QString Oil::switchValue() const
{
    StaticOilData data = d_ptr->oilDataDB->staticOilData();
    return data.switchValue();
}

QString Oil::powerDevice() const
{
    StaticOilData data = d_ptr->oilDataDB->staticOilData();
    return data.powerDevice();
}

QString Oil::powerValue() const
{
    StaticOilData data = d_ptr->oilDataDB->staticOilData();
    return data.powerValue();
}

bool Oil::setStaticOilData(const StaticOilData& data)
{
    return d_ptr->oilDataDB->setStaticOilData(data);
}

StaticOilData Oil::staticOilData()
{
    return d_ptr->oilDataDB->staticOilData();
}

double Oil::pricePerKWh() const
{
    StaticOilData data = d_ptr->oilDataDB->staticOilData();
    return (data.litrePriceDouble() / data.kWhPerLitreDouble()) / data.efficiencyDouble();
}

