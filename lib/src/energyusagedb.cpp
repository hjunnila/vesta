#include "energyusageentry.h"
#include "energyusagedb_p.h"
#include "energyusagedb.h"
#include "database.h"

EnergyUsageDB::EnergyUsageDB(const DatabaseInfo& info)
    : Database(info)
    , d_ptr(new EnergyUsageDBP)
{
    d_ptr->parent = this;
}

EnergyUsageDB::~EnergyUsageDB()
{
    d_ptr->parent = NULL;

    delete d_ptr;
    d_ptr = NULL;
}

QStringList EnergyUsageDB::essentialTables() const
{
    return QStringList() << kTableEnergyUsage;
}

bool EnergyUsageDB::createTable(const QString& tableName)
{
    if (tableName == kTableEnergyUsage) {
        return createEnergyUsageTable();
    } else {
        return false;
    }
}

bool EnergyUsageDB::createEnergyUsageTable()
{
    qDebug() << Q_FUNC_INFO;

    QSqlQuery query(database());
    if (!query.prepare("CREATE TABLE " kTableEnergyUsage "("
                       kFieldEnergyUsageStart " DATETIME NOT NULL,"
                       kFieldEnergyUsageEnd " DATETIME NOT NULL,"
                       kFieldEnergyUsageElSeconds " INTEGER,"
                       kFieldEnergyUsageElAveragePower " INTEGER,"
                       kFieldEnergyUsageOilSeconds " INTEGER,"
                       kFieldEnergyUsageOilAveragePower " INTEGER,"
                       kFieldEnergyUsageError " TEXT);")) {
        qWarning() << "Unable to prepare query for " kTableEnergyUsage " table creation:" << query.lastError();
        return false;
    }

    if (!query.exec()) {
        qWarning() << "Unable to create table:" << kTableEnergyUsage << query.lastError();
        return false;
    }

    return true;
}

EnergyUsageEntry EnergyUsageDB::energyUsageEntry(const QDateTime& dateTime)
{
    qDebug() << Q_FUNC_INFO;

    QString where = QString("WHERE (%1 <= '%2' AND %3 >= '%2')")
                            .arg(kFieldEnergyUsageStart)
                            .arg(dateTime.toString(Qt::ISODate))
                            .arg(kFieldEnergyUsageEnd);
    QString select = QString("SELECT * FROM %1 %2;").arg(kTableEnergyUsage).arg(where);

    if (!open()) {
        qWarning() << Q_FUNC_INFO << "Unable to open database." << database().lastError();
        close();
        return EnergyUsageEntry();
    }

    QSqlQuery query(database());
    if (!query.exec(select)) {
        qWarning() << Q_FUNC_INFO << "Unable to execute query:" << query.lastError();
    } else {
        QSqlRecord record;
        if (query.isValid() || query.next()) {
            record = query.record();
            close();
            return d_ptr->entryFromRecord(record);
        }
    }

    close();
    return EnergyUsageEntry();
}

bool EnergyUsageDB::storeEntry(const EnergyUsageEntry& entry)
{
    qDebug() << Q_FUNC_INFO << entry.start() << entry.end();

    if (!entry.isValid()) {
        qWarning() << "Unable to store an invalid energy usage entry.";
        return false;
    }

    bool result = false;

    if (!open()) {
        qWarning() << Q_FUNC_INFO << "Unable to open energy usage database connection:" << database().lastError();
        result = false;
    } else {
        result = d_ptr->insertEntry(entry);
    }

    close();
    return result;
}

//
// Private implementation
//

EnergyUsageEntry EnergyUsageDBP::entryFromRecord(const QSqlRecord& record) const
{
    return EnergyUsageEntry(record.value(kFieldEnergyUsageStart).toDateTime(),
                            record.value(kFieldEnergyUsageEnd).toDateTime(),
                            record.value(kFieldEnergyUsageElSeconds).toInt(),
                            record.value(kFieldEnergyUsageElAveragePower).toInt(),
                            record.value(kFieldEnergyUsageOilSeconds).toInt(),
                            record.value(kFieldEnergyUsageOilAveragePower).toInt(),
                            record.value(kFieldEnergyUsageError).toString());
}

bool EnergyUsageDBP::insertEntry(const EnergyUsageEntry& entry)
{
    QString values = QString("'%1', '%2', '%3', '%4', '%5', '%6', '%7'")
                            .arg(entry.start().toString(Qt::ISODate))
                            .arg(entry.end().toString(Qt::ISODate))
                            .arg(entry.elSeconds())
                            .arg(entry.elAveragePower())
                            .arg(entry.oilSeconds())
                            .arg(entry.oilAveragePower())
                            .arg(entry.error());
    QString insert = QString("INSERT INTO %1 VALUES (%2)").arg(kTableEnergyUsage).arg(values);

    QSqlQuery query(parent->database());
    if (!query.exec(insert)) {
        qWarning() << Q_FUNC_INFO << "Unable to insert energy usage entry:" << query.lastError();
        return false;
    }

    return true;
}
