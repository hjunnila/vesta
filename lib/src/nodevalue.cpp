#include "nodevalue_p.h"
#include "nodevalue.h"

NodeValue::NodeValue(uint64 vid, uint32 home)
    : QObject()
    , d_ptr(new NodeValueP)
{
    d_ptr->vid = vid;
    d_ptr->home = home;
}

NodeValue::~NodeValue()
{
    delete d_ptr;
    d_ptr = NULL;
}

bool NodeValue::operator==(const NodeValue& nv) const
{
    return (nv.d_ptr->vid == d_ptr->vid && nv.d_ptr->home == d_ptr->home);
}

QString NodeValue::label() const
{
    return QString::fromStdString(Manager::Get()->GetValueLabel(valueId()));
}

QString NodeValue::help() const
{
    return QString::fromStdString(Manager::Get()->GetValueHelp(valueId()));
}

QString NodeValue::units() const
{
    return QString::fromStdString(Manager::Get()->GetValueUnits(valueId()));
}

bool NodeValue::isReadOnly() const
{
    return Manager::Get()->IsValueReadOnly(valueId());
}

bool NodeValue::isWriteOnly() const
{
    return Manager::Get()->IsValueWriteOnly(valueId());
}

QString NodeValue::genre() const
{
    QString genre;
    switch (valueId().GetGenre()) {
        case ValueID::ValueGenre_Basic:
            genre = "Basic";
            break;
        case ValueID::ValueGenre_User:
            genre = "User";
            break;
        case ValueID::ValueGenre_Config:
            genre = "Config";
            break;
        case ValueID::ValueGenre_System:
            genre = "System";
            break;
        default:
            genre = "Unknown";
            break;
    }
    
    return genre;
}

ValueID NodeValue::valueId() const
{
    return d_ptr->valueId();
}

uint64 NodeValue::vid() const
{
    return d_ptr->vid;
}

QString NodeValue::type() const
{
    switch (valueId().GetType()) {
        case ValueID::ValueType_Bool:
            return QString("Bool");
        case ValueID::ValueType_Byte:
            return QString("Byte");
        case ValueID::ValueType_Decimal:
            return QString("Decimal");
        case ValueID::ValueType_Int:
            return QString("Int");
        case ValueID::ValueType_List:
            return QString("List");
        case ValueID::ValueType_Schedule:
            return QString("Schedule");
        case ValueID::ValueType_Short:
            return QString("Short");
        case ValueID::ValueType_String:
            return QString("String");
        case ValueID::ValueType_Button:
            return QString("Button");
        case ValueID::ValueType_Raw:
            return QString("Raw");
        default:
            return QString("Unknown");
    }
}

QVariant NodeValue::currentValue() const
{
    QVariant variant;
    ValueID valueid = valueId();

    switch (valueid.GetType()) {
        case ValueID::ValueType_Button:
        case ValueID::ValueType_Bool: {
            bool val = false;
            if (Manager::Get()->GetValueAsBool(valueid, &val)) {
                variant = val;
            } else {
                qWarning() << "Unable to get value as bool";
            }
        } break;

        case ValueID::ValueType_Byte: {
            uint8 val = 0;
            if (Manager::Get()->GetValueAsByte(valueid, &val)) {
                variant = val;
            } else {
                qWarning() << "Unable to get value as byte";
            }
        } break;

        case ValueID::ValueType_Decimal: {
            float val = 0;
            if (Manager::Get()->GetValueAsFloat(valueid, &val)) {
                variant = val;
            } else {
                qWarning() << "Unable to get value as decimal";
            }
        } break;

        case ValueID::ValueType_Int: {
            int32 val = 0;
            if (Manager::Get()->GetValueAsInt(valueid, &val)) {
                variant = val;
            } else {
                qWarning() << "Unable to get value as int";
            }
        } break;

        case ValueID::ValueType_List: {
            string str;
            if (Manager::Get()->GetValueListSelection(valueid, &str)) {
                variant = QString::fromStdString(str);
            } else {
                qWarning() << "Unable to get value as list";
            }
        } break;

        case ValueID::ValueType_Schedule: {
            variant = QVariant(QString("Schedule"));
        } break;
        
        case ValueID::ValueType_Short: {
            short val = 0;
            if (Manager::Get()->GetValueAsShort(valueid, &val)) {
                variant = val;
            } else {
                qWarning() << "Unable to get value as short";
            }
        } break;

        case ValueID::ValueType_String: {
            string val;
            if (Manager::Get()->GetValueAsString(valueid, &val)) {
                variant = QString::fromStdString(val);
            } else {
                qWarning() << "Unable to get value as string";
            }
        } break;

        case ValueID::ValueType_Raw:
            qWarning() << "Unable to get value as raw";
            break;

        default:
            qWarning() << "Unhandled value type:" << valueid.GetType();
            break;
    }

    return variant;
}

QStringList NodeValue::listItems() const
{
    QStringList list;

    vector <string> stdVector;
    if (Manager::Get()->GetValueListItems(valueId(), &stdVector)) {
        QVector <string> vector = QVector<string>::fromStdVector(stdVector);
        for (QVector <string>::const_iterator it = vector.begin(); it != vector.end(); ++it) {
            list << QString::fromStdString(*it);
        }
    }

    return list;
}

void NodeValue::turnOn()
{
    ValueID val = valueId();
    if (val.GetType() != ValueID::ValueType_Button &&
        val.GetType() != ValueID::ValueType_Bool) {
        qWarning() << "Unable to turn on a non-boolean value!";
    } else {
        Manager::Get()->SetValue(val, (bool) true);
    }
}

void NodeValue::turnOff()
{
    ValueID val = valueId();
    if (val.GetType() != ValueID::ValueType_Button &&
        val.GetType() != ValueID::ValueType_Bool) {
        qWarning() << "Unable to turn on a non-boolean value!";
    } else {
        Manager::Get()->SetValue(val, (bool) false);
    }
}

bool NodeValue::setValue(const QVariant& value)
{
    QMetaType::Type valueType = (QMetaType::Type) value.type();
    bool result = false;

    switch (valueId().GetType()) {
        case ValueID::ValueType_String: {
            if (valueType == QMetaType::QString || value.canConvert(QMetaType::QString)) {
                Manager::Get()->SetValue(valueId(), value.toString().toStdString());
                result = true;
            } else {
                qWarning() << "Cannot convert" << value << "to a string-type value";
            }
        } break;

        case ValueID::ValueType_List: {
            if (valueType == QMetaType::QString || value.canConvert(QMetaType::QString)) {
                Manager::Get()->SetValueListSelection(valueId(), value.toString().toStdString());
                result = true;
            } else {
                qWarning() << "Cannot convert" << value << "to a list-type value";
            }
        } break;

        case ValueID::ValueType_Byte: {
            if (valueType == QMetaType::UInt || value.canConvert(QMetaType::UInt)) {
                uint8 num = 0;
                bool ok = false;
                num = (uint8) value.toUInt(&ok);
                if (ok) {
                    qDebug() << "Setting byte:" << (uint) num;
                    Manager::Get()->SetValue(valueId(), num);
                    result = true;
                } else {
                    qWarning() << "Unable to set" << value << "as byte value!";
                }
            } else {
                qWarning() << "Cannot convert" << value << "to a byte value";
            }
        } break;

        case ValueID::ValueType_Short: {
            if (valueType == QMetaType::Short || value.canConvert(QMetaType::Short)) {
                short num = 0;
                bool ok = false;
                num = (short) value.toInt(&ok);
                if (ok) {
                    Manager::Get()->SetValue(valueId(), num);
                    result = true;
                } else {
                    qWarning() << "Unable to set" << value << "as short value!";
                }
            } else {
                qWarning() << "Cannot convert" << value << "to a short value";
            }
        } break;

        case ValueID::ValueType_Decimal: {
            if (valueType == QMetaType::Float || value.canConvert(QMetaType::Float)) {
                float num = 0;
                bool ok = false;
                num = value.toFloat(&ok);
                if (ok) {
                    Manager::Get()->SetValue(valueId(), num);
                    result = true;
                } else {
                    qWarning() << "Unable to set" << value << "as float value!";
                }
            } else {
                qWarning() << "Cannot convert" << value << "to a float value";
            }
        } break;

        case ValueID::ValueType_Bool: {
            if (valueType == QMetaType::Bool || value.canConvert(QMetaType::Bool)) {
                Manager::Get()->SetValue(valueId(), value.toBool());
                result = true;
            } else {
                qWarning() << "Cannot convert" << value << "to a boolean value";
            }
        } break;

        default: {
            qWarning() << "Unhandled value type" << type();
        } break;
    }

    if (result) {
        //Manager::Get()->EnablePoll(valueId());
        //d_ptr->startPendingTimer(5);
    }

    return result;
}

void NodeValue::handleValueChanged()
{
    if (d_ptr->pendingTimer) {
        //Manager::Get()->DisablePoll(valueId());
        //d_ptr->stopPendingTimer();
    }
}

//
// NodeValueP implementation
//

NodeValueP::NodeValueP(QObject* parent)
    : QObject(parent)
    , vid(0)
    , home(0)
    , pendingUpdate(false)
    , pendingTimer(NULL)
{
}

NodeValueP::~NodeValueP()
{
    stopPendingTimer();
}

ValueID NodeValueP::valueId() const
{
    return ValueID(home, vid);
}

void NodeValueP::startPendingTimer(int times)
{
    stopPendingTimer();

    pendingUpdate = times;
    pendingTimer = new QTimer(this);
    pendingTimer->setSingleShot(true);
    connect(pendingTimer, SIGNAL(timeout()), this, SLOT(onPendingTimer()));
    pendingTimer->start(1000);
}

void NodeValueP::stopPendingTimer()
{
    if (pendingTimer) {
        delete pendingTimer;
        pendingTimer = NULL;
    }

    pendingUpdate = 0;
}

void NodeValueP::onPendingTimer()
{
    qDebug() << Q_FUNC_INFO;

    if (!Manager::Get()->RefreshValue(valueId())) {
        qWarning() << "Unable to refresh value" << vid;
    }

    pendingUpdate--;
    if (pendingUpdate > 0 && pendingTimer != NULL) {
        pendingTimer->start(1000);
    } else {
        qDebug() << "Giving up on updates with value" << vid;
        pendingUpdate = 0;
        delete pendingTimer;
        pendingTimer = NULL;
    }
}


