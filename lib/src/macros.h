#pragma once

/** Stolen from GLib; Clamps x between low and high */
#define CLAMP(x, low, high)  (((x) > (high)) ? (high) : (((x) < (low)) ? (low) : (x)))
