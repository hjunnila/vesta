#pragma once

class ResponseP
{
public:
    // Properties
    int status;
    QString body;
    QMap<QString,QString> headers;

public:
    // Methods
    void updateContentLength();
    QString statusText() const;
    QString headerData() const;
};
