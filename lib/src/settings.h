#pragma once

class SettingsP;
class Settings
{
public:
    /**
     * Construct a new Settings object, using the given $fileName as a source file.
     *
     * @param fileName Name of the file to read settings from.
     */
    Settings(const QString& fileName);

    /** Copy constructor */
    Settings(const Settings& settings);

    /** Destructor */
    ~Settings();

    /** Assignment operator */
    Settings& operator=(const Settings& settings);

    /**
     * Load & parse the file given in constructor.
     *
     * @return true if successful, otherwise false
     */
    bool load();

    /**
     * Attempt to find a value from the file with the given key list. The key list
     * describes a direct object path to a value. For example, given a settings file
     * with the following layout:
     *  {
     *      "foo": {
     *          "bar":15,
     *          "xyzzy":"asd"
     *      }
     *  }
     *
     * Then, calling this method with (QStringList() << "foo" << "bar") would return
     * a QVariant with an integer value of 15.
     *
     * @param keyList Direct path to the value to get.
     * @return A QVariant with the value or an invalid QVariant if the key was not found.
     */
    QVariant valueForKeyList(const QStringList& keyList) const;

private:
    SettingsP* d_ptr;
};
