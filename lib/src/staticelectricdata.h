#pragma once

class StaticElectricDataP;
class StaticElectricData
{
public:
    StaticElectricData();
    StaticElectricData(const StaticElectricData& prices);
    StaticElectricData(const QString& energyTax, const QString& transfer, const QString& transferTax, const QString& premium, const QString& premiumTax, const QString& switchDevice, const QString& switchValue, const QString& powerDevice, const QString& powerValue, const QString& area);
    ~StaticElectricData();

    StaticElectricData& operator=(const StaticElectricData& prices);

    void setEnergyTax(const QString& energyTax);
    QString energyTax() const;
    double energyTaxDouble() const;

    void setTransfer(const QString& transfer);
    QString transfer() const;
    double transferDouble() const;

    void setTransferTax(const QString& transferTax);
    QString transferTax() const;
    double transferTaxDouble() const;

    void setPremium(const QString& premium);
    QString premium() const;
    double premiumDouble() const;

    void setPremiumTax(const QString& premiumTax);
    QString premiumTax() const;
    double premiumTaxDouble() const;

    void setSwitchDevice(const QString& switchDevice);
    QString switchDevice() const;

    void setSwitchValue(const QString& switchValue);
    QString switchValue() const;

    void setPowerDevice(const QString& powerDevice);
    QString powerDevice() const;

    void setPowerValue(const QString& powerValue);
    QString powerValue() const;

    void setArea(const QString& area);
    QString area() const;

private:
    StaticElectricDataP* d_ptr;
};
