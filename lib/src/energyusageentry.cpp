#include "energyusageentry_p.h"
#include "energyusageentry.h"

EnergyUsageEntry::EnergyUsageEntry()
    : d_ptr(new EnergyUsageEntryP)
{
}

EnergyUsageEntry::EnergyUsageEntry(const QDateTime& start, const QDateTime& end, int elSeconds, int elAveragePower, int oilSeconds, int oilAveragePower, const QString& error)
    : d_ptr(new EnergyUsageEntryP)
{
    d_ptr->start = start;
    d_ptr->end = end;
    d_ptr->elSeconds = elSeconds;
    d_ptr->elAveragePower = elAveragePower;
    d_ptr->oilSeconds = oilSeconds;
    d_ptr->oilAveragePower = oilAveragePower;
    d_ptr->error = error;
}

EnergyUsageEntry::EnergyUsageEntry(const EnergyUsageEntry& entry)
    : d_ptr(new EnergyUsageEntryP)
{
    d_ptr->start = entry.d_ptr->start;
    d_ptr->end = entry.d_ptr->end;
    d_ptr->elSeconds = entry.d_ptr->elSeconds;
    d_ptr->elAveragePower = entry.d_ptr->elAveragePower;
    d_ptr->oilSeconds = entry.d_ptr->oilSeconds;
    d_ptr->oilAveragePower = entry.d_ptr->oilAveragePower;
    d_ptr->error = entry.d_ptr->error;
}

EnergyUsageEntry& EnergyUsageEntry::operator=(const EnergyUsageEntry& entry)
{
    if (this != &entry) {
        d_ptr->start = entry.d_ptr->start;
        d_ptr->end = entry.d_ptr->end;
        d_ptr->elSeconds = entry.d_ptr->elSeconds;
        d_ptr->elAveragePower = entry.d_ptr->elAveragePower;
        d_ptr->oilSeconds = entry.d_ptr->oilSeconds;
        d_ptr->oilAveragePower = entry.d_ptr->oilAveragePower;
        d_ptr->error = entry.d_ptr->error;
    }

    return *this;
}

EnergyUsageEntry::~EnergyUsageEntry()
{
    delete d_ptr;
    d_ptr = NULL;
}

bool EnergyUsageEntry::isValid() const
{
    if (!d_ptr->start.isValid()) return false;
    if (!d_ptr->end.isValid()) return false;
    return true;
}

QDateTime EnergyUsageEntry::start() const
{
    return d_ptr->start;
}

QDateTime EnergyUsageEntry::end() const
{
    return d_ptr->end;
}

int EnergyUsageEntry::elSeconds() const
{
    return d_ptr->elSeconds;
}

int EnergyUsageEntry::elAveragePower() const
{
    return d_ptr->elAveragePower;
}

int EnergyUsageEntry::oilSeconds() const
{
    return d_ptr->oilSeconds;
}

int EnergyUsageEntry::oilAveragePower() const
{
    return d_ptr->oilAveragePower;
}

QString EnergyUsageEntry::error() const
{
    return d_ptr->error;
}
