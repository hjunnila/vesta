#pragma once

class PriceDataEntryP;
class PriceDataEntry
{
public:
    /** Create an emptry price data entry */
    PriceDataEntry();
    PriceDataEntry(const QDateTime& start, const QDateTime& end, double energy);
    PriceDataEntry(const PriceDataEntry& entry);

    /** Assignment operator */
    PriceDataEntry& operator=(const PriceDataEntry& entry);

    /** Destructor */
    ~PriceDataEntry();

    bool isValid() const;

    QDateTime start() const;
    QDateTime end() const;

    double energyWithTax() const;
    double transferWithTax() const;
    double premiumWithTax() const;

    double energy() const;
    void setEnergy(double energy);

    double energyTax() const;
    void setEnergyTax(double percent);

    double transfer() const;
    void setTransfer(double transfer);

    double transferTax() const;
    void setTransferTax(double percent);

    double premium() const;
    void setPremium(double premium);

    double premiumTax() const;
    void setPremiumTax(double premiumTax);

private:
    PriceDataEntryP* d_ptr;
};
