#pragma once

class RequestP
{
public:
    // Properties
    QString method;
    QString path;
    QString query;
    QString protocol;
    QMap <QString,QString> formData;
    QMap <QString,QString> headers;
    bool hasAllHeaders;
    QString body;

public:
    // Methods
    void parseFromData(const QByteArray& byteArray);
};
