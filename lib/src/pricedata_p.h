#pragma once

class PriceDataP
{
public:
    QString area;
    QDateTime start;
    QDateTime end;
    QDateTime updated;
    QList <PriceDataEntry> prices;
    QSqlError::ErrorType error;
};
