#pragma once

#include "service.h"

class ServerP : public QObject
{
    Q_OBJECT
    Q_DISABLE_COPY(ServerP)

public:
    ServerP(QObject* parent);
    ~ServerP();

public:
    // Properties
    quint16 port;
    QString certFile;
    QString pKeyFile;

    QList <Service*> services;
    QMap <QSslSocket*,QByteArray> connections;

public:
    // Methods
    void serveRequest(const Request& request, QSslSocket* socket);
    void sendResponse(const Response& response, QSslSocket* socket);

public slots:
    // Slots
    void onEncrypted();
    void onReadyRead();
    void onSslErrors(const QList <QSslError>& errors);
};
