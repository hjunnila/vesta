#pragma once
#include "energymanager.h"

class DeviceNode;
class QObject;
class ZWEvent;
class Shell;

class DeviceManagerP;
class DeviceManager : public QObject
{
    Q_OBJECT

    friend class Shell;

public:
    DeviceManager(QObject* parent = 0);
    virtual ~DeviceManager();

    /** Start the Z-Wave watcher */
    bool start();

    /** Stop the Z-Wave watcher */
    void stop();

    /** Check if the watcher has been started */
    bool running() const;

    /** Reset the controller. If hard == true, removes all known devices. */
    void reset(bool hard = false);

    /** Start the device add procedure (i.e. the next new device is added) */
    virtual void addDevice();

    /** Start the device remove procedure (WTF?) */
    void removeDevice();

    /** Heal the network */
    void healNetwork();

    /** Get a Z-Wave device node by its node id */
    DeviceNode* deviceNode(quint8 node) const;

    /** Find device with the a user-assigned name */
    DeviceNode* deviceNode(const QString& deviceName) const;

    /** Get all known devices. Key is the device's id.  */
    QMap <quint8, DeviceNode*> devices() const;

    /** Get the manager's home id */
    quint32 home() const;

    /** Get the manager's node id */
    quint8 node() const;

public slots:
    void onEnergySourcePriorityChanged(EnergySource* energySource, EnergySourcePriority priority);

signals:
    void stateChanged(bool running);
    void ready();
    void deviceNodeValueChanged(const QString& deviceName, const QString& valueName, const QVariant& newValue);

private:
    DeviceManagerP* d_ptr;
};
