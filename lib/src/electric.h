#pragma once
#include "energysource.h"
#include "database.h"

class StaticElectricData;
class PriceData;
class ElectricP;

class Electric : public EnergySource
{
    Q_OBJECT
    Q_DISABLE_COPY(Electric)

public:
    Electric(const DatabaseInfo& info = Database::defaultDatabaseInfo());
    ~Electric();

    /** See EnergySource */
    virtual QString switchDevice() const;

    /** See EnergySource */
    virtual QString switchValue() const;

    /** See EnergySource */
    virtual QString powerDevice() const;

    /** See EnergySource */
    virtual QString powerValue() const;

    /** Get price data entries after (and including) the given date & time. */
    PriceData priceDataAfterIncluding(const QDateTime& dateTime) const;

    /** Set static electricity data */
    void setStaticElectricData(const StaticElectricData& data);

    /** Get static electricity data */
    StaticElectricData staticElectricData();

private:
    ElectricP* d_ptr;
};
