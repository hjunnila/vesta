#pragma once

class NodeValue;
class DeviceNodeP;
class DeviceNode : public QObject
{
    Q_OBJECT
    Q_DISABLE_COPY(DeviceNode)

    friend class DeviceManagerP;

public:
    DeviceNode(quint32 home, quint8 node, QObject* parent = 0);
    virtual ~DeviceNode();

    /** Comparison operator to see, if two instances represent the same node */
    bool operator==(const DeviceNode& node) const;

    /** Get this device's home id */
    quint32 home() const;

    /** Get this device's node id */
    quint8 node() const;

    /** Get the manufacturer name */
    virtual QString manufacturer() const;

    /** Get the product name */
    virtual QString productName() const;

    /** Get the user-assigned name */
    virtual QString name() const;

    /** Set the user-assigned name */
    virtual void setName(const QString& name);

    /** Check if this device node is awake */
    virtual bool isAwake() const;

    /** Ask the node to refresh its static information */
    virtual bool refreshInfo() const;

    /** Ask the node to refresh its dynamic state */
    virtual bool requestState() const;

    /** Ask only the dynamics */
    virtual bool requestDynamicState() const;

    /** Attempt to heal this node */
    virtual void heal() const;

    /** Get a list of known values in this node */
    virtual QList <NodeValue*> values() const;

    /** Get a value with the given label */
    virtual NodeValue* value(const QString& label) const;

    /** Get a value with the given vid */
    virtual NodeValue* value(quint64 vid) const;

signals:
    /** Emitted when one of the node's values has changed */
    void valueChanged(DeviceNode* node, NodeValue* nv);

private:
    DeviceNodeP* d_ptr;
};
