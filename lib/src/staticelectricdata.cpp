#include "staticelectricdata.h"
#include "staticelectricdata_p.h"

StaticElectricData::StaticElectricData()
    : d_ptr(new StaticElectricDataP)
{
    d_ptr->energyTax = "0";
    d_ptr->transfer = "0";
    d_ptr->transferTax = "0";
    d_ptr->premium = "0";
    d_ptr->premiumTax = "0";
}

StaticElectricData::StaticElectricData(const StaticElectricData& data)
    : d_ptr(new StaticElectricDataP)
{
    d_ptr->energyTax = data.d_ptr->energyTax;
    d_ptr->transfer = data.d_ptr->transfer;
    d_ptr->transferTax = data.d_ptr->transferTax;
    d_ptr->premium = data.d_ptr->premium;
    d_ptr->premiumTax = data.d_ptr->premiumTax;

    d_ptr->switchDevice = data.d_ptr->switchDevice;
    d_ptr->switchValue = data.d_ptr->switchValue;

    d_ptr->powerDevice = data.d_ptr->powerDevice;
    d_ptr->powerValue = data.d_ptr->powerValue;

    d_ptr->area = data.d_ptr->area;
}

StaticElectricData::StaticElectricData(const QString& energyTax, const QString& transfer, const QString& transferTax, const QString& premium, const QString& premiumTax, const QString& switchDevice, const QString& switchValue, const QString& powerDevice, const QString& powerValue, const QString& area)
    : d_ptr(new StaticElectricDataP)
{
    d_ptr->energyTax = energyTax;
    d_ptr->transfer = transfer;
    d_ptr->transferTax = transferTax;
    d_ptr->premium = premium;
    d_ptr->premiumTax = premiumTax;

    d_ptr->switchDevice = switchDevice;
    d_ptr->switchValue = switchValue;

    d_ptr->powerDevice = powerDevice;
    d_ptr->powerValue = powerValue;

    d_ptr->area = area;
}

StaticElectricData::~StaticElectricData()
{
    delete d_ptr;
    d_ptr = NULL;
}

StaticElectricData& StaticElectricData::operator=(const StaticElectricData& data)
{
    if (this != &data) {
        d_ptr->energyTax = data.d_ptr->energyTax;
        d_ptr->transfer = data.d_ptr->transfer;
        d_ptr->transferTax = data.d_ptr->transferTax;
        d_ptr->premium = data.d_ptr->premium;
        d_ptr->premiumTax = data.d_ptr->premiumTax;

        d_ptr->switchDevice = data.d_ptr->switchDevice;
        d_ptr->switchValue = data.d_ptr->switchValue;

        d_ptr->powerDevice = data.d_ptr->powerDevice;
        d_ptr->powerValue = data.d_ptr->powerValue;

        d_ptr->area = data.d_ptr->area;
    }

    return *this;
}

void StaticElectricData::setEnergyTax(const QString& energyTax)
{
    d_ptr->energyTax = energyTax;
}

QString StaticElectricData::energyTax() const
{
    return d_ptr->energyTax;
}

double StaticElectricData::energyTaxDouble() const
{
    return d_ptr->energyTax.toDouble();
}

void StaticElectricData::setTransfer(const QString& transfer)
{
    d_ptr->transfer = transfer;
}

QString StaticElectricData::transfer() const
{
    return d_ptr->transfer;
}

double StaticElectricData::transferDouble() const
{
    return d_ptr->transfer.toDouble();
}

void StaticElectricData::setTransferTax(const QString& transferTax)
{
    d_ptr->transferTax = transferTax;
}

QString StaticElectricData::transferTax() const
{
    return d_ptr->transferTax;
}

double StaticElectricData::transferTaxDouble() const
{
    return d_ptr->transferTax.toDouble();
}

void StaticElectricData::setPremium(const QString& premium)
{
    d_ptr->premium = premium;
}

QString StaticElectricData::premium() const
{
    return d_ptr->premium;
}

double StaticElectricData::premiumDouble() const
{
    return d_ptr->premium.toDouble();
}

void StaticElectricData::setPremiumTax(const QString& premiumTax)
{
    d_ptr->premiumTax = premiumTax;
}

QString StaticElectricData::premiumTax() const
{
    return d_ptr->premiumTax;
}

double StaticElectricData::premiumTaxDouble() const
{
    return d_ptr->premiumTax.toDouble();
}

void StaticElectricData::setSwitchDevice(const QString& switchDevice)
{
    d_ptr->switchDevice = switchDevice;
}

QString StaticElectricData::switchDevice() const
{
    return d_ptr->switchDevice;
}

void StaticElectricData::setSwitchValue(const QString& switchValue)
{
    d_ptr->switchValue = switchValue;
}

QString StaticElectricData::switchValue() const
{
    return d_ptr->switchValue;
}

void StaticElectricData::setPowerDevice(const QString& powerDevice)
{
    d_ptr->powerDevice = powerDevice;
}

QString StaticElectricData::powerDevice() const
{
    return d_ptr->powerDevice;
}

void StaticElectricData::setPowerValue(const QString& powerValue)
{
    d_ptr->powerValue = powerValue;
}

QString StaticElectricData::powerValue() const
{
    return d_ptr->powerValue;
}

void StaticElectricData::setArea(const QString& area)
{
    d_ptr->area = area;
}

QString StaticElectricData::area() const
{
    return d_ptr->area;
}
