#pragma once
#include "energysource.h"
#include "database.h"

class StaticOilData;
class DatabaseInfo;
class Settings;
class OilP;
class Oil : public EnergySource
{
    Q_OBJECT
    Q_DISABLE_COPY(Oil)

public:
    Oil(const DatabaseInfo& info = Database::defaultDatabaseInfo());
    virtual ~Oil();

    /** See EnergySource */
    virtual QString switchDevice() const;

    /** See EnergySource */
    virtual QString switchValue() const;

    /** See EnergySource */
    virtual QString powerDevice() const;

    /** See EnergySource */
    virtual QString powerValue() const;

    /** Set static oil data */
    virtual bool setStaticOilData(const StaticOilData& entry);

    /** Get static oil price */
    virtual StaticOilData staticOilData();

    /** Get oil price (in cents) per kWh */
    virtual double pricePerKWh() const;

private:
    OilP* d_ptr;
};
