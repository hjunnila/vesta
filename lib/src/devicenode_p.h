#pragma once

class DeviceNode;
class NodeValue;
class DeviceNodeP : public QObject
{
    Q_OBJECT
    Q_DISABLE_COPY(DeviceNodeP)

public:
    DeviceNodeP(QObject* parent);
    ~DeviceNodeP();

public:
    // Properties
    OpenZWave::Manager* manager;
    uint32 home;
    uint8 node;

    QList <NodeValue*> values;

public:
    // Methods
    NodeValue* value(uint64 vid) const;
    void addValue(quint64 vid);
    void removeValue(quint64 vid);
    void handleValueChanged(quint64 vid);

signals:
    void valueChanged(DeviceNode* deviceNode, NodeValue* nv);
};
