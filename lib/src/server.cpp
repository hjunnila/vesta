#include "server_p.h"
#include "server.h"
#include "settings.h"
#include "response.h"
#include "request.h"
#include "http.h"

Server::Server(Settings settings, QObject* parent)
    : QTcpServer(parent)
    , d_ptr(new ServerP(this))
{
    if (!settings.load()) {
        return;
    }

    qRegisterMetaType<Request>("Request");
    qRegisterMetaType<Response>("Response");

    d_ptr->port = settings.valueForKeyList(SETTINGS_KEY_SERVER_PORT).toUInt();
    d_ptr->certFile = settings.valueForKeyList(SETTINGS_KEY_SERVER_CERT).toString();
    d_ptr->pKeyFile = settings.valueForKeyList(SETTINGS_KEY_SERVER_PKEY).toString();

    // Just make sure we can open the certificates and warn if not.
    QFile cert(d_ptr->certFile);
    if (!cert.open(QIODevice::ReadOnly)) {
        qWarning() << "Unable to open" << d_ptr->certFile << cert.errorString();
    }
    cert.close();

    QFile pkey(d_ptr->pKeyFile);
    if (!pkey.open(QIODevice::ReadOnly)) {
        qWarning() << "Unable to open" << d_ptr->pKeyFile << pkey.errorString();
    }
    pkey.close();
}

Server::~Server()
{
    delete d_ptr;
    d_ptr = NULL;
}

bool Server::start()
{
    qDebug() << "Starting server.";
    return listen(QHostAddress::Any, d_ptr->port);
}

void Server::registerService(Service* service)
{
    if (d_ptr->services.contains(service) == false) {
        d_ptr->services.append(service);
    }
}

void Server::unregisterService(Service* service)
{
    d_ptr->services.removeAll(service);
}

void Server::incomingConnection(qintptr socketDescriptor)
{
    QSslSocket *socket = new QSslSocket(this);
    if (socket->setSocketDescriptor(socketDescriptor)) {
        socket->setProtocol(QSsl::AnyProtocol);
        socket->setLocalCertificate(d_ptr->certFile);
        socket->setPrivateKey(d_ptr->pKeyFile);

        connect(socket, SIGNAL(encrypted()), d_ptr, SLOT(onEncrypted()));
        connect(socket, SIGNAL(sslErrors(const QList<QSslError>&)), d_ptr, SLOT(onSslErrors(const QList<QSslError>&)));

        socket->startServerEncryption();
        d_ptr->connections[socket] = QByteArray();
    } else {
        qDebug() << "Unable to set socket descriptor";
        delete socket;
    }
}

//
// Private implementation
//

ServerP::ServerP(QObject* parent)
    : QObject(parent)
    , port(0)
{
}

ServerP::~ServerP()
{
}

void ServerP::onEncrypted()
{
    connect(sender(), SIGNAL(readyRead()), this, SLOT(onReadyRead()));
}

void ServerP::onReadyRead()
{
    QSslSocket* socket = static_cast<QSslSocket*> (sender());

    // Read available data to a byte array associated with the socket we're reading from.
    QByteArray& socketData = connections[socket];
    socketData += socket->readAll();
    Request request(socketData);

    if (request.hasAllHeaders()) {
        // All headers that we can expect have arrived
        if (request.method() == HTTP_GET || request.method() == HTTP_HEAD) {
            // No need to expect anything in the request body
            serveRequest(request, socket);
        } else {
            // Check if we have all the data we're expecting
            if (request.headers().contains(HTTP_HEADER_CONTENT_LENGTH)) {
                int cl = request.headers()[HTTP_HEADER_CONTENT_LENGTH].toInt();
                if (request.body().length() >= cl) {
                    // Looks like we have everything that's coming from the socket. It's time to
                    // serve the request.
                    serveRequest(request, socket);
                }
            } else {
                Response response(Response::badRequest());
                sendResponse(response, socket);
            }
        }
    }
}

void ServerP::onSslErrors(const QList <QSslError>& errors)
{
    for (QList <QSslError>::const_iterator it = errors.begin(); it != errors.end(); ++it) {
        qWarning() << Q_FUNC_INFO << (*it);
    }
}

void ServerP::serveRequest(const Request& request, QSslSocket* socket)
{
    Response response(Response::notFound());

    for (QList <Service*>::const_iterator it = services.begin(); it != services.end(); ++it) {
        Service* svc = *it;
        if (svc->handleRequest(request, response)) {
            break;
        }
    }

    sendResponse(response, socket);
}

void ServerP::sendResponse(const Response& response, QSslSocket* socket)
{
    Response copy(response);
    copy.setHeader(HTTP_HEADER_DATE, QDateTime::currentDateTime().toString(Qt::RFC2822Date));
    copy.setHeader(HTTP_HEADER_SERVER, QString("%1 %2").arg(QCoreApplication::applicationName())
                                                           .arg(QCoreApplication::applicationVersion()));
    copy.setHeader(HTTP_HEADER_ACCESS_CONTROL_ALLOW_ORIGIN, "*");
    QStringList exposeHeaders = QStringList() << HTTP_HEADER_DATE << HTTP_HEADER_SERVER;
    copy.setHeader(HTTP_HEADER_ACCESS_CONTROL_EXPOSE_HEADERS, exposeHeaders.join(", "));

    QByteArray responseData(copy.responseData());
    socket->write(responseData);
    connections.remove(socket);
}
