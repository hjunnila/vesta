#pragma once
#include "database.h"

#define kTableStaticElectricData            "static_electric_data"
#define kFieldStaticElectricDataEnergyTax   "energyTax"
#define kFieldStaticElectricDataTransfer    "transfer"
#define kFieldStaticElectricDataTransferTax "transferTax"
#define kFieldStaticElectricDataPremium     "premium"
#define kFieldStaticElectricDataPremiumTax  "premiumTax"

#define kFieldStaticElectricDataSwitchDevice "switchDevice"
#define kFieldStaticElectricDataSwitchValue  "switchValue"
#define kFieldStaticElectricDataPowerDevice  "powerDevice"
#define kFieldStaticElectricDataPowerValue   "powerValue"

#define kFieldStaticElectricDataArea        "area"

class StaticElectricData;
class QSqlRecord;

class StaticElectricDBP;
class StaticElectricDB : public Database
{
    Q_OBJECT

    friend class StaticElectricDBP;

public:
    StaticElectricDB(const DatabaseInfo& info = Database::defaultDatabaseInfo());
    ~StaticElectricDB();

    /** Set static electric data */
    bool setStaticElectricData(const StaticElectricData& data);

    /** Get static electric data */
    StaticElectricData staticElectricData();

protected:
    QStringList essentialTables() const;
    bool createTable(const QString& tableName);

private:
    StaticElectricDBP* d_ptr;
};
