#pragma once
#include "database.h"

#define kTableStaticOilData            "static_oil_data"
#define kFieldStaticOilDataLitrePrice  "litrePrice"
#define kFieldStaticOilDataEfficiency  "efficiency"
#define kFieldStaticOilDataKWhPerLitre "kWhPerLitre"

#define kFieldStaticOilDataSwitchDevice "switchDevice"
#define kFieldStaticOilDataSwitchValue  "switchValue"
#define kFieldStaticOilDataPowerDevice  "powerDevice"
#define kFieldStaticOilDataPowerValue   "powerValue"

class StaticOilData;
class StaticOilDataDBP;
class StaticOilDataDB : public Database
{
    Q_OBJECT
    Q_DISABLE_COPY(StaticOilDataDB)

    friend class StaticOilDataDBP;

public:
    /** Create a new StaticOilDataDB instance with the given database info */
    StaticOilDataDB(const DatabaseInfo& info);
    ~StaticOilDataDB();

    /** Set static oil data */
    bool setStaticOilData(const StaticOilData& entry);

    /** Get static oil data */
    StaticOilData staticOilData();

protected:
    QStringList essentialTables() const;
    bool createTable(const QString& tableName);

private:
    StaticOilDataDBP* d_ptr;
};
