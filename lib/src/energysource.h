#pragma once

typedef enum EnergySourcePriority {
    EnergySourcePriorityHigh,
    EnergySourcePriorityLow
} EnergySourcePriority;

class EnergySourceP;
class EnergySource : public QObject
{
    Q_OBJECT

public:
    EnergySource();
    ~EnergySource();

    /** Returns the name of the device that controls/monitors whether the energy source is on/off */
    virtual QString switchDevice() const = 0;

    /** Returns the name of the value in the device that controls whether the energy source is on/off */
    virtual QString switchValue() const = 0;

    /** Returns the name of the device that monitors the energy source's power consumption */
    virtual QString powerDevice() const = 0;

    /** Returns the name of the value in the device that monitors the energy source's power consumption */
    virtual QString powerValue() const = 0;

    /** Return the datetime when the log was last reset. */
    virtual QDateTime logStartDate() const;

    /**
     * Log a report from the network that the node has been switched on/off
     * (i.e. its usage might be permitted but not necessarily used).
     */
    virtual void logUsageChange(bool on);

    /**
     * Log a report from the network that the node's power consumption has changed
     * (i.e. it is actually producing energy).
     */
    virtual void logPowerUsage(int watts);

    /**
     * Get the number of seconds this energy source has been ON since last call to this
     * method and reset the counter.
     */
    virtual int cumulativeOnTimeAndReset();

    /**
     * Get the average power consumption (in watts) that has been consumed while the
     * the device has been switched ON.
     */
    virtual int averagePowerConsumptionWhileOnAndReset();

private:
    EnergySourceP* d_ptr;
};
