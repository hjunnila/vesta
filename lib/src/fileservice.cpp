#include "fileservice_p.h"
#include "fileservice.h"
#include "response.h"
#include "request.h"
#include "service.h"
#include "http.h"

FileService::FileService(QObject* parent, const QString& serverRoot)
    : Service(parent)
    , d_ptr(new FileServiceP)
{
    d_ptr->serverRoot = serverRoot;

    // Only allow these files to be served. A simpler any file pattern would get easily compromised.
    d_ptr->servedFiles = QStringList() << "index.html" << "devicemanagement.js" << "energyplan.js" << "jquery.canvasjs.min.js" << "purl.js" << "serverconfig.js" << "staticdata.js";

    const QRegExp rxAnyFile("/vesta/.*");
    addService(HTTP_GET, rxAnyFile, "getFile");
}

FileService::~FileService()
{
    delete d_ptr;
    d_ptr = NULL;
}

Response FileService::getFile(const Request& request)
{
    qDebug() << Q_FUNC_INFO << request.path();

    Response response;

    if (request.path() == "/vesta" || request.path() == "/vesta/") {
        // Serve index.html when a file is not given
        QString fileName = d_ptr->serverRoot + QString("/index.html");
        QFile file(fileName);
        if (file.open(QIODevice::ReadOnly)) {
            response.setBody(file.readAll());
            response.setStatus(200);
            file.close();
        } else {
            qWarning() << file.errorString() << file.fileName();
        }
    } else {
        // Serve one of the predefined files
        QString requestedFile = request.path().remove("/vesta/");
        QString servedFile;
        for (QStringList::const_iterator it = d_ptr->servedFiles.begin(); it != d_ptr->servedFiles.end(); ++it) {
            if (requestedFile == *it) {
                // Make damn sure that a user-agent-originated string does not get misused as part of the path, so
                // put the string that was found from the predefined list to the path.
                servedFile = d_ptr->serverRoot + ("/") + *it;
                break;
            }
        }

        if (servedFile.length()) {
            QFile file(servedFile);
            if (file.open(QIODevice::ReadOnly)) {
                response.setBody(file.readAll());
                response.setStatus(200);
                file.close();
            } else {
                qWarning() << file.errorString() << file.fileName();
            }
        }
    }

    return response;
}

