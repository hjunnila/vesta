#pragma once

#include "energyplanentry.h"
#include "energysource.h"

class EnergyUsageDB;
class EnergyPlanDB;
class Oil;
class Electric;
class PriceData;

class EnergyManagerP : public QObject
{
    Q_OBJECT
    Q_DISABLE_COPY(EnergyManagerP)

public:
    EnergyManagerP(QObject* parent);
    ~EnergyManagerP();

public:
    // Properties
    EnergyPlanDB* plandb;
    EnergyUsageDB* usagedb;

    Oil* oil;
    Electric* electric;

    QTimer* planTimer;
    EnergyPlanEntry currentHourPlan;

    QDateTime retryElectricPriceStartDate;

    bool manualElectricOverride;

public slots:
    // Methods
    void onRetryElectricPriceFetch();
    void onPlanTimer();
    void onHourChange();

public:
    void createNewEnergyPlans(const QDateTime& startDateTime);
    void updateEnergyPlan(const PriceData& electricPrices);
    void updateCurrentHourEnergyPlan();
    void setCurrentEnergyPlan(const EnergyPlanEntry& plan);
    static EnergySourcePriority electricPriorityForPlan(const EnergyPlanEntry& plan);
    static EnergyPlanEntry failSafePlan(const EnergyPlanEntry& basedOnPlan);
    static EnergyPlanEntry manualElectricPlan(const EnergyPlanEntry& basedOnPlan);

signals:
    void energySourcePriorityChanged(EnergySource* energySource, EnergySourcePriority priority);
};
