#include "devicemanager.h"
#include "energysource.h"
#include "devicenode_p.h"
#include "devicenode.h"
#include "nodevalue.h"
#include "settings.h"
#include "zwevent.h"

#include "devicemanager_p.h"

#define SETTINGS_FILE                               "devicemanager.json"
#define SETTINGS_KEY_DEVICEMANAGER                  "deviceManager"
#define SETTINGS_KEYLIST_DEVICEMANAGER_DEVICE       (QStringList() << SETTINGS_KEY_DEVICEMANAGER << "device")
#define SETTINGS_KEYLIST_DEVICEMANAGER_CONFIG_PATH  (QStringList() << SETTINGS_KEY_DEVICEMANAGER << "configPath")

static void ZWaveNotificationHandler(OpenZWave::Notification const* ntf, void* ctx)
{
    DeviceManagerP* dmp = (DeviceManagerP*)ctx;
    ZWEvent* ev = new ZWEvent(ntf);
    QCoreApplication::postEvent(dmp, ev);
}

DeviceManager::DeviceManager(QObject* parent)
    : QObject(parent)
    , d_ptr(new DeviceManagerP(this))
{
    connect(d_ptr, SIGNAL(ready()), this, SIGNAL(ready()));

    // Load settings
    Settings settings(SETTINGS_FILE);
    if (!settings.load()) {
        qWarning() << "Unable to open settings file" << SETTINGS_FILE;
    }

    d_ptr->device = settings.valueForKeyList(SETTINGS_KEYLIST_DEVICEMANAGER_DEVICE).toString();
    d_ptr->configPath = settings.valueForKeyList(SETTINGS_KEYLIST_DEVICEMANAGER_CONFIG_PATH).toString();

    d_ptr->running = false;
    d_ptr->home = 0;
    d_ptr->node = 0;

    // Register ZWEvent to Qt as an event type
    ZWEvent::registerZWEventType();

    QString vestahome(getenv("HOME"));
    if (vestahome.length() == 0) {
        qFatal("HOME environment variable not set!");
    }

    // Construct command line parameters for OZW and lock options.
    QStringList args = QCoreApplication::arguments();
    QString cmdLine = args.join(" ");
    Options* opts = Options::Create(d_ptr->configPath.toUtf8().constData(),
                                    vestahome.toUtf8().constData(),
                                    cmdLine.toUtf8().constData());
    if (!opts->Lock()) {
        qWarning() << "Unable to lock options.";
        exit(1);
    }

    if (!Manager::Get()) {
        Manager::Create();
    }
}

DeviceManager::~DeviceManager()
{
    stop();

    delete d_ptr;
    d_ptr = NULL;

    Manager::Destroy();
    Options::Destroy();
}

uint32 DeviceManager::home() const
{
    return d_ptr->home;
}

uint8 DeviceManager::node() const
{
    return d_ptr->node;
}

bool DeviceManager::start()
{
    if (d_ptr->running) {
        return true;
    }

    if (Manager::Get()->AddWatcher(ZWaveNotificationHandler, d_ptr)) {
        if (Manager::Get()->AddDriver(d_ptr->device.toUtf8().constData())) {
            d_ptr->running = true;
            emit stateChanged(running());

            qDebug() << "DeviceManager is running.";
        }
    }

    return d_ptr->running;
}

void DeviceManager::stop()
{
    Manager::Get()->RemoveWatcher(ZWaveNotificationHandler, d_ptr);

    if (d_ptr->running) {
        d_ptr->running = false;
        emit stateChanged(running());
    }

    qDebug() << "DeviceManager is stopped.";
}

bool DeviceManager::running() const
{
    return d_ptr->running;
}

void DeviceManager::reset(bool hard)
{
    if (hard) {
        Manager::Get()->ResetController(d_ptr->home);
    } else {
        Manager::Get()->SoftReset(d_ptr->home);
    }
}

void DeviceManager::addDevice()
{
#ifdef Q_OS_MAC
    Manager::Get()->BeginControllerCommand(d_ptr->home, Driver::ControllerCommand_AddDevice);
#else
    Manager::Get()->AddNode(d_ptr->home);
#endif
}

void DeviceManager::removeDevice()
{
#ifdef Q_OS_MAC
    Manager::Get()->BeginControllerCommand(d_ptr->home, Driver::ControllerCommand_RemoveDevice);
#else
    Manager::Get()->RemoveNode(d_ptr->home);
#endif
}

void DeviceManager::healNetwork()
{
    Manager::Get()->HealNetwork(d_ptr->home, false);
}

DeviceNode* DeviceManager::deviceNode(uint8 node) const
{
    return d_ptr->devices[node];
}

DeviceNode* DeviceManager::deviceNode(const QString& deviceName) const
{
    for (QMap<uint8,DeviceNode*>::const_iterator it = d_ptr->devices.begin(); it != d_ptr->devices.end(); ++it) {
        DeviceNode* dn = (*it);
        if (dn->name().length() > 0 && dn->name() == deviceName) {
            return dn;
        }
    }

    return NULL;
}

QMap <uint8, DeviceNode*> DeviceManager::devices() const
{
    return d_ptr->devices;
}

void DeviceManager::onEnergySourcePriorityChanged(EnergySource* energySource, EnergySourcePriority priority)
{
    QString devName = energySource->switchDevice();
    DeviceNode* dn = deviceNode(devName);
    if (!dn) {
        qWarning() << "No device node found for energysource with device name:" << devName;
        return;
    }

    QString swVal = energySource->switchValue();
    NodeValue* nv = dn->value(swVal);
    if (!nv) {
        qWarning() << "Unable to find value named:" << swVal << "from device" << devName;
        return;
    }

    switch (priority) {
        case EnergySourcePriorityLow: {
            qDebug() << "Turning OFF switch:" << swVal << "in device:" << devName;
            nv->turnOff();
        } break;

        case EnergySourcePriorityHigh: {
            qDebug() << "Turning ON switch:" << swVal << "in device:" << devName;
            nv->turnOn();
        } break;
    }
}

//
// Private implementation
//

DeviceManagerP::DeviceManagerP(QObject* parent)
    : QObject(parent)
{
}

DeviceManagerP::~DeviceManagerP()
{
    while (!devices.isEmpty()) {
        delete devices.take(devices.lastKey());
    }

    home = 0;
    node = 0;
}

bool DeviceManagerP::event(QEvent* event)
{
    if (event->type() == ZWEvent::ZWEventType()) {
        onNotification(static_cast<ZWEvent*>(event));
        return true;
    } else {
        return false;
    }
}

void DeviceManagerP::onNotification(ZWEvent* event)
{
    switch (event->type()) {
        case Notification::Type_DriverReady: {
            home = event->home();
            node = event->node();
            qDebug() << "OZW driver is ready. Home Id:" << QByteArray::number(home, 16) << "Node Id:" << QByteArray::number(node, 16);
        } break;

        case Notification::Type_ValueAdded: {
            DeviceNode* dn = devices[event->node()];
            if (dn) {
                dn->d_ptr->addValue(event->vid());
            }
        } break;

        case Notification::Type_ValueChanged: {
            qDebug() << "Value change for node" << event->node();
            DeviceNode* dn = devices[event->node()];
            if (dn) {
                dn->d_ptr->handleValueChanged(event->vid());
            }
        } break;

        case Notification::Type_ValueRemoved: {
            DeviceNode* dn = devices[event->node()];
            if (dn) {
                dn->d_ptr->removeValue(event->vid());
            }
        } break;

        case Notification::Type_NodeAdded: {
            qDebug() << "Node" << event->node() << "added.";
            DeviceNode* dn = new DeviceNode(event->home(), event->node());
            devices.insert(event->node(), dn);
            connect(dn, SIGNAL(valueChanged(DeviceNode*,NodeValue*)),
                    this, SLOT(onDeviceNodeValueChanged(DeviceNode*,NodeValue*)));
        } break;

        case Notification::Type_NodeRemoved: {
            qDebug() << "Node" << event->node() << "removed.";
            devices.remove(event->node());
        } break;

        case Notification::Type_Notification: {
            qDebug() << "Notification from node:" << event->node() << ":" << event->string();
        } break;

        case Notification::Type_AwakeNodesQueried:
        case Notification::Type_AllNodesQueriedSomeDead:
        case Notification::Type_AllNodesQueried: {
            qDebug() << "All nodes queried.";
            emit ready();
        } break;

        case Notification::Type_NodeNaming: {
            // qDebug() << "Node" << event->node() << "reported of a naming change";
        } break;

        case Notification::Type_ControllerCommand: {
            qDebug() << "Node" << event->node() << "sent controller command with event:" << event->event() << "and notif.code:" << event->code();
        } break;

        default: {
            // qDebug() << "No handler for notification type:" << event->type();
        } break;
    }
}

void DeviceManagerP::onDeviceNodeValueChanged(DeviceNode* node, NodeValue* value)
{
    Q_ASSERT(node);
    Q_ASSERT(value);

    DeviceManager* dm = qobject_cast<DeviceManager*>(parent());
    emit dm->deviceNodeValueChanged(node->name(), value->label(), value->currentValue());
}
