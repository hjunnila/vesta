#pragma once

#define SETTINGS_KEY_NORDPOOL               "nordpool"
#define SETTINGS_KEYLIST_NORDPOOL_URL       (QStringList() << SETTINGS_KEY_NORDPOOL << "url")
#define SETTINGS_KEYLIST_NORDPOOL_AREA      (QStringList() << SETTINGS_KEY_NORDPOOL << "area")
#define SETTINGS_KEYLIST_NORDPOOL_TIME      (QStringList() << SETTINGS_KEY_NORDPOOL << "time")

class PriceDataEntry;
class QNetworkReply;
class QJsonObject;
class NordPoolP;
class PriceData;

class NordPool : public QObject
{
    Q_OBJECT

public:
    /** Create a NordPool instance with default settings */
    static NordPool* createWithDefaultSettings();

    NordPool(const QString& url, const QString& area, const QTime& dailyPollTime);
    ~NordPool();

    /** Start the daily network poll routine. */
    void startPoll();

    /** Stop the daily network poll routine. */
    void stopPoll();

    /** Check, if this instance is currently active. */
    bool isPolling() const;

signals:
    /** Emitted when new prices have been fetched. */
    void priceDataUpdated(const PriceData& priceData);

private:
    void fetchPrices();
    void cleanupAfterNetworkActivity();
    void storeDataToFile(const QByteArray& data);
    void parseByteArray(const QByteArray& byteArray);
    void parseJsonDataObject(const QJsonObject& data);
    QList<PriceDataEntry> parsePriceRows(const QJsonArray& rows);
    QDateTime stockholmDateToLocalDate(const QString& string) const;

private slots:
    void onReplyTimeout();
    void onReplyFinished(QNetworkReply* reply);
    void onTimerEvent();

private:
    NordPoolP* d_ptr;
};
