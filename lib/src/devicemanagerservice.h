#pragma once
#include "service.h"
#include "nodevalue.h"

class DeviceManagerServiceP;
class DeviceManager;
class DeviceNode;

/**
 * Produces an HTTP(S) service for getting access to the list of devices managed by
 * a DeviceManager instance.
 *
 * API:
 *  - GET /devices: Get a list of currently known devices and their values.
 *
 *  - POST /devices/add: Start the device add procedure. You should push an "add" button on
 *         the Z-Wave device you want to add after calling this. No body is expected for the
 *         request.
 *
 *  - GET /devices/<nodeid>: Get a single device entry, whose nodeid matches the URL param.
 *
 *  - POST /devices/<nodeid>: Edit the device whose nodeid matches. Only the [name]
 *         param can be changed; other keys are ignored.
 *         Example body to change the name of a device: { "name": "foobar" }
 *
 *  - GET /devices/<nodeid>/value/<vid>: Get the current state of a value (vid) within the
 *        given node. Note that the vid may change between service restarts.
 *
 *  - POST /devices/<nodeid>/value/<vid>: Set the current state of a value (vid) within the
 *        given node. Only the [current] value can be set; other keys are ignored.
 *        Example body for a button: { "current": true }
 *
 */
class DeviceManagerService : public Service
{
    Q_OBJECT
    Q_DISABLE_COPY(DeviceManagerService)

public:
    DeviceManagerService(DeviceManager* deviceManager);
    virtual ~DeviceManagerService();

public slots:
    Response getDevices(const Request& request);
    Response getDevice(const Request& request);

    Response postAddDevice(const Request& request);
    Response postRemoveDevice(const Request& request);

    Response postEditDevice(const Request& request);

    Response getDeviceValue(const Request& request);
    Response postEditDeviceValue(const Request& request);

private:
    DeviceManagerServiceP* d_ptr;
};
