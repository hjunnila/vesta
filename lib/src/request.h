#pragma once

class RequestP;
class Request
{
public:
    /** Default constructor */
    Request();

    /** Construct from request data received from network */
    Request(const QByteArray& byteArray);

    /** Copy constructor */
    Request(const Request& request);

    /** Destructor */
    ~Request();

    /** Copy operator */
    Request& operator=(const Request& request);

    /** Get the HTTP method */
    QString method() const;

    /** Get the request path */
    QString path() const;

    /** Get the query part */
    QString query() const;

    /** Get the request protocol and version */
    QString protocol() const;

    /** Form data found from the query path */
    QMap <QString,QString> formData() const;

    /** Get all HTTP headers in the request */
    QMap <QString,QString> headers() const;

    /** Check if all headers are present (i.e. body-separating CRLF parsed) */
    bool hasAllHeaders() const;

    /** Get the request body */
    QString body() const;

private:
    RequestP* d_ptr;
};
