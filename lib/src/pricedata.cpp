#include "pricedataentry.h"
#include "pricedata.h"
#include "pricedata_p.h"

PriceData::PriceData()
    : d_ptr(new PriceDataP)
{
    d_ptr->error = QSqlError::NoError;
}

PriceData::PriceData(const QString& area, const QDateTime& start, const QDateTime& end, const QDateTime& updated, const QList <PriceDataEntry> prices)
    : d_ptr(new PriceDataP)
{
    d_ptr->area = area;
    d_ptr->start = start;
    d_ptr->end = end;
    d_ptr->updated = updated;
    d_ptr->prices = prices;
    d_ptr->error = QSqlError::NoError;
}

PriceData::PriceData(const QString& area, const QSqlError& error)
    : d_ptr(new PriceDataP)
{
    d_ptr->area = area;
    d_ptr->error = error.type();
}

PriceData::PriceData(const PriceData& priceData)
    : d_ptr(new PriceDataP)
{
    d_ptr->area = priceData.d_ptr->area;
    d_ptr->start = priceData.d_ptr->start;
    d_ptr->end = priceData.d_ptr->end;
    d_ptr->updated = priceData.d_ptr->updated;
    d_ptr->prices = priceData.d_ptr->prices;
    d_ptr->error = priceData.d_ptr->error;
}

PriceData& PriceData::operator=(const PriceData& priceData)
{
    if (this != &priceData) {
        d_ptr->area = priceData.d_ptr->area;
        d_ptr->start = priceData.d_ptr->start;
        d_ptr->end = priceData.d_ptr->end;
        d_ptr->updated = priceData.d_ptr->updated;
        d_ptr->prices = priceData.d_ptr->prices;
        d_ptr->error = priceData.d_ptr->error;
    }

    return *this;
}

PriceData::~PriceData()
{
    delete d_ptr;
    d_ptr = NULL;
}

QString PriceData::area() const
{
    return d_ptr->area;
}

QList <PriceDataEntry> PriceData::prices() const
{
    return d_ptr->prices;
}

QDateTime PriceData::start() const
{
    return d_ptr->start;
}

QDateTime PriceData::end() const
{
    return d_ptr->end;
}

QDateTime PriceData::updated() const
{
    return d_ptr->updated;
}

QSqlError::ErrorType PriceData::error() const
{
    return d_ptr->error;
}

void PriceData::applyCosts(double energyTax, double transfer, double transferTax, double premium, double premiumTax)
{
    for (QList <PriceDataEntry>::iterator it = d_ptr->prices.begin(); it != d_ptr->prices.end(); ++it) {
        it->setEnergyTax(energyTax);
        it->setTransfer(transfer);
        it->setTransferTax(transferTax);
        it->setPremium(premium);
        it->setPremiumTax(premiumTax);
    }
}
