#include "energyplanentry_p.h"
#include "energyplanentry.h"

EnergyPlanEntry::EnergyPlanEntry()
    : d_ptr(new EnergyPlanEntryP)
{
    d_ptr->start = currentHourStart();
    d_ptr->end = currentHourEnd();
    d_ptr->oil = -1;
    d_ptr->energy = -1;
    d_ptr->transfer = -1;
    d_ptr->premium = -1;
    d_ptr->planType = PriceBasedPlan;
}

EnergyPlanEntry::EnergyPlanEntry(const EnergyPlanEntry& entry)
    : d_ptr(new EnergyPlanEntryP)
{
    d_ptr->start = entry.start();
    d_ptr->end = entry.end();
    d_ptr->oil = entry.oil();
    d_ptr->energy = entry.energy();
    d_ptr->transfer = entry.transfer();
    d_ptr->premium = entry.premium();
    d_ptr->planType = entry.planType();
}

EnergyPlanEntry::EnergyPlanEntry(const EnergyPlanEntry& entry, PlanType planType)
    : d_ptr(new EnergyPlanEntryP)
{
    d_ptr->start = entry.start();
    d_ptr->end = entry.end();
    d_ptr->oil = entry.oil();
    d_ptr->energy = entry.energy();
    d_ptr->transfer = entry.transfer();
    d_ptr->premium = entry.premium();
    d_ptr->planType = planType;
}

EnergyPlanEntry::EnergyPlanEntry(const QDateTime& start, const QDateTime& end, double oil, double energy, double transfer, double premium, PlanType planType)
    : d_ptr(new EnergyPlanEntryP)
{
    d_ptr->start = start;
    d_ptr->end = end;
    d_ptr->oil = oil;
    d_ptr->energy = energy;
    d_ptr->transfer = transfer;
    d_ptr->premium = premium;
    d_ptr->planType = planType;
}

EnergyPlanEntry& EnergyPlanEntry::operator=(const EnergyPlanEntry& entry)
{
    if (this != &entry) {
        d_ptr->start = entry.start();
        d_ptr->end = entry.end();
        d_ptr->oil = entry.oil();
        d_ptr->energy = entry.energy();
        d_ptr->transfer = entry.transfer();
        d_ptr->premium = entry.premium();
        d_ptr->planType = entry.planType();
    }

    return *this;
}

bool EnergyPlanEntry::operator==(const EnergyPlanEntry& entry) const
{
    if (this != &entry) {
        if (d_ptr->start != entry.start()) return false;
        if (d_ptr->end != entry.end()) return false;
        if (d_ptr->oil != entry.oil()) return false;
        if (d_ptr->energy != entry.energy()) return false;
        if (d_ptr->transfer != entry.transfer()) return false;
        if (d_ptr->premium != entry.premium()) return false;
        if (d_ptr->planType != entry.planType()) return false;
    }

    return true;
}

bool EnergyPlanEntry::operator!=(const EnergyPlanEntry& entry) const
{
    return !(this == &entry);
}

EnergyPlanEntry::~EnergyPlanEntry()
{
    delete d_ptr;
    d_ptr = NULL;
}

bool EnergyPlanEntry::isValid() const
{
    if (!d_ptr->start.isValid())
        return false;

    if (!d_ptr->end.isValid())
        return false;

    if (d_ptr->oil < 0)
        return false;

    if (d_ptr->energy < 0)
        return false;

    if (d_ptr->transfer < 0)
        return false;

    if (d_ptr->premium < 0)
        return false;

    return true;
}

EnergyPlanEntry::PlanType EnergyPlanEntry::planType() const
{
    return (EnergyPlanEntry::PlanType) d_ptr->planType;
}

QDateTime EnergyPlanEntry::start() const
{
    return d_ptr->start;
}

QDateTime EnergyPlanEntry::end() const
{
    return d_ptr->end;
}

double EnergyPlanEntry::oil() const
{
    return d_ptr->oil;
}

double EnergyPlanEntry::energy() const
{
    return d_ptr->energy;
}

double EnergyPlanEntry::transfer() const
{
    return d_ptr->transfer;
}

double EnergyPlanEntry::premium() const
{
    return d_ptr->premium;
}

double EnergyPlanEntry::oilPerKWh() const
{
    return d_ptr->oil;
}

double EnergyPlanEntry::electricPerKWh() const
{
    return d_ptr->energy + d_ptr->transfer + d_ptr->premium;
}

QDateTime EnergyPlanEntry::currentHourStart()
{
    QDateTime start(QDateTime::currentDateTime());
    start.setTime(QTime(start.time().hour(), 0, 0, 0));
    return start;
}

QDateTime EnergyPlanEntry::currentHourEnd()
{
    QDateTime end(EnergyPlanEntry::currentHourStart());
    end = end.addSecs(3600);
    return end;
}
