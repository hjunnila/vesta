#pragma once

#define HTTP_HEAD "HEAD"
#define HTTP_GET "GET"
#define HTTP_POST "POST"

#define HTTP_PROTOCOL_11 "HTTP/1.1"
#define HTTP_CRLF "\r\n"
#define HTTP_SPACE " "
#define HTTP_COLON ":"

#define HTTP_HEADER_DATE "Date"
#define HTTP_HEADER_SERVER "Server"
#define HTTP_HEADER_CONTENT_LENGTH "Content-Length"
#define HTTP_HEADER_CONTENT_TYPE "Content-Type"
#define HTTP_HEADER_ACCESS_CONTROL_ALLOW_ORIGIN "Access-Control-Allow-Origin"
#define HTTP_HEADER_ACCESS_CONTROL_EXPOSE_HEADERS "Access-Control-Expose-Headers"

#define HTTP_APPLICATION_JSON "application/json"
