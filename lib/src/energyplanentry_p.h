#pragma once

class EnergyPlanEntryP
{
public:
    QDateTime start;
    QDateTime end;
    double oil;
    double energy;
    double transfer;
    double premium;
    uint planType;
};
