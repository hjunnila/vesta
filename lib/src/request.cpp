#include "request_p.h"
#include "request.h"
#include "http.h"

Request::Request()
    : d_ptr(new RequestP)
{
    d_ptr->hasAllHeaders = false;
}

Request::Request(const QByteArray& byteArray)
    : d_ptr(new RequestP)
{
    d_ptr->hasAllHeaders = false;
    d_ptr->parseFromData(byteArray);
}

Request::Request(const Request& request)
    : d_ptr(new RequestP)
{
    d_ptr->method = request.d_ptr->method;
    d_ptr->path = request.d_ptr->path;
    d_ptr->protocol = request.d_ptr->protocol;
    d_ptr->query = request.d_ptr->query;
    d_ptr->formData = request.d_ptr->formData;
    d_ptr->headers = request.d_ptr->headers;
    d_ptr->hasAllHeaders = request.d_ptr->hasAllHeaders;
    d_ptr->body = request.d_ptr->body;
}

Request::~Request()
{
    delete d_ptr;
    d_ptr = NULL;
}

Request& Request::operator=(const Request& request)
{
    delete d_ptr;
    d_ptr = new RequestP;

    if (this != &request) {
        d_ptr->method = request.d_ptr->method;
        d_ptr->path = request.d_ptr->path;
        d_ptr->protocol = request.d_ptr->protocol;
        d_ptr->query = request.d_ptr->query;
        d_ptr->headers = request.d_ptr->headers;
        d_ptr->formData = request.d_ptr->formData;
        d_ptr->hasAllHeaders = request.d_ptr->hasAllHeaders;
        d_ptr->body = request.d_ptr->body;
    }

    return *this;
}

QString Request::method() const
{
    return d_ptr->method;
}

QString Request::path() const
{
    return d_ptr->path;
}

QString Request::query() const
{
    return d_ptr->query;
}

QString Request::protocol() const
{
    return d_ptr->protocol;
}

QMap <QString,QString> Request::formData() const
{
    return d_ptr->formData;
}

QMap <QString,QString> Request::headers() const
{
    return d_ptr->headers;
}

bool Request::hasAllHeaders() const
{
    return d_ptr->hasAllHeaders;
}

QString Request::body() const
{
    return d_ptr->body;
}

//
// Private implementation
//

void RequestP::parseFromData(const QByteArray& byteArray)
{
    QString buf(byteArray);

    QStringList rows = buf.split(HTTP_CRLF);
    if (rows.count() < 2) {
        return;
    }

    // First line e.g. "POST /foo/bar?x=y&z=r HTTP/1.1"
    QStringList line = rows[0].split(HTTP_SPACE);
    if (line.count() >= 3) {
        method = line[0];
        path = line[1];
        protocol = line[2];

        QStringList pathParts = line[1].split(QRegExp("(\\?|\%3F)"));
        if (pathParts.count() == 2) {
            // There's a "?" or "%3F" there, separating path from query.
            path = pathParts[0];
            query = pathParts[1];

            // Go thru all form fields and values
            QStringList queryParts = query.split(QRegExp("(\\&|\%26)"));
            for (QStringList::const_iterator it = queryParts.begin(); it != queryParts.end(); ++it) {
                QString keyValue = *it;
                QStringList keyAndValue = (*it).split(QRegExp("(\\=|\%3D)"));
                if (keyAndValue.count() == 2) {
                    // "field=value"
                    formData[keyAndValue[0]] = keyAndValue[1];
                } else if (keyAndValue.count() == 1) {
                    // "field=" or just "something"
                    formData[keyAndValue[0]] = QString();
                }
            }
        } else {
            // No query part. Consider the whole thing as being part of the path.
            path = line[1];
        }
    }

    // Headers e.g. "Content-Type: application/json"
    int i;
    for (i = 1; i < rows.count(); i++) {
        if (rows[i].length() == 0) {
            // First empty row is the end of HTTP header part & start of body.
            hasAllHeaders = true;
            break;
        }

        // Split each header line by the colon. Trim whitespace from both ends.
        line = rows[i].split(HTTP_COLON);
        if (line.count() == 2) {
            QString key = line[0].trimmed();
            QString value = line[1].trimmed();
            if (key.length() > 0) {
                headers[key] = value;
            }
        }
    }

    // Body (skip over the extra CRLF between headers and body)
    if (hasAllHeaders) {
        int cl = headers.value(HTTP_HEADER_CONTENT_LENGTH).toInt();
        if (cl > 0) {
            for (; i < rows.count(); i++) {
                if (rows[i].length() == 0) {
                    continue;
                }

                body.append(rows[i]);
            }
        }
    }
}
