#pragma once
#include "service.h"

class FileServiceP;

/**
 * Produces an HTTP(S) service for getting files (HTML & JS)
 *
 */
class FileService : public Service
{
    Q_OBJECT
    Q_DISABLE_COPY(FileService)

public:
    FileService(QObject* parent = 0, const QString& serverRoot = "/var/www/html/vesta");
    virtual ~FileService();

public slots:
    Response getFile(const Request& request);

private:
    FileServiceP* d_ptr;
};

