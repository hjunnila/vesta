#pragma once

class EnergyUsageEntryP
{
public:
    QDateTime start;
    QDateTime end;
    int elSeconds;
    int elAveragePower;
    int oilSeconds;
    int oilAveragePower;
    QString error;
};
