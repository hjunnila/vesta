#pragma once

class DeviceManager;
class DeviceNode;
class NodeValue;

class DeviceManagerServiceP
{
public:
    DeviceManager* deviceManager;

public:
    DeviceNode* deviceNodeFromPath(const QString& path) const;
    NodeValue* nodeValueFromPath(const QString& path) const;
    QJsonObject deviceAsJson(const DeviceNode* dn) const;
    QJsonArray nodeValues(QList <NodeValue*> values) const;
    QJsonObject nodeValueAsJson(const NodeValue* nodeValue) const;
};
