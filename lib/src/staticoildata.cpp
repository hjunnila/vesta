#include "staticoildata.h"
#include "staticoildata_p.h"

StaticOilData::StaticOilData()
    : d_ptr(new StaticOilDataP)
{
    d_ptr->litrePrice = "0";
    d_ptr->efficiency = "0";
    d_ptr->kWhPerLitre = "0";

    d_ptr->switchDevice = "";
    d_ptr->switchValue = "";

    d_ptr->powerDevice = "";
    d_ptr->powerValue = "";
}

StaticOilData::StaticOilData(const StaticOilData& data)
    : d_ptr(new StaticOilDataP)
{
    d_ptr->litrePrice = data.d_ptr->litrePrice;
    d_ptr->efficiency = data.d_ptr->efficiency;
    d_ptr->kWhPerLitre = data.d_ptr->kWhPerLitre;

    d_ptr->switchDevice = data.d_ptr->switchDevice;
    d_ptr->switchValue = data.d_ptr->switchValue;

    d_ptr->powerDevice = data.d_ptr->powerDevice;
    d_ptr->powerValue = data.d_ptr->powerValue;
}

StaticOilData::StaticOilData(const QString& litrePrice, const QString& efficiency, const QString& kWhPerLitre, const QString& switchDevice, const QString& switchValue, const QString& powerDevice, const QString& powerValue)
    : d_ptr(new StaticOilDataP)
{
    d_ptr->litrePrice = litrePrice;
    d_ptr->efficiency = efficiency;
    d_ptr->kWhPerLitre = kWhPerLitre;

    d_ptr->switchDevice = switchDevice;
    d_ptr->switchValue = switchValue;

    d_ptr->powerDevice = powerDevice;
    d_ptr->powerValue = powerValue;
}

StaticOilData::~StaticOilData()
{
    delete d_ptr;
    d_ptr = NULL;
}

StaticOilData& StaticOilData::operator=(const StaticOilData& data)
{
    if (this != &data) {
        d_ptr->litrePrice = data.d_ptr->litrePrice;
        d_ptr->efficiency = data.d_ptr->efficiency;
        d_ptr->kWhPerLitre = data.d_ptr->kWhPerLitre;

        d_ptr->switchDevice = data.d_ptr->switchDevice;
        d_ptr->switchValue = data.d_ptr->switchValue;

        d_ptr->powerDevice = data.d_ptr->powerDevice;
        d_ptr->powerValue = data.d_ptr->powerValue;
    }

    return *this;
}

void StaticOilData::setLitrePrice(const QString& litrePrice)
{
    d_ptr->litrePrice = litrePrice;
}

QString StaticOilData::litrePrice() const
{
    return d_ptr->litrePrice;
}

double StaticOilData::litrePriceDouble() const
{
    return d_ptr->litrePrice.toDouble();
}

void StaticOilData::setEfficiency(const QString& efficiency)
{
    d_ptr->efficiency = efficiency;
}

QString StaticOilData::efficiency() const
{
    return d_ptr->efficiency;
}

double StaticOilData::efficiencyDouble() const
{
    return d_ptr->efficiency.toDouble();
}

void StaticOilData::setKWhPerLitre(const QString& kWhPerLitre)
{
    d_ptr->kWhPerLitre = kWhPerLitre;
}

QString StaticOilData::kWhPerLitre() const
{
    return d_ptr->kWhPerLitre;
}

double StaticOilData::kWhPerLitreDouble() const
{
    return d_ptr->kWhPerLitre.toDouble();
}

void StaticOilData::setSwitchDevice(const QString& switchDevice)
{
    d_ptr->switchDevice = switchDevice;
}

QString StaticOilData::switchDevice() const
{
    return d_ptr->switchDevice;
}

void StaticOilData::setSwitchValue(const QString& switchValue)
{
    d_ptr->switchValue = switchValue;
}

QString StaticOilData::switchValue() const
{
    return d_ptr->switchValue;
}

void StaticOilData::setPowerDevice(const QString& powerDevice)
{
    d_ptr->powerDevice = powerDevice;
}

QString StaticOilData::powerDevice() const
{
    return d_ptr->powerDevice;
}

void StaticOilData::setPowerValue(const QString& powerValue)
{
    d_ptr->powerValue = powerValue;
}

QString StaticOilData::powerValue() const
{
    return d_ptr->powerValue;
}
