#include "database.h"
#include "database_p.h"
#include "database.h"
#include "settings.h"

Database::Database(const DatabaseInfo& info)
    : QObject()
    , d_ptr(new DatabaseP)
{
    d_ptr->user = info.user;
    d_ptr->secret = info.secret;
    d_ptr->host = info.host;
    d_ptr->dbName = info.dbName;
    d_ptr->options = info.options;
    d_ptr->dbDriver = info.dbDriver;
}

Database::~Database()
{
    close();

    QSqlDatabase::removeDatabase(d_ptr->dbName);

    delete d_ptr;
    d_ptr = NULL;
}

DatabaseInfo Database::defaultDatabaseInfo()
{
    DatabaseInfo info;

    // Load settings
    Settings settings(DATABASE_SETTINGS_FILE);
    if (!settings.load()) {
        qWarning() << "Unable to open settings file" << DATABASE_SETTINGS_FILE;
        return info;
    }

    info.user = settings.valueForKeyList(SETTINGS_KEYLIST_DATABASE_USER).toString();
    info.secret = settings.valueForKeyList(SETTINGS_KEYLIST_DATABASE_PASSWORD).toString();
    info.host = settings.valueForKeyList(SETTINGS_KEYLIST_DATABASE_HOST).toString(),
    info.dbName = settings.valueForKeyList(SETTINGS_KEYLIST_DATABASE_DBNAME).toString();
    info.options = QString();
    info.dbDriver = QString("QMYSQL");

    return info;
}

bool Database::open()
{
    QSqlDatabase db = QSqlDatabase::database(d_ptr->dbName);
    if (!db.isValid()) {
        db = QSqlDatabase::addDatabase(d_ptr->dbDriver, d_ptr->dbName);
        if (!db.isValid()) {
            qWarning() << Q_FUNC_INFO << "Unable to add database" << db.lastError();
            close();
            return false;
        }
    }

    db.setDatabaseName(d_ptr->dbName);
    db.setHostName(d_ptr->host);
    db.setUserName(d_ptr->user);
    db.setPassword(d_ptr->secret);
    db.setConnectOptions(d_ptr->options);
    if (!db.open()) {
        qWarning() << "Unable to open database named:" << d_ptr->dbName << ":" << db.lastError();
        close();
        return false;
    }

    QStringList tables = db.tables();
    QStringList checkTables = essentialTables();
    for (int i = 0; i < checkTables.count(); i++) {
        if (!tables.contains(checkTables[i])) {
            if (!createTable(checkTables[i])) {
                qWarning() << "Unable to create essential table:" << checkTables[i];
                close();
                return false;
            }
        }
    }

    // close();
    return true;
}

void Database::close()
{
    database().close();
}

QSqlDatabase Database::database() const
{
    return QSqlDatabase::database(d_ptr->dbName);
}
