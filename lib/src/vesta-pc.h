#if defined __cplusplus
// Qt
#include <QtNetwork>
#include <QtCore>
#include <QtSql>
#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonValue>

// C++
#include <string>

// OpenZWave
#include <Notification.h>
#include <Manager.h>
#include <Options.h>
#include <Basic.h>

// Vesta
#include "macros.h"

using namespace std;
using namespace OpenZWave;

#endif
