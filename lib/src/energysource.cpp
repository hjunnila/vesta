#include "energysource_p.h"
#include "energysource.h"

EnergySource::EnergySource()
    : d_ptr(new EnergySourceP)
{
    d_ptr->currentState = false;
    d_ptr->secondsOn = 0;
    d_ptr->currentPower = 0;
    d_ptr->cumulativePower = 0;
    d_ptr->numberOfSamples = 0;
    d_ptr->logStartDate = QDateTime::currentDateTime();
}

EnergySource::~EnergySource()
{
    delete d_ptr;
    d_ptr = NULL;
}

QDateTime EnergySource::logStartDate() const
{
    return d_ptr->logStartDate;
}

void EnergySource::logUsageChange(bool on)
{
    if (on != d_ptr->currentState) {
        QDateTime now = QDateTime::currentDateTime();
        if (d_ptr->currentState == true) {
            // Switched from ON to OFF. Add elapsed ON seconds to existing data.
            d_ptr->secondsOn += (d_ptr->lastStateChange.secsTo(now));
            qDebug() << "Logged usage switch for" << switchDevice() << "to OFF.";
        } else {
            // Switched from OFF to ON
            qDebug() << "Logged usage switch for" << switchDevice() << "to ON.";
        }

        d_ptr->lastStateChange = now;
        d_ptr->currentState = on;
    }
}

void EnergySource::logPowerUsage(int watts)
{
    qDebug() << "Logging power consumption for" << powerDevice() << "at" << watts << "watts.";
    d_ptr->currentPower = watts;

    if (d_ptr->currentState == true) {
        d_ptr->cumulativePower += watts;
        d_ptr->numberOfSamples++;
    }
}

int EnergySource::cumulativeOnTimeAndReset()
{
    int seconds = d_ptr->secondsOn;

    d_ptr->secondsOn = 0;
    d_ptr->lastStateChange = QDateTime::currentDateTime();
    d_ptr->logStartDate = QDateTime::currentDateTime();

    return seconds;
}

int EnergySource::averagePowerConsumptionWhileOnAndReset()
{
    // Prevent division by zero errors when there are no samples. 0/1 == 0 anyways.
    int average = d_ptr->cumulativePower / max(1, d_ptr->numberOfSamples);
    d_ptr->cumulativePower = 0;
    d_ptr->numberOfSamples = 0;

    if (d_ptr->currentState == true) {
        d_ptr->cumulativePower = d_ptr->currentPower;
        d_ptr->numberOfSamples++;
    }
    return average;
}

