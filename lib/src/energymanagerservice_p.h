#pragma once

class EnergyManager;
class EnergyPlanEntry;
class EnergyManagerServiceP
{
public:
    EnergyManager* energyManager;

public:
    QJsonObject energyPlanEntryAsJson(const EnergyPlanEntry& entry) const;

    QJsonObject staticElectricDataAsJson();
    bool setStaticElectricDataAsJson(const QJsonObject& object);

    QJsonObject staticOilDataAsJson();
    bool setStaticOilDataAsJson(const QJsonObject& object);
};
