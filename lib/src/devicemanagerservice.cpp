#include "devicemanagerservice_p.h"
#include "devicemanagerservice.h"
#include "devicemanager.h"
#include "devicenode.h"
#include "nodevalue.h"
#include "response.h"
#include "request.h"
#include "server.h"
#include "http.h"

#define KEY_NAME "name"
#define KEY_VALUES "values"

DeviceManagerService::DeviceManagerService(DeviceManager* deviceManager)
    : Service(deviceManager)
    , d_ptr(new DeviceManagerServiceP)
{
    Q_ASSERT(deviceManager);
    d_ptr->deviceManager = deviceManager;

    const QRegExp rxDevices("/devices");
    const QRegExp rxAddDevice("/devices/add");
    const QRegExp rxRemoveDevice("/devices/remove");
    const QRegExp rxAccessDevice("/devices/[0-9]{1,3}");
    const QRegExp rxAccessDeviceValue("/devices/[0-9]{1,3}/value/[0-9]+");

    addService(HTTP_GET, rxDevices, "getDevices");

    addService(HTTP_POST, rxAddDevice, "postAddDevice");
    addService(HTTP_POST, rxRemoveDevice, "postRemoveDevice");

    addService(HTTP_GET, rxAccessDevice, "getDevice");
    addService(HTTP_POST, rxAccessDevice, "postEditDevice");

    addService(HTTP_GET, rxAccessDeviceValue, "getDeviceValue");
    addService(HTTP_POST, rxAccessDeviceValue, "postEditDeviceValue");
}

DeviceManagerService::~DeviceManagerService()
{
    delete d_ptr;
    d_ptr = NULL;
}

Response DeviceManagerService::getDevices(const Request& request)
{
    qDebug() << Q_FUNC_INFO;

    Q_UNUSED(request);

    QJsonArray array;
    QMap <quint8,DeviceNode*> devices(d_ptr->deviceManager->devices());
    for (QMap <quint8,DeviceNode*>::const_iterator it = devices.begin(); it != devices.end(); ++it) {
        DeviceNode* dn = *it;
        array.append(d_ptr->deviceAsJson(dn));
    }

    QJsonObject root;
    root["devices"] = array;
    QJsonDocument doc(root);

    Response response;
    response.setStatus(200);
    response.setJsonBody(doc);
    return response;
}

Response DeviceManagerService::getDevice(const Request& request)
{
    qDebug() << Q_FUNC_INFO;

    Response response(Response::notFound());

    DeviceNode* dn = d_ptr->deviceNodeFromPath(request.path());
    if (dn) {
        QJsonDocument doc(d_ptr->deviceAsJson(dn));
        response.setStatus(200);
        response.setJsonBody(doc);
    }

    return response;
}

Response DeviceManagerService::postAddDevice(const Request& request)
{
    qDebug() << Q_FUNC_INFO;

    Q_UNUSED(request);

    d_ptr->deviceManager->addDevice();

    Response response;
    response.setStatus(200);
    return response;
}

Response DeviceManagerService::postRemoveDevice(const Request& request)
{
    qDebug() << Q_FUNC_INFO;

    Q_UNUSED(request);

    d_ptr->deviceManager->removeDevice();

    Response response;
    response.setStatus(200);
    return response;
}

Response DeviceManagerService::postEditDevice(const Request& request)
{
    qDebug() << Q_FUNC_INFO;

    DeviceNode* dn = d_ptr->deviceNodeFromPath(request.path());
    if (!dn) {
        return Response::notFound();
    }

    QJsonParseError error;
    QJsonDocument doc = QJsonDocument::fromJson(request.body().toUtf8(), &error);
    if (error.error != QJsonParseError::NoError) {
        qDebug() << error.errorString();
        Response resp;
        resp.setStatus(400);
        return resp;
    }

    QJsonObject object = doc.object();
    QJsonValue val = object[KEY_NAME];
    if (val.isString()) {
        dn->setName(val.toString());
        Response response;
        response.setStatus(200);
        return response;
    }

    if (object.contains(KEY_VALUES)) {
        val = object[KEY_VALUES];
        if (val.isArray()) {
            QJsonArray arr = val.toArray();
            for (QJsonArray::const_iterator it = arr.begin(); it != arr.end(); ++it) {
            }
        }
    }

    return Response::notFound();
}

Response DeviceManagerService::getDeviceValue(const Request& request)
{
    qDebug() << Q_FUNC_INFO;

    Response response(Response::notFound());

    NodeValue* nv = d_ptr->nodeValueFromPath(request.path());
    if (nv) {
        QJsonDocument doc(d_ptr->nodeValueAsJson(nv));
        response.setStatus(200);
        response.setJsonBody(doc);
    }

    return response;
}

Response DeviceManagerService::postEditDeviceValue(const Request& request)
{
    qDebug() << Q_FUNC_INFO;

    Response resp;

    QJsonParseError error;
    QJsonDocument doc = QJsonDocument::fromJson(request.body().toUtf8(), &error);
    if (error.error != QJsonParseError::NoError) {
        qDebug() << error.errorString();
        resp.setError(400, "error_invalid_format", "Invalid JSON format.");
        return resp;
    }

    QJsonObject object = doc.object();
    QJsonValue val = object["current"];
    if (!val.isNull()) {
        NodeValue* nv = d_ptr->nodeValueFromPath(request.path());
        if (nv) {
            if (nv->setValue(val.toVariant()) == true) {
                resp.setStatus(204);
            } else {
                resp.setError(404, "error_wrong_type", "Unable to set value of given type.");
            }
        } else {
            resp.setError(404, "error_value_not_found", "Unable to find a value with given id.");
        }
    }

    return resp;
}

//
//Private implementation
//

DeviceNode* DeviceManagerServiceP::deviceNodeFromPath(const QString& path) const
{
    QRegExp rx("/devices/[0-9]+");
    if (rx.indexIn(path) >= 0) {
        if (rx.capturedTexts().count() == 1) {
            uint8 id = rx.capturedTexts()[0].remove("/devices/").toUShort();
            DeviceNode* dn = deviceManager->deviceNode(id);
            return dn;
        }
    }

    return NULL;
}

NodeValue* DeviceManagerServiceP::nodeValueFromPath(const QString& path) const
{
    DeviceNode* dn = deviceNodeFromPath(path);
    if (dn) {
        QRegExp rx("/value/[0-9]+");
        if (rx.indexIn(path) > 0) {
            qDebug() << "Match" << rx.capturedTexts();
            if (rx.capturedTexts().count() == 1) {
                uint64 vid = rx.capturedTexts()[0].remove("/value/").toULongLong();
                return dn->value(vid);
            }
        }
    }

    return NULL;
}

QJsonObject DeviceManagerServiceP::deviceAsJson(const DeviceNode* dn) const
{
    Q_ASSERT(dn);

    QJsonObject object;
    object["node"] = dn->node();
    object["manufacturer"] = dn->manufacturer();
    object["productName"] = dn->productName();
    object["name"] = dn->name();
    object["isAwake"] = dn->isAwake();
    object["values"] = nodeValues(dn->values());

    return object;
}

QJsonArray DeviceManagerServiceP::nodeValues(QList <NodeValue*> values) const
{
    QJsonArray array;
    for (QList <NodeValue*>::const_iterator it = values.begin(); it != values.end(); ++it) {
        QJsonObject object = nodeValueAsJson(*it);
        array.append(object);
    }

    return array;
}

QJsonObject DeviceManagerServiceP::nodeValueAsJson(const NodeValue* nv) const
{
    Q_ASSERT(nv);

    QJsonObject object;
    object["id"] = QString::number(nv->vid());
    object["label"] = nv->label();
    object["units"] = nv->units();
    object["type"] = nv->type();
    object["current"] = nv->currentValue().value<QString>();
    object["readonly"] = nv->isReadOnly();
    object["writeonly"] = nv->isWriteOnly();
    object["genre"] = nv->genre();

    if (nv->valueId().GetType() == ValueID::ValueType_List) {
        QStringList listItems(nv->listItems());
        if (listItems.count()) {
            QJsonArray itemArray;
            for (QStringList::const_iterator it = listItems.begin(); it != listItems.end(); ++it) {
                itemArray.append(*it);
            }
            object["items"] = itemArray;
        }
    }

    return object;
}
