#include "energyusageentry.h"
#include "energyplanentry.h"
#include "pricedataentry.h"
#include "energymanager.h"
#include "devicemanager.h"
#include "energyusagedb.h"
#include "energyplandb.h"
#include "devicenode.h"
#include "nodevalue.h"
#include "pricedata.h"
#include "electric.h"
#include "oil.h"
#include "energymanager_p.h"

#define RETRY_TIMER_MSEC (10 * 1000)

EnergyManager::EnergyManager()
    : QObject()
    , d_ptr(new EnergyManagerP(this))
{
    connect(d_ptr, SIGNAL(energySourcePriorityChanged(EnergySource*,EnergySourcePriority)),
            this, SIGNAL(energySourcePriorityChanged(EnergySource*,EnergySourcePriority)));
}

EnergyManager::~EnergyManager()
{
    stop();

    delete d_ptr;
    d_ptr = NULL;
}

void EnergyManager::start()
{
    if (!d_ptr->planTimer) {
        d_ptr->planTimer = new QTimer(this);
        connect(d_ptr->planTimer, SIGNAL(timeout()), d_ptr, SLOT(onPlanTimer()));
        d_ptr->planTimer->setInterval(1 * 60 * 1000); // 1 min
        d_ptr->planTimer->start();

        // Make sure we have an energyplan
        createNewEnergyPlans(QDateTime::currentDateTime());

        // Simulate the first timer tick
        d_ptr->onPlanTimer();
        
        qDebug() << "Energy management started.";
    }
}

void EnergyManager::stop()
{
    if (d_ptr->planTimer) {
        d_ptr->planTimer->stop();
        delete d_ptr->planTimer;
        d_ptr->planTimer = NULL;
    }
}

bool EnergyManager::running() const
{
    return (d_ptr->planTimer && d_ptr->planTimer->isActive());
}

void EnergyManager::createNewEnergyPlans(const QDateTime& startDateTime)
{
    d_ptr->createNewEnergyPlans(startDateTime);
}

QList <EnergyPlanEntry> EnergyManager::energyPlanEntriesForDate(const QDate& date)
{
    return d_ptr->plandb->energyPlanEntriesForDate(date);
}

void EnergyManager::onNewPriceDataAvailable(const QString& start, const QString& end)
{
    qDebug() << Q_FUNC_INFO << "start:" << start << "end:" << end;

    QDateTime startDateTime = QDateTime::fromString(start, Qt::ISODate);
    if (!startDateTime.isValid()) {
        startDateTime = QDateTime::currentDateTime();
    }

    createNewEnergyPlans(startDateTime);
}

void EnergyManager::onDeviceNodeValueChanged(const QString& deviceName, const QString& valueName, const QVariant& newValue)
{
    if (static_cast<QMetaType::Type> (newValue.type()) == QMetaType::Bool) {
        bool on = newValue.toBool();

        if (deviceName == electric()->switchDevice() && valueName == electric()->switchValue()) {
            electric()->logUsageChange(on);
        } else if (deviceName == oil()->switchDevice() && valueName == oil()->switchValue()) {
            oil()->logUsageChange(on);
        }
    } else if (static_cast<QMetaType::Type> (newValue.type()) == QMetaType::Int ||
               static_cast<QMetaType::Type> (newValue.type()) == QMetaType::Float ||
               static_cast<QMetaType::Type> (newValue.type()) == QMetaType::Double) {
        int power = newValue.toInt();

        if (deviceName == electric()->powerDevice() && valueName == electric()->powerValue()) {
            electric()->logPowerUsage(power);
        } else if (deviceName == oil()->powerDevice() && valueName == oil()->powerValue()) {
            oil()->logPowerUsage(power);
        }
    }
}

Electric* EnergyManager::electric() const
{
    return d_ptr->electric;
}

Oil* EnergyManager::oil() const
{
    return d_ptr->oil;
}

void EnergyManager::setManualElectricOverride(bool on)
{
    d_ptr->manualElectricOverride = on;
    d_ptr->updateCurrentHourEnergyPlan();
}

bool EnergyManager::isManualElectricOverride() const
{
    return d_ptr->manualElectricOverride;
}

//
// Private implementation
//

EnergyManagerP::EnergyManagerP(QObject* parent)
    : QObject(parent)
    , plandb(new EnergyPlanDB)
    , usagedb(new EnergyUsageDB)
    , oil(new Oil)
    , electric(new Electric)
    , planTimer(NULL)
    , currentHourPlan(this->failSafePlan(EnergyPlanEntry()))
    , retryElectricPriceStartDate(QDateTime())
    , manualElectricOverride(false)
{
}

EnergyManagerP::~EnergyManagerP()
{
    delete oil;
    oil = NULL;

    delete electric;
    electric = NULL;

    delete plandb;
    plandb = NULL;

    delete usagedb;
    usagedb = NULL;
}

void EnergyManagerP::onRetryElectricPriceFetch()
{
    qDebug() << Q_FUNC_INFO;
    createNewEnergyPlans(retryElectricPriceStartDate);
}

void EnergyManagerP::onPlanTimer()
{
    updateCurrentHourEnergyPlan();
}

void EnergyManagerP::onHourChange()
{
    qDebug() << "Hour has changed.";

    QDateTime start = electric->logStartDate();
    QDateTime end = QDateTime::currentDateTime();
    EnergyUsageEntry entry(start, end,
                           electric->cumulativeOnTimeAndReset(),
                           electric->averagePowerConsumptionWhileOnAndReset(),
                           oil->cumulativeOnTimeAndReset(),
                           oil->averagePowerConsumptionWhileOnAndReset());
    usagedb->storeEntry(entry);
}

void EnergyManagerP::createNewEnergyPlans(const QDateTime& startDateTime)
{
    qDebug() << Q_FUNC_INFO;

    PriceData priceData(electric->priceDataAfterIncluding(startDateTime));
    if (priceData.error() != QSqlError::NoError) {
        qWarning() << "Unable to get price data. Retrying after" << (RETRY_TIMER_MSEC / 1000) << "seconds.";
        retryElectricPriceStartDate = startDateTime;
        QTimer::singleShot(RETRY_TIMER_MSEC, this, SLOT(onRetryElectricPriceFetch()));
    } else {
        qDebug() << priceData.prices().count() << "electric price entries available";
        updateEnergyPlan(priceData);
    }
}

void EnergyManagerP::updateEnergyPlan(const PriceData& priceData)
{
    qDebug() << Q_FUNC_INFO;

    QList <EnergyPlanEntry> planEntries;

    QList <PriceDataEntry> entries(priceData.prices());
    for (QList <PriceDataEntry>::const_iterator it = entries.begin(); it != entries.end(); ++it) {
        const PriceDataEntry& entry(*it);
        planEntries << EnergyPlanEntry(entry.start(), entry.end(), oil->pricePerKWh(),
                                       entry.energyWithTax(), entry.transferWithTax(),
                                       entry.premiumWithTax());
    }

    plandb->storeEnergyPlanEntries(planEntries);

    updateCurrentHourEnergyPlan();
}

void EnergyManagerP::updateCurrentHourEnergyPlan()
{
    QDateTime now(QDateTime::currentDateTime());

    qint64 timeDiff = currentHourPlan.end().msecsTo(now);

    EnergyPlanEntry plan = plandb->energyPlanEntryForDate(now);
    if (plan.isValid()) {
        if (manualElectricOverride) {
            qDebug() << "Using manually overridden electric energy plan.";
            setCurrentEnergyPlan(manualElectricPlan(plan));
        } else {
            qDebug() << "Using price-based energy plan.";
            setCurrentEnergyPlan(plan);
        }
    } else {
        qWarning() << "Using failsafe plan. Unable to get a valid energy plan.";
        setCurrentEnergyPlan(failSafePlan(plan));
    }

    if (timeDiff > 0) {
        onHourChange();
    }
}

void EnergyManagerP::setCurrentEnergyPlan(const EnergyPlanEntry& plan)
{
    currentHourPlan = plan;

    EnergySourcePriority electricPriority = electricPriorityForPlan(plan);
    emit energySourcePriorityChanged(electric, electricPriority);
}

EnergySourcePriority EnergyManagerP::electricPriorityForPlan(const EnergyPlanEntry& plan)
{
    // No need to do calculations for these plans.
    if (plan.planType() == EnergyPlanEntry::FailSafePlan) {
        return EnergySourcePriorityHigh;
    } else if (plan.planType() == EnergyPlanEntry::ManualElectricPlan) {
        return EnergySourcePriorityHigh;
    }

    // Prevent division by zero errors and useless <-100% values
    double percent = 0;
    if (plan.electricPerKWh() > 0) {
        percent = (plan.oilPerKWh() / plan.electricPerKWh()) * 100;
    } else {
        percent = 100;
    }

    qDebug() << "Plan from" << plan.start().toString("hh:00") << "to" << plan.end().toString("hh:00")
             << "Oil:" << plan.oilPerKWh() << "c/kWh, Electric:" << plan.electricPerKWh() << "c/kWh, Diff %:" << percent;

    // Even when price difference is zero, treat electric as being cheaper.
    // It is more likely that you run out of oil than electricity.
    EnergySourcePriority priority;
    if (percent >= 100) {
        priority = EnergySourcePriorityHigh;
    } else {
        priority = EnergySourcePriorityLow;
    }

    return priority;
}

EnergyPlanEntry EnergyManagerP::failSafePlan(const EnergyPlanEntry& basedOnPlan)
{
    return EnergyPlanEntry(basedOnPlan, EnergyPlanEntry::FailSafePlan);
}

EnergyPlanEntry EnergyManagerP::manualElectricPlan(const EnergyPlanEntry& basedOnPlan)
{
    return EnergyPlanEntry(basedOnPlan, EnergyPlanEntry::ManualElectricPlan);
}
