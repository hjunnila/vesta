#pragma once
#include <QtNetwork>
#include "settings.h"

#define SERVER_SETTINGS_FILE         "server.json"
#define SETTINGS_KEY_SERVER          "server"
#define SETTINGS_KEY_SERVER_PORT     (QStringList() << SETTINGS_KEY_SERVER << "port")
#define SETTINGS_KEY_SERVER_CERT     (QStringList() << SETTINGS_KEY_SERVER << "sslCertificate")
#define SETTINGS_KEY_SERVER_PKEY     (QStringList() << SETTINGS_KEY_SERVER << "sslPrivateKey")

class Service;
class ServerP;
class Request;
class Server : public QTcpServer
{
    Q_OBJECT
    Q_DISABLE_COPY(Server)

public:
    Server(Settings settings = Settings(SERVER_SETTINGS_FILE), QObject* parent = 0);
    virtual ~Server();

public slots:
    /**
     * Start listening to connections.
     *
     * @return true if successful, otherwise false
     */
    virtual bool start();

    /**
     * Register a new service to the server.
     *
     * @param service The service to register
     */
    virtual void registerService(Service* service);

    /**
     * Stop serving requests to the given service.
     *
     * @param service The service to un-register
     */
    virtual void unregisterService(Service* service);

protected:
    void incomingConnection(qintptr socketDescriptor);

private:
    ServerP* d_ptr;
};
