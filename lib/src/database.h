#pragma once

#define DATABASE_SETTINGS_FILE              "database.json"
#define SETTINGS_KEY_DATABASE               "database"
#define SETTINGS_KEYLIST_DATABASE_USER      (QStringList() << SETTINGS_KEY_DATABASE << "user")
#define SETTINGS_KEYLIST_DATABASE_PASSWORD  (QStringList() << SETTINGS_KEY_DATABASE << "password")
#define SETTINGS_KEYLIST_DATABASE_HOST      (QStringList() << SETTINGS_KEY_DATABASE << "host")
#define SETTINGS_KEYLIST_DATABASE_DBNAME    (QStringList() << SETTINGS_KEY_DATABASE << "dbname")

class QSqlDatabase;
class DatabaseP;

class DatabaseInfo
{
public:
    QString user;
    QString secret;
    QString host;
    QString dbName;
    QString options;
    QString dbDriver;
};

class Database : public QObject
{
    Q_OBJECT

public:
    Database(const DatabaseInfo& info);
    virtual ~Database();

    static DatabaseInfo defaultDatabaseInfo();

protected:
    /** Open the database */
    bool open();

    /** Close the database */
    void close();

protected:
    /** Subclass accessor to the database object */
    QSqlDatabase database() const;

    /** Implemented by subclasses. Returns a list of essential tables whose presence is checked while opening the database. */
    virtual QStringList essentialTables() const = 0;

    /** Implented by subclasses. Called by superclass when the given essential table needs to be created. */
    virtual bool createTable(const QString& tableName) = 0;

protected:
    DatabaseP* d_ptr;
};

