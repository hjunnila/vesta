#include "nordpool.h"
#include "settings.h"
#include "pricedata.h"
#include "pricedataentry.h"
#include "nordpool_p.h"

#define SETTINGS_FILE "nordpool.json"
#define DATA_FILE "/var/tmp/nordpool_data.json"
#define STOCKHOLM "Europe/Stockholm"
#define POLL_INTERVAL (5 * 60 * 1000) // 5min
#define TIMEOUT_INTERVAL (30 * 1000) // 30s

NordPool* NordPool::createWithDefaultSettings()
{
   // Open settings for pricepoller
    Settings settings(SETTINGS_FILE);
    if (!settings.load()) {
        qWarning() << "Unable to open settings file" << SETTINGS_FILE;
        return NULL;
    }

    // Create the nordpool poller object
    QString url = settings.valueForKeyList(SETTINGS_KEYLIST_NORDPOOL_URL).toString();
    QString area = settings.valueForKeyList(SETTINGS_KEYLIST_NORDPOOL_AREA).toString();
    QTime time = settings.valueForKeyList(SETTINGS_KEYLIST_NORDPOOL_TIME).toTime();
    if (!time.isValid()) {
        time = QTime(15, 0);
    }

    NordPool* np = new NordPool(url, area, time);
    return np;
}

NordPool::NordPool(const QString& url, const QString& area, const QTime& dailyPollTime)
    : d_ptr(new NordPoolP)
{
    d_ptr->manager = new QNetworkAccessManager(this);
    d_ptr->url = url;
    d_ptr->area = area;
    d_ptr->dailyPollTime = dailyPollTime;
    d_ptr->currentReply = NULL;
    d_ptr->pollTimer = NULL;
    d_ptr->replyTimeoutTimer = NULL;
    d_ptr->stockholm = QTimeZone(QByteArray(STOCKHOLM));

    connect(d_ptr->manager, SIGNAL(finished(QNetworkReply*)), this, SLOT(onReplyFinished(QNetworkReply*)));
}

NordPool::~NordPool()
{
    qDebug() << Q_FUNC_INFO;
    stopPoll();
}

void NordPool::startPoll()
{
    qDebug() << Q_FUNC_INFO;

    Q_ASSERT(d_ptr->pollTimer == NULL);

    d_ptr->pollTimer = new QTimer(this);
    connect(d_ptr->pollTimer, SIGNAL(timeout()), this, SLOT(onTimerEvent()));
    d_ptr->pollTimer->setInterval(POLL_INTERVAL);
    d_ptr->pollTimer->start();

    qDebug() << "Prices are fetched daily after" << d_ptr->dailyPollTime.toString("hh:mm");

    // Request prices (if necessary) immediately when polling is started
    onTimerEvent();
}

void NordPool::stopPoll()
{
    qDebug() << Q_FUNC_INFO;

    if (d_ptr->pollTimer) {
        d_ptr->pollTimer->stop();
        delete d_ptr->pollTimer;
        d_ptr->pollTimer = NULL;
    }

    if (d_ptr->replyTimeoutTimer) {
        delete d_ptr->replyTimeoutTimer;
        d_ptr->replyTimeoutTimer = NULL;
    }

    if (d_ptr->currentReply) {
        d_ptr->currentReply->abort();
        d_ptr->currentReply->deleteLater();
        d_ptr->currentReply = NULL;
    }
}

bool NordPool::isPolling() const
{
    if (d_ptr->pollTimer && d_ptr->pollTimer->isActive()) {
        return true;
    } else {
        return false;
    }
}

void NordPool::onTimerEvent()
{
    if (QTime::currentTime() >= d_ptr->dailyPollTime &&
        (d_ptr->lastSuccessfulUpdate.isValid() == false || d_ptr->lastSuccessfulUpdate.daysTo(QDateTime::currentDateTime()) >= 1)) {
        fetchPrices();
    }
}

void NordPool::fetchPrices()
{
    qDebug() << Q_FUNC_INFO;

    if (d_ptr->currentReply) {
        qWarning() << "Already waiting for a reply";
        return;
    }

    d_ptr->replyTimeoutTimer = new QTimer(this);
    connect(d_ptr->replyTimeoutTimer, SIGNAL(timeout()), this, SLOT(onReplyTimeout()));
    d_ptr->replyTimeoutTimer->setInterval(TIMEOUT_INTERVAL);
    d_ptr->replyTimeoutTimer->setSingleShot(true);
    d_ptr->replyTimeoutTimer->start();
    d_ptr->currentReply = d_ptr->manager->get(QNetworkRequest(QUrl(d_ptr->url)));
}

void NordPool::onReplyTimeout()
{
    qDebug() << Q_FUNC_INFO;

    if (d_ptr->currentReply) {
        qWarning() << "Request timed out" << d_ptr->currentReply;

        // Abort calls finished() and cleans up the timeout timer & reply object
        d_ptr->currentReply->abort();
    }
}

void NordPool::onReplyFinished(QNetworkReply* reply)
{
    qDebug() << Q_FUNC_INFO;

    const QByteArray& data = reply->readAll();
    if (data.size() > 0) {
        // Parse price data
        parseByteArray(data);

        // Store the latest reply to a file
        storeDataToFile(data);
    } else {
        qWarning() << "Unable to receive price data. Another attempt will be made shortly.";
    }

    cleanupAfterNetworkActivity();
}

void NordPool::cleanupAfterNetworkActivity()
{
    qDebug() << Q_FUNC_INFO;

    d_ptr->currentReply->deleteLater();
    d_ptr->currentReply = NULL;

    if (d_ptr->replyTimeoutTimer) {
        delete d_ptr->replyTimeoutTimer;
        d_ptr->replyTimeoutTimer = NULL;
    }
}

void NordPool::storeDataToFile(const QByteArray& data)
{
    qDebug() << Q_FUNC_INFO;

    QFile file(DATA_FILE);
    file.open(QIODevice::WriteOnly);
    qint64 wrote = file.write(data);
    file.close();
    qDebug() << "wrote" << wrote << "bytes";
}

void NordPool::parseByteArray(const QByteArray& byteArray)
{
    qDebug() << Q_FUNC_INFO;

    QJsonParseError error;
    QJsonDocument doc = QJsonDocument::fromJson(byteArray, &error);
    if (error.error != QJsonParseError::NoError) {
        qWarning() << "JSON parse error:" << error.errorString();
    } else {
        if (doc.isObject()) {
            QJsonValue value = doc.object().value("data");
            if (value.isObject()) {
                parseJsonDataObject(value.toObject());
            }
        }
    }
}

QDateTime NordPool::stockholmDateToLocalDate(const QString& string) const
{
    QDateTime dt = QDateTime::fromString(string, Qt::ISODate);
    dt.setTimeZone(d_ptr->stockholm);
    return dt.toLocalTime();
}

void NordPool::parseJsonDataObject(const QJsonObject& data)
{
    qDebug() << Q_FUNC_INFO;

    QString units;
    QJsonValue value = data.value("Units");
    if (value.isArray()) {
        const QJsonArray& array = value.toArray();
        if (array.count() == 1 && array[0].isString()) {
            units = array[0].toString();
        } else {
            qWarning() << "Key (Units) is ambiguous:" << value;
        }
    } else {
        qWarning() << "Unable to find key (Units) as an array";
    }

    QDateTime dataStart;
    value = data.value("DataStartdate");
    if (value.isString()) {
        // Nordpool announces prices in stockholm time (not GMT, those bastards!)
        dataStart = stockholmDateToLocalDate(value.toString());
    } else {
        qWarning() << "Unable to find key (DataStartdate) as a string";
    }

    QDateTime dataEnd;
    value = data.value("DataEnddate");
    if (value.isString()) {
        dataEnd = stockholmDateToLocalDate(value.toString());
    } else {
        qWarning() << "Unable to find key (DataEnddate) as a string";
    }

    QDateTime dataUpdated;
    value = data.value("DateUpdated");
    if (value.isString()) {
        dataUpdated = stockholmDateToLocalDate(value.toString());
    } else {
        qWarning() << "Unable to find key (DataUpdated) as a string";
    }

    QList <PriceDataEntry> prices;
    value = data.value("Rows");
    if (value.isArray()) {
        prices = parsePriceRows(value.toArray());
    } else {
        qWarning() << "Unable to find key (Rows) as an array";
        return;
    }

    if (prices.count() > 0) {
        PriceData priceData(d_ptr->area, dataStart, dataEnd, dataUpdated, prices);
        d_ptr->lastSuccessfulUpdate = QDateTime::currentDateTime();
        emit priceDataUpdated(priceData);
    }
}

QList<PriceDataEntry> NordPool::parsePriceRows(const QJsonArray& rows)
{
    qDebug() << Q_FUNC_INFO;
    QList <PriceDataEntry> prices;

    for (QJsonArray::const_iterator it = rows.begin(); it != rows.end(); ++it) {
        if (!(*it).isObject()) {
            continue;
        }

        const QJsonObject& row = (*it).toObject();

        QJsonValue value = row.value("IsExtraRow");
        if (value.isBool()) {
            if (value.toBool() == true) {
                // Extra statistics rows are marked with IsExtraRow = true
                continue;
            }
        }

        QDateTime startTime;
        value = row.value("StartTime");
        if (value.isString()) {
            startTime = stockholmDateToLocalDate(value.toString());
        } else {
            qWarning() << "Unable to find key (StartTime) as a string";
        }

        QDateTime endTime;
        value = row.value("EndTime");
        if (value.isString()) {
            endTime = stockholmDateToLocalDate(value.toString());
        } else {
            qWarning() << "Unable to find key (EndTime) as a string";
        }

        if (startTime.time() == endTime.time()) {
            // If startTime and endTime are the same, the row is useless.
            // This might happen in case (IsExtraRow = true) for a statistics row.
            qDebug() << "Ignoring unmarked extra row";
            continue;
        }

        // Columns in this row
        value = row.value("Columns");
        if (!value.isArray()) {
            qWarning() << "Unable to find key (Column) as an array";
            continue;
        }

        const QJsonArray& columns = value.toArray();
        for (QJsonArray::const_iterator col_it = columns.begin(); col_it != columns.end(); ++col_it) {
            if (!(*col_it).isObject()) {
            qWarning() << "Column item is not an object:" << (*col_it);
                continue;
            }

            const QJsonObject& column = (*col_it).toObject();

            QJsonValue columnValue = column.value("Name");
            QString currentArea = columnValue.toString();
            if (currentArea == d_ptr->area) {
                columnValue = column.value("Value");
                double price = 0;
                if (columnValue.isDouble()) {
                    price = columnValue.toDouble();
                } else if (columnValue.isString()) {
                    price = columnValue.toString().replace(",", ".").toDouble();
                } else {
                    qWarning() << "Unable to find price for time period from:" << startTime << "to:" << endTime;
                    continue;
                }

                price = (price / 1000) * 100; // €/MWh -> c/kWh, same as (price / 10) but it's more explicit this way

                prices << PriceDataEntry(startTime, endTime, price);
            }
        }
    }

    return prices;
}
