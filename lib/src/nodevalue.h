#pragma once

namespace OpenZWave {
    class ValueID;
};

class NodeValueP;
class NodeValue : public QObject
{
    Q_OBJECT
    Q_DISABLE_COPY(NodeValue)

public:
    NodeValue(quint64 vid, quint32 home);
    virtual ~NodeValue();

    /** Comparison operator to see, if two instances represent the same value object */
    bool operator==(const NodeValue& nv) const;

    /** Get the value label */
    virtual QString label() const;

    /** Get the value help text (not available for most values) */
    QString help() const;

    /** Get the units that the value represents */
    virtual QString units() const;

    /** Check, whether the value is read-only */
    virtual bool isReadOnly() const;

    /** Check, whether the value is write-only */
    virtual bool isWriteOnly() const;

    /** Get the value genre as a string */
    QString genre() const;

    /** Get the ValueID instance that this object represents */
    virtual OpenZWave::ValueID valueId() const;

    /** Get the 64-bit value id used to construct a ValueID */
    virtual quint64 vid() const;

    /** Get the type of the value */
    virtual QString type() const;

    /** Get the value's current contents */
    virtual QVariant currentValue() const;

    /** For list-type values only: Get a list of possible values. */
    virtual QStringList listItems() const;

    /** For switch/bool-type values only: turn the value on */
    virtual void turnOn();

    /** For switch/bool-type values only: turn the value off */
    virtual void turnOff();

    /**
     * Set the current value to $value with a string. Most values can be converted to the actual type.
     *
     * @param value The value to set
     * @return true if the value was successfully sent (does not guarantee that the node received the value, though).
     */
    virtual bool setValue(const QVariant& value);

    /** */
    virtual void handleValueChanged();

private:
    NodeValueP* d_ptr;
};
