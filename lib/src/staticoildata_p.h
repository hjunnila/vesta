#pragma once

class StaticOilDataP
{
public:
    QString litrePrice;
    QString efficiency;
    QString kWhPerLitre;

    QString switchDevice;
    QString switchValue;

    QString powerDevice;
    QString powerValue;
};
