#include "pricedataentry_p.h"
#include "pricedataentry.h"

PriceDataEntry::PriceDataEntry()
    : d_ptr(new PriceDataEntryP)
{
    d_ptr->energy = 0;
    d_ptr->energyTax = 0;
    d_ptr->transfer = 0;
    d_ptr->transferTax = 0;
    d_ptr->premium = 0;
    d_ptr->premiumTax = 0;
}

PriceDataEntry::PriceDataEntry(const QDateTime& start, const QDateTime& end, double energy)
    : d_ptr(new PriceDataEntryP)
{
    d_ptr->start = start;
    d_ptr->end = end;
    d_ptr->energy = energy;
    d_ptr->energyTax = 0;
    d_ptr->transfer = 0;
    d_ptr->transferTax = 0;
    d_ptr->premium = 0;
    d_ptr->premiumTax = 0;
}

PriceDataEntry::PriceDataEntry(const PriceDataEntry& entry)
    : d_ptr(new PriceDataEntryP)
{
    d_ptr->start = entry.d_ptr->start;
    d_ptr->end = entry.d_ptr->end;
    d_ptr->energy = entry.d_ptr->energy;
    d_ptr->energyTax = entry.d_ptr->energyTax;
    d_ptr->transfer = entry.d_ptr->transfer;
    d_ptr->transferTax = entry.d_ptr->transferTax;
    d_ptr->premium = entry.d_ptr->premium;
    d_ptr->premiumTax = entry.d_ptr->premiumTax;
}

PriceDataEntry& PriceDataEntry::operator=(const PriceDataEntry& entry)
{
    if (this != &entry) {
        d_ptr->start = entry.d_ptr->start;
        d_ptr->end = entry.d_ptr->end;
        d_ptr->energy = entry.d_ptr->energy;
        d_ptr->energyTax = entry.d_ptr->energyTax;
        d_ptr->transfer = entry.d_ptr->transfer;
        d_ptr->transferTax = entry.d_ptr->transferTax;
        d_ptr->premium = entry.d_ptr->premium;
        d_ptr->premiumTax = entry.d_ptr->premiumTax;
    }

    return *this;
}

PriceDataEntry::~PriceDataEntry()
{
    delete d_ptr;
    d_ptr = NULL;
}

bool PriceDataEntry::isValid() const
{
    return (d_ptr->start.isValid() && d_ptr->end.isValid());
}

QDateTime PriceDataEntry::start() const
{
    return d_ptr->start;
}

QDateTime PriceDataEntry::end() const
{
    return d_ptr->end;
}

double PriceDataEntry::energyWithTax() const
{
    return energy() + (energy() * energyTax());
}

double PriceDataEntry::transferWithTax() const
{
    return transfer() + (transfer() * transferTax());
}

double PriceDataEntry::premiumWithTax() const
{
    return premium() + (premium() * premiumTax());
}

double PriceDataEntry::energy() const
{
    return d_ptr->energy;
}

void PriceDataEntry::setEnergy(double energy)
{
    d_ptr->energy = energy;
}

double PriceDataEntry::energyTax() const
{
    return d_ptr->energyTax;
}

void PriceDataEntry::setEnergyTax(double percent)
{
    d_ptr->energyTax = percent;
}

double PriceDataEntry::transfer() const
{
    return d_ptr->transfer;
}

void PriceDataEntry::setTransfer(double transfer)
{
    d_ptr->transfer = transfer;
}

double PriceDataEntry::transferTax() const
{
    return d_ptr->transferTax;
}

void PriceDataEntry::setTransferTax(double percent)
{
    d_ptr->transferTax = percent;
}

double PriceDataEntry::premium() const
{
    return d_ptr->premium;
}

void PriceDataEntry::setPremium(double premium)
{
    d_ptr->premium = premium;
}

double PriceDataEntry::premiumTax() const
{
    return d_ptr->premiumTax;
}

void PriceDataEntry::setPremiumTax(double premiumTax)
{
    d_ptr->premiumTax = premiumTax;
}
