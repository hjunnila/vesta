#pragma once

class ZWEventP;
class ZWEvent : public QEvent
{
public:
    /** Returns the event type registered to a ZWEvent */
    static int ZWEventType();

    /** Call exactly once per binary to register this event type with Qt */
    static void registerZWEventType();

public:
    ZWEvent(OpenZWave::Notification const* ntf);
    ~ZWEvent();

    /** Returns the OZW Notification type */
    Notification::NotificationType type() const;

    /** Returns the notification's home id */
    uint32 home() const;

    /** Returns the node that is involved in the notification */
    uint8 node() const;

    /** Returns the value ID that is involved in the notification */
    uint64 vid() const;

    /** Notification code (only valid for Notification & ControllerCommand type events) */
    uint8 code() const;

    /** Event value (only valid for ControllerCommand & NodeEvent events) */
    uint8 event() const;

    /** Returns an associated string */
    QString string() const;

private:
    ZWEventP* d_ptr;
};
