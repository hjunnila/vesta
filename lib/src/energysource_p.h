#pragma once

class EnergySourceP
{
public:
    bool currentState;
    QDateTime lastStateChange;
    int secondsOn;

    int currentPower;
    int cumulativePower;
    int numberOfSamples;

    QDateTime logStartDate;
};
