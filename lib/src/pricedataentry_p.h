#pragma once

class PriceDataEntryP
{
public:
    QDateTime start;
    QDateTime end;
    double energy;
    double energyTax;
    double transfer;
    double transferTax;
    double premium;
    double premiumTax;
};
