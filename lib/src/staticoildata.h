#pragma once

class StaticOilDataP;
class StaticOilData
{
public:
    StaticOilData();
    StaticOilData(const StaticOilData& data);
    StaticOilData(const QString& oil, const QString& efficiency, const QString& kWhPerLitre, const QString& switchDevice, const QString& switchValue, const QString& powerDevice, const QString& powerValue);
    ~StaticOilData();

    StaticOilData& operator=(const StaticOilData& data);

    void setLitrePrice(const QString& litrePrice);
    QString litrePrice() const;
    double litrePriceDouble() const;

    void setEfficiency(const QString& efficiency);
    QString efficiency() const;
    double efficiencyDouble() const;

    void setKWhPerLitre(const QString& kWhPerLitre);
    QString kWhPerLitre() const;
    double kWhPerLitreDouble() const;

    void setSwitchDevice(const QString& switchDevice);
    QString switchDevice() const;

    void setSwitchValue(const QString& switchValue);
    QString switchValue() const;

    void setPowerValue(const QString& powerValue);
    QString powerValue() const;

    void setPowerDevice(const QString& powerDevice);
    QString powerDevice() const;

private:
    StaticOilDataP* d_ptr;
};
