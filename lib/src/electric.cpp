#include "staticelectricdata.h"
#include "staticelectricdb.h"
#include "electric_p.h"
#include "pricedata.h"
#include "elpricedb.h"
#include "electric.h"
#include "macros.h"

Electric::Electric(const DatabaseInfo& info)
    : EnergySource()
    , d_ptr(new ElectricP)
{
    d_ptr->dynamicDB = new ElPriceDB(info);
    d_ptr->staticDB = new StaticElectricDB(info);
}

Electric::~Electric()
{
    delete d_ptr->dynamicDB;
    delete d_ptr->staticDB;

    delete d_ptr;
    d_ptr = NULL;
}

QString Electric::switchDevice() const
{
    StaticElectricData data(d_ptr->staticDB->staticElectricData());
    return data.switchDevice();
}

QString Electric::switchValue() const
{
    StaticElectricData data(d_ptr->staticDB->staticElectricData());
    return data.switchValue();
}

QString Electric::powerDevice() const
{
    StaticElectricData data(d_ptr->staticDB->staticElectricData());
    return data.powerDevice();
}

QString Electric::powerValue() const
{
    StaticElectricData data(d_ptr->staticDB->staticElectricData());
    return data.powerValue();
}

PriceData Electric::priceDataAfterIncluding(const QDateTime& dateTime) const
{
    StaticElectricData staticData(d_ptr->staticDB->staticElectricData());

    PriceData priceData(d_ptr->dynamicDB->priceDataAfterIncluding(staticData.area(), dateTime));

    priceData.applyCosts(staticData.energyTaxDouble(),
                         staticData.transferDouble(), staticData.transferTaxDouble(),
                         staticData.premiumDouble(), staticData.premiumTaxDouble());
    return priceData;
}

void Electric::setStaticElectricData(const StaticElectricData& data)
{
    d_ptr->staticDB->setStaticElectricData(data);
}

StaticElectricData Electric::staticElectricData()
{
    return d_ptr->staticDB->staticElectricData();
}

/*
double Electric::pricePerKWh() const
{
    qDebug() << Q_FUNC_INFO;

    // Nordpool announces prices in stockholm time!!!

    double eurosPerMWh = 30.99; // € per MWh from nordpool
    double centsPerkWh = (eurosPerMWh / 1000) * 100; // 1000 kWh per 1 MWh, into cents

    double price = centsPerkWh + (centsPerkWh * m_energyTax);
    price += m_transferPrice + (m_transferPrice * m_transferTax);
    price += m_premiumPrice + (m_premiumPrice * m_premiumTax);

    return price;
}
*/
