#pragma once

#include "response.h"

class Request;
class Server;
class ServiceP;
class Service : public QObject
{
    Q_OBJECT
    Q_DISABLE_COPY(Service)

public:
    Service(QObject* parent = 0);
    virtual ~Service();

    /**
     * Called by Server to ask the service to handle the request. If the service is
     * able to handle the request, true is returned and $response will contain a response
     * that can be sent to the peer. If the service is not able to handle the request,
     * false will be returned.
     *
     * @param request A received request that needs to be served
     * @param response A response filled by the service (if possible)
     * @return true if request was handled, otherwise false
     */
    bool handleRequest(const Request& request, Response& response);

protected:
    /**
     * Subclasses call this to register various $member callbacks to handle HTTP requests
     * with the given $method to a $path.
     *
     * @param method The HTTP method to listen to (GET, POST, DELETE...)
     * @param path A regular expression describing the service path
     * @param member Name of the member method to handle the service call
     */
    void addService(const QString& method, const QRegExp& path, const QString& member);

private:
    ServiceP* d_ptr;
};
