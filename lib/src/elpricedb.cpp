#include "pricedataentry.h"
#include "elpricedb_p.h"
#include "pricedata.h"
#include "elpricedb.h"
#include "database.h"

ElPriceDB::ElPriceDB(const DatabaseInfo& info)
    : Database(info)
    , d_ptr(new ElPriceDBP)
{
    d_ptr->parent = this;
}

ElPriceDB::~ElPriceDB()
{
    d_ptr->parent = NULL;

    delete d_ptr;
    d_ptr = NULL;
}

QStringList ElPriceDB::essentialTables() const
{
    return QStringList() << kTableElPrice;
}

bool ElPriceDB::createTable(const QString& tableName)
{
    if (tableName == kTableElPrice) {
        return createElectricPriceTable();
    } else {
        return false;
    }
}

bool ElPriceDB::createElectricPriceTable()
{
    QSqlQuery query(database());
    if (!query.prepare("CREATE TABLE " kTableElPrice "("
                       kFieldElPriceArea " TEXT NOT NULL,"
                       kFieldElPriceStart " DATETIME NOT NULL,"
                       kFieldElPriceEnd " DATETIME NOT NULL,"
                       kFieldElPriceEnergy " DOUBLE NOT NULL);")) {
        qWarning() << "Unable to prepare query for " kTableElPrice " table creation:" << query.lastError();
        return false;
    }

    if (!query.exec()) {
        qWarning() << "Unable to create table:" << kTableElPrice << query.lastError();
        return false;
    }

    return true;
}

PriceDataEntry ElPriceDB::priceDataEntry(const QString& area, const QDateTime& dateTime)
{
    QString where = QString("WHERE (%1 <= '%2' AND %3 >= '%2' AND %4 = '%5')")
                            .arg(kFieldElPriceStart)
                            .arg(dateTime.toString(Qt::ISODate))
                            .arg(kFieldElPriceEnd)
                            .arg(kFieldElPriceArea)
                            .arg(area);
    QString select = QString("SELECT * FROM %1 %2;").arg(kTableElPrice).arg(where);

    if (!open()) {
        qWarning() << Q_FUNC_INFO << "Unable to open database." << database().lastError();
        close();
        return PriceDataEntry();
    }

    QSqlQuery query(database());
    if (!query.exec(select)) {
        qWarning() << Q_FUNC_INFO << "Unable to execute query:" << query.lastError();
    } else {
        QSqlRecord record;
        if (query.isValid() || query.next()) {
            record = query.record();
            close();
            return d_ptr->priceDataEntryFromRecord(record);
        }
    }

    close();
    return PriceDataEntry();
}

PriceData ElPriceDB::priceDataAfterIncluding(const QString& area, const QDateTime& dateTime)
{
    QString where = QString("WHERE (%1 >= '%2' AND %3 = '%4')")
                            .arg(kFieldElPriceStart)
                            .arg(dateTime.toString(Qt::ISODate))
                            .arg(kFieldElPriceArea)
                            .arg(area);
    QString select = QString("SELECT * FROM %1 %2;").arg(kTableElPrice).arg(where);

    // Open the database
    if (!open()) {
        qWarning() << Q_FUNC_INFO << "Unable to open database." << database().lastError();

        close();
        return PriceData(area, database().lastError());
    }

    PriceData priceData;

    QSqlQuery query(database());
    if (!query.exec(select)) {
        qWarning() << Q_FUNC_INFO << "Unable to execute query:" << query.lastError();
        priceData = PriceData(area, query.lastError());
    } else {
        QList <PriceDataEntry> entries;
        QDateTime end(dateTime);

        while (query.next()) {
            PriceDataEntry entry = d_ptr->priceDataEntryFromRecord(query.record());
            if (entry.end() >= end) {
                // Pick the end time that's farthest in the future
                end = entry.end();
            }

            entries << entry;
        }

        priceData = PriceData(area, dateTime, end, QDateTime::currentDateTime(), entries);
    }

    close();
    return priceData;
}

void ElPriceDB::onPriceDataUpdated(const PriceData& priceData)
{
    // Open the database
    if (!open()) {
        qWarning() << Q_FUNC_INFO << "Unable to open database." << database().lastError();
        return;
    }

    QList <PriceDataEntry> entries(priceData.prices());
    for (QList<PriceDataEntry>::const_iterator it = entries.begin(); it != entries.end(); ++it) {
        const PriceDataEntry& entry(*it);
        if (d_ptr->isEntryAlreadyStored(priceData.area(), entry) == false) {
            d_ptr->insertPriceDataEntry(priceData.area(), entry);
        } else {
            qDebug() << "Price data already stored from" << entry.start() << "to" << entry.end();
        }
    }

    // Emit the signal anyways, in case the energy manager hasn't received previous price updates
    emit newPriceDataAvailable(priceData.start().toString(Qt::ISODate), priceData.end().toString(Qt::ISODate));
}

//
// Private implementation
//

PriceDataEntry ElPriceDBP::priceDataEntryFromRecord(const QSqlRecord& record) const
{
    return PriceDataEntry(record.value(kFieldElPriceStart).toDateTime(),
                          record.value(kFieldElPriceEnd).toDateTime(),
                          record.value(kFieldElPriceEnergy).toFloat());
}

bool ElPriceDBP::isEntryAlreadyStored(const QString& area, const PriceDataEntry& entry) const
{
    QString where = QString("WHERE (%1 = '%2' AND %3 = '%4' AND %5 = '%6')")
                            .arg(kFieldElPriceStart)
                            .arg(entry.start().toString(Qt::ISODate))
                            .arg(kFieldElPriceEnd)
                            .arg(entry.end().toString(Qt::ISODate))
                            .arg(kFieldElPriceArea)
                            .arg(area);
    QString select = QString("SELECT * FROM %1 %2;").arg(kTableElPrice).arg(where);

    QSqlQuery query(parent->database());
    if (!query.exec(select)) {
        qWarning() << "Unable to execute query:" << query.lastError();
        return false;
    }

    if (query.isValid()) {
        return true;
    } else {
        return query.next();
    }
}

bool ElPriceDBP::insertPriceDataEntry(const QString& area, const PriceDataEntry& entry)
{
    QString values = QString("'%1', '%2', '%3', '%4'")
                            .arg(area)
                            .arg(entry.start().toString(Qt::ISODate))
                            .arg(entry.end().toString(Qt::ISODate))
                            .arg(entry.energy());
    QString insert = QString("INSERT INTO %1 VALUES (%2)").arg(kTableElPrice).arg(values);

    QSqlQuery query(parent->database());
    if (!query.exec(insert)) {
        qWarning() << "Unable to insert price data entry:" << query.lastError();
        return false;
    }

    return true;
}
