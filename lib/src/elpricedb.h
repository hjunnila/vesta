#pragma once
#include "database.h"

#define kTableElPrice            "electric_price"
#define kFieldElPriceArea        "area"
#define kFieldElPriceStart       "start"
#define kFieldElPriceEnd         "end"
#define kFieldElPriceEnergy      "energy"

class PriceDataEntry;
class QSqlRecord;
class PriceData;

class ElPriceDBP;
class ElPriceDB : public Database
{
    Q_OBJECT

    friend class ElPriceDBP;

public:
    ElPriceDB(const DatabaseInfo& info = Database::defaultDatabaseInfo());
    ~ElPriceDB();

    /** Get the price data entry for the given dateTime in the given area. */
    PriceDataEntry priceDataEntry(const QString& area, const QDateTime& dateTime);

    /** Get price data from the given area, after (and including) the given dateTime, up to a year in advance. */
    PriceData priceDataAfterIncluding(const QString& area, const QDateTime& dateTime);

signals:
    /** Emitted if new price data was actually stored to the database. $start and $end are ISODate formatted strings that
     * are easily passed thru DBus; they tell the start and end times of the updated dataset. */
    void newPriceDataAvailable(const QString& start, const QString& end);

public slots:
    /** Store the given set of price data to the database */
    void onPriceDataUpdated(const PriceData& priceData);

protected:
    QStringList essentialTables() const;
    bool createTable(const QString& tableName);
    bool createElectricPriceTable();

private:
    ElPriceDBP* d_ptr;
};
