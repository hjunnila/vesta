#pragma once

#define RESPONSE_ERROR "error"
#define RESPONSE_ERROR_DESCRIPTION "error_description"

class ResponseP;
class Response
{
public:
    /** Default constructor */
    Response();

    /** Copy constructor */
    Response(const Response& response);

    /** Destructor */
    ~Response();

    /** Copy operator */
    Response& operator=(const Response& response);

    /** Set the HTTP status code */
    void setStatus(int status);

    /** Get the HTTP status code */
    int status() const;

    /** Set the response body and update Content-Length header accordingly */
    void setBody(const QString& body);

    /** Return the response body */
    QString body() const;

    /** Set the body as a JSON document */
    void setJsonBody(const QJsonDocument& doc);

    /** Set the response header key to contain the given value */
    void setHeader(const QString& key, const QString& value);

    /** Return response headers */
    QMap<QString,QString> headers() const;

    /** Construct a generic "404 Not Found" response */
    static Response notFound();

    /** Construct a generic "400 Bad Request" response */
    static Response badRequest();

    /** Get the response as a byte array that can be sent to the client */
    QByteArray responseData() const;

    /** Set the response status along with an optional reason for the error. */
    void setError(int status, const QString& error = QString(), const QString& errorDescription = QString());

private:
    ResponseP* d_ptr;
};
