#pragma once

class NodeValueP : public QObject
{
    Q_OBJECT
    Q_DISABLE_COPY(NodeValueP)

public:
    NodeValueP(QObject* parent = 0);
    virtual ~NodeValueP();

    ValueID valueId() const;

    void startPendingTimer(int times);
    void stopPendingTimer();
    
public slots:
    void onPendingTimer();

public:
    uint64 vid;
    uint32 home;
    
    int pendingUpdate;
    QTimer* pendingTimer;
};
