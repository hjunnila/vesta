#include "devicenode.h"
#include "nodevalue.h"
#include "devicenode_p.h"

DeviceNode::DeviceNode(uint32 home, uint8 node, QObject* parent)
    : QObject(parent)
    , d_ptr(new DeviceNodeP(this))
{
    connect(d_ptr, SIGNAL(valueChanged(DeviceNode*, NodeValue*)), this, SIGNAL(valueChanged(DeviceNode*, NodeValue*)));

    d_ptr->home = home;
    d_ptr->node = node;
}

DeviceNode::~DeviceNode()
{
    delete d_ptr;
    d_ptr = NULL;
}

bool DeviceNode::operator==(const DeviceNode& node) const
{
    if (d_ptr->home == node.d_ptr->home && d_ptr->node == node.d_ptr->node) {
        return true;
    }

    return false;
}

uint32 DeviceNode::home() const
{
    return d_ptr->home;
}

uint8 DeviceNode::node() const
{
    return d_ptr->node;
}

QString DeviceNode::manufacturer() const
{
    return QString::fromStdString(Manager::Get()->GetNodeManufacturerName(d_ptr->home, d_ptr->node));
}

QString DeviceNode::productName() const
{
    return QString::fromStdString(Manager::Get()->GetNodeProductName(d_ptr->home, d_ptr->node));
}

QString DeviceNode::name() const
{
    return QString::fromStdString(Manager::Get()->GetNodeName(d_ptr->home, d_ptr->node));
}

void DeviceNode::setName(const QString& name)
{
    Manager::Get()->SetNodeName(d_ptr->home, d_ptr->node, name.toStdString());
}

bool DeviceNode::isAwake() const
{
    return Manager::Get()->IsNodeAwake(d_ptr->home, d_ptr->node);
}

bool DeviceNode::refreshInfo() const
{
    return Manager::Get()->RefreshNodeInfo(d_ptr->home, d_ptr->node);
}

bool DeviceNode::requestState() const
{
    return Manager::Get()->RequestNodeState(d_ptr->home, d_ptr->node);
}

bool DeviceNode::requestDynamicState() const
{
    return Manager::Get()->RequestNodeDynamic(d_ptr->home, d_ptr->node);
}

void DeviceNode::heal() const
{
    Manager::Get()->HealNetworkNode(d_ptr->home, d_ptr->node, true);
}

QList <NodeValue*> DeviceNode::values() const
{
    return d_ptr->values;
}

NodeValue* DeviceNode::value(const QString& label) const
{
    for (QList <NodeValue*>::const_iterator it = d_ptr->values.begin(); it != d_ptr->values.end(); ++it) {
        NodeValue* nv = (*it);
        if (label.length() > 0 && nv->label() == label) {
            return nv;
        }
    }

    return NULL;
}

NodeValue* DeviceNode::value(uint64 vid) const
{
    return d_ptr->value(vid);
}

//
// Private implementation
//

DeviceNodeP::DeviceNodeP(QObject* parent)
    : QObject(parent)
{
}

DeviceNodeP::~DeviceNodeP()
{
    while (!values.isEmpty()) {
        delete values.takeLast();
    }
}

NodeValue* DeviceNodeP::value(uint64 vid) const
{
    for (QList <NodeValue*>::const_iterator it = values.begin(); it != values.end(); ++it) {
        NodeValue* nv = (*it);
        if (nv->vid() == vid) {
            return nv;
        }
    }

    return NULL;
}

void DeviceNodeP::addValue(uint64 vid)
{
    if (!value(vid)) {
        NodeValue* nv = new NodeValue(vid, home);
        values.append(nv);
    }
}

void DeviceNodeP::removeValue(uint64 vid)
{
    for (int i = 0; i < values.count(); i++) {
        NodeValue* nv = values.at(i);
        if (nv->vid() == vid) {
            values.removeAt(i);
            break;
        }
    }
}

void DeviceNodeP::handleValueChanged(uint64 vid)
{
    for (QList <NodeValue*>::const_iterator it = values.begin(); it != values.end(); ++it) {
        NodeValue* nv(*it);
        if (nv->vid() == vid) {
            nv->handleValueChanged();
            emit valueChanged(qobject_cast<DeviceNode*>(parent()), nv);
            break;
        }
    }
}
