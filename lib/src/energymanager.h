#pragma once

#include "energysource.h"

class PriceData;
class DeviceNode;
class DeviceManager;
class PriceDataEntry;
class EnergyPlanEntry;
class EnergySource;
class Electric;
class Oil;

class EnergyManagerP;
class EnergyManager : public QObject
{
    Q_OBJECT

public:
    EnergyManager();
    ~EnergyManager();

public slots:
    /** Start energy management */
    void start();

    /** Stop energy management */
    void stop();

    /** Called from DBus telling that new price data is available in electric price DB */
    void onNewPriceDataAvailable(const QString& start, const QString& end);

    /** Called when a monitored device's value changes */
    void onDeviceNodeValueChanged(const QString& deviceName, const QString& valueName, const QVariant& newValue);

public:
    /** Check, if energy manager is running */
    bool running() const;

    /** Create new energy plans by fetching all available electric price data after $startDateTime. */
    void createNewEnergyPlans(const QDateTime& startDateTime);

    /** Get all existing energy plan entries for the given date. */
    QList <EnergyPlanEntry> energyPlanEntriesForDate(const QDate& date);

    /** Get the electric object */
    Electric* electric() const;

    /** Get the Oil object */
    Oil* oil() const;

    /** By setting $on==true electric will be enabled regardless of whether it is cheaper than oil. */
    void setManualElectricOverride(bool on);

    /** Check, whether manual use of electricity is enabled or not. */
    bool isManualElectricOverride() const;

signals:
    /** Emitted when the EnergySource changes priority based on its price compared to other energy source(s) */
    void energySourcePriorityChanged(EnergySource* energySource, EnergySourcePriority priority);

private:
    EnergyManagerP* d_ptr;
};
