#pragma once
#include "database.h"

#define kTableEnergyUsage                   "energy_usage"
#define kFieldEnergyUsageStart              "start"
#define kFieldEnergyUsageEnd                "end"
#define kFieldEnergyUsageElSeconds          "el_seconds"
#define kFieldEnergyUsageElAveragePower     "el_average_power"
#define kFieldEnergyUsageOilSeconds         "oil_seconds"
#define kFieldEnergyUsageOilAveragePower    "oil_average_power"
#define kFieldEnergyUsageError              "error"

class EnergyUsageEntry;
class QSqlRecord;

class EnergyUsageDBP;
class EnergyUsageDB : public Database
{
    Q_OBJECT

    friend class EnergyUsageDBP;

public:
    EnergyUsageDB(const DatabaseInfo& info = Database::defaultDatabaseInfo());
    ~EnergyUsageDB();

    /** Get the price data entry for the given dateTime in the given area. */
    EnergyUsageEntry energyUsageEntry(const QDateTime& dateTime);

    /** Store the given entry to the energy usage database. */
    bool storeEntry(const EnergyUsageEntry& entry);

protected:
    QStringList essentialTables() const;
    bool createTable(const QString& tableName);
    bool createEnergyUsageTable();

private:
    EnergyUsageDBP* d_ptr;
};

