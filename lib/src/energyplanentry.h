#pragma once

class EnergyPlanEntryP;
class EnergyPlanEntry
{
public:
    enum PlanType {
        FailSafePlan,   // < Failsafe plan which enables electric use in case of errors
        PriceBasedPlan, // < Normal plan which allows electric use if it is cheaper than oil
        ManualElectricPlan  // < Basically same as FailSafePlan but is enabled by user
    };

public:
    EnergyPlanEntry();
    EnergyPlanEntry(const EnergyPlanEntry& entry);
    EnergyPlanEntry(const EnergyPlanEntry& entry, PlanType planType);
    EnergyPlanEntry(const QDateTime& start, const QDateTime& end, double oil, double energy, double transfer, double premium, PlanType planType = PriceBasedPlan);
    ~EnergyPlanEntry();

    /** Assignment operator */
    EnergyPlanEntry& operator=(const EnergyPlanEntry& entry);

    /** Equal operator */
    bool operator==(const EnergyPlanEntry& entry) const;

    /** Not equal operator */
    bool operator!=(const EnergyPlanEntry& entry) const;

    /** Returns true if this plan entry is valid (i.e. contains enough information to be used for energy planning) */
    bool isValid() const;

    /** Get the current plan type */
    PlanType planType() const;

    /** Start time of this period */
    QDateTime start() const;

    /** End time of this period */
    QDateTime end() const;

    /** Oil price per kWh during this period (incl. tax) */
    double oil() const;

    /** Electric energy price per kWh during this period (incl. tax) */
    double energy() const;

    /** Electric transfer price per kWh during this period (incl. tax) */
    double transfer() const;

    /** Electric premium price per kWh during this period (incl. tax) */
    double premium() const;

    /** Essentially same as oil() */
    double oilPerKWh() const;

    /** Adds all electric prices (energy, transfer, premium) together and returns the result */
    double electricPerKWh() const;

    static QDateTime currentHourStart();
    static QDateTime currentHourEnd();

private:
    EnergyPlanEntryP* d_ptr;
};
