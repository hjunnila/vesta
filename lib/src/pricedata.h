#pragma once
#include "pricedataentry.h"

class PriceDataP;
class PriceData
{
public:
    /** Default constructor */
    PriceData();

    /** Construct new price data with the given data */
    PriceData(const QString& area, const QDateTime& start, const QDateTime& end, const QDateTime& updated,
              const QList <PriceDataEntry> prices);

    /** Constructor for when there is an error acquiring price data entries from the DB. */
    PriceData(const QString& area, const QSqlError& error);

    /** Copy constructor */
    PriceData(const PriceData& priceData);

    /** Assignment operator */
    PriceData& operator=(const PriceData& priceData);

    /** Destroy the dataset. */
    ~PriceData();

    /** The area whose data this dataset contains */
    QString area() const;

    /** Start time of dataset */
    QDateTime start() const;

    /** End time of dataset */
    QDateTime end() const;

    /** Date when dataset has been updated to service */
    QDateTime updated() const;

    /** Returns an error (see .isValid to check if it's actually set) */
    QSqlError::ErrorType error() const;

    /** List of available price entries for the dataset time period */
    QList <PriceDataEntry> prices() const;

    /**
     * Apply additional costs to each price entry.
     *
     * @param energyTax Tells the tax percentage applied to energy per kWh
     * @param transfer Tells the price of energy transfer per kWh
     * @param transferTax Tells the tax percentage applied to transfer per kWh
     * @param premium Tells the amount of premium (extra costs) per kWh
     * @param premiumTax Tells the tax percentage applied to premium per kWh
     */
    void applyCosts(double energyTax, double transfer, double transferTax, double premium, double premiumTax);

private:
    PriceDataP* d_ptr;
};
