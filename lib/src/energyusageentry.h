#pragma once

class EnergyUsageEntryP;
class EnergyUsageEntry
{
public:
    EnergyUsageEntry();
    EnergyUsageEntry(const QDateTime& start, const QDateTime& end, int elSeconds, int elAveragePower, int oilSeconds, int oilAveragePower, const QString& error = QString());
    EnergyUsageEntry(const EnergyUsageEntry& entry);
    virtual ~EnergyUsageEntry();

    EnergyUsageEntry& operator=(const EnergyUsageEntry& entry);

    bool isValid() const;

    QDateTime start() const;
    QDateTime end() const;
    int elSeconds() const;
    int elAveragePower() const;
    int oilSeconds() const;
    int oilAveragePower() const;
    QString error() const;

private:
    EnergyUsageEntryP* d_ptr;
};
