#pragma once
#include "database.h"

#define kTableEnergyPlan            "energy_plan"
#define kFieldEnergyPlanStart       "start"
#define kFieldEnergyPlanEnd         "end"
#define kFieldEnergyPlanOil         "oil"
#define kFieldEnergyPlanEnergy      "energy"
#define kFieldEnergyPlanTransfer    "transfer"
#define kFieldEnergyPlanPremium     "premium"

class EnergyPlanEntry;
class EnergyPlanDBP;

class EnergyPlanDB : public Database
{
    Q_OBJECT

    friend class EnergyPlanDBP;

public:
    EnergyPlanDB(const DatabaseInfo& info = Database::defaultDatabaseInfo());
    ~EnergyPlanDB();

    /** Return all existing energy plans for the given date. */
    QList <EnergyPlanEntry> energyPlanEntriesForDate(const QDate& date);

    /** Return an energy plan entry for the given date. If one cannot be found, an invalid entry is returned. */
    EnergyPlanEntry energyPlanEntryForDate(const QDateTime& date);

    /** Store a list of energy plan entries to the database. */
    bool storeEnergyPlanEntries(const QList <EnergyPlanEntry>& planEntries);

protected:
    QStringList essentialTables() const;
    bool createTable(const QString& tableName);
    bool createEnergyPlanTable();

private:
    EnergyPlanDBP* d_ptr;
};
