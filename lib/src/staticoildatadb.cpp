#include "staticoildata.h"
#include "staticoildatadb_p.h"
#include "staticoildatadb.h"
#include "database.h"

StaticOilDataDB::StaticOilDataDB(const DatabaseInfo& info)
    : Database(info)
    , d_ptr(new StaticOilDataDBP)
{
    d_ptr->parent = this;
}

StaticOilDataDB::~StaticOilDataDB()
{
    d_ptr->parent = NULL;

    delete d_ptr;
    d_ptr = NULL;
}

QStringList StaticOilDataDB::essentialTables() const
{
    return QStringList() << kTableStaticOilData;
}

bool StaticOilDataDB::createTable(const QString& tableName)
{
    if (tableName == kTableStaticOilData) {
        return d_ptr->createStaticOilDataTable();
    } else {
        return false;
    }
}

bool StaticOilDataDB::setStaticOilData(const StaticOilData& entry)
{
    qDebug() << Q_FUNC_INFO;

    if (!open()) {
        qWarning() << Q_FUNC_INFO << "Unable to open database." << database().lastError();
        close();
        return false;
    }

    QSqlQuery query = d_ptr->staticOilDataQuery();
    if (query.isValid()) {
        qDebug() << "Replacing existing static oil data";
        QString values = QString("%1 = '%2', %3 = '%4', %5 = '%6', %7 = '%8', %9 = '%10', %11 = '%12', %13 = '%14'")
                                                    .arg(kFieldStaticOilDataLitrePrice)
                                                    .arg(entry.litrePrice())
                                                    .arg(kFieldStaticOilDataEfficiency)
                                                    .arg(entry.efficiency())
                                                    .arg(kFieldStaticOilDataKWhPerLitre)
                                                    .arg(entry.kWhPerLitre())
                                                    .arg(kFieldStaticOilDataSwitchDevice)
                                                    .arg(entry.switchDevice())
                                                    .arg(kFieldStaticOilDataSwitchValue)
                                                    .arg(entry.switchValue())
                                                    .arg(kFieldStaticOilDataPowerDevice)
                                                    .arg(entry.powerDevice())
                                                    .arg(kFieldStaticOilDataPowerValue)
                                                    .arg(entry.powerValue());
        QString insert = QString("UPDATE %1 SET %2").arg(kTableStaticOilData).arg(values);
        QSqlQuery query(database());
        if (!query.exec(insert)) {
            qWarning() << "Unable to update static oil data entry:" << query.lastError();
            close();
            return false;
        }
    } else {
        qDebug() << "Inserting new static oil Data entry";
        QString values = QString("'%1', '%2', '%3', '%4', '%5', '%6', '%7'")
                                                    .arg(entry.litrePrice())
                                                    .arg(entry.efficiency())
                                                    .arg(entry.kWhPerLitre())
                                                    .arg(entry.switchDevice())
                                                    .arg(entry.switchValue())
                                                    .arg(entry.powerDevice())
                                                    .arg(entry.powerValue());
        QString insert = QString("INSERT INTO %1 VALUES (%2)").arg(kTableStaticOilData).arg(values);
        QSqlQuery query(database());
        if (!query.exec(insert)) {
            qWarning() << "Unable to insert static oil data entry:" << query.lastError();
            close();
            return false;
        }
    }

    close();
    return true;
}

StaticOilData StaticOilDataDB::staticOilData()
{
    if (!open()) {
        qWarning() << Q_FUNC_INFO << "Unable to open database." << database().lastError();
        close();
        return StaticOilData();
    }

    QSqlQuery query = d_ptr->staticOilDataQuery();
    if (query.isValid()) {
        StaticOilData entry(d_ptr->staticOilDataFromRecord(query.record()));
        close();
        return entry;
    } else {
        close();
        return StaticOilData();
    }
}

//
// Private implementation
//

QSqlQuery StaticOilDataDBP::staticOilDataQuery()
{
    QString select = QString("SELECT * FROM %1;").arg(kTableStaticOilData);

    QSqlQuery query(parent->database());
    if (!query.exec(select)) {
        qWarning() << "Unable to execute query:" << query.lastError();
    }

    if (!query.isValid()) {
        query.next();
    }

    return query;
}

StaticOilData StaticOilDataDBP::staticOilDataFromRecord(const QSqlRecord& record) const
{
    return StaticOilData(record.value(kFieldStaticOilDataLitrePrice).toString(),
                               record.value(kFieldStaticOilDataEfficiency).toString(),
                               record.value(kFieldStaticOilDataKWhPerLitre).toString(),
                               record.value(kFieldStaticOilDataSwitchDevice).toString(),
                               record.value(kFieldStaticOilDataSwitchValue).toString(),
                               record.value(kFieldStaticOilDataPowerDevice).toString(),
                               record.value(kFieldStaticOilDataPowerValue).toString());
}

bool StaticOilDataDBP::createStaticOilDataTable()
{
    QSqlQuery query(parent->database());
    if (!query.prepare("CREATE TABLE " kTableStaticOilData "("
                       kFieldStaticOilDataLitrePrice " TEXT NOT NULL, "
                       kFieldStaticOilDataEfficiency " TEXT NOT NULL, "
                       kFieldStaticOilDataKWhPerLitre " TEXT NOT NULL, "

                       kFieldStaticOilDataSwitchDevice " TEXT NOT NULL, "
                       kFieldStaticOilDataSwitchValue " TEXT NOT NULL, "
                       kFieldStaticOilDataPowerDevice " TEXT NOT NULL,"
                       kFieldStaticOilDataPowerValue " TEXT NOT NULL);")) {
        qWarning() << "Unable to prepare query for " kTableStaticOilData " table creation:" << query.lastError();
        return false;
    }

    if (!query.exec()) {
        qWarning() << "Unable to create table:" << kTableStaticOilData << query.lastError();
        return false;
    }

    // Insert some default values to begin with
    StaticOilData e("85", "0.9", "10", "Oil", "Switch", "Oil", "Power");
    return parent->setStaticOilData(e);
}
