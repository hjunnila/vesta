#pragma once

class StaticElectricData;
class StaticElectricDB;
class StaticElectricDBP
{
public:
    // Properties
    StaticElectricDB* parent;

public:
    // Methods
    QSqlQuery staticElectricDataQuery();
    StaticElectricData staticElectricDataFromRecord(const QSqlRecord& record) const;
    bool createStaticElectricDataTable();
};
