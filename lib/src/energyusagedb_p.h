#pragma once

class EnergyUsageDB;
class EnergyUsageDBP
{
public:
    // Properties
    EnergyUsageDB* parent;

public:
    // Methods
    EnergyUsageEntry entryFromRecord(const QSqlRecord& record) const;
    bool insertEntry(const EnergyUsageEntry& entry);
};


