#include "staticelectricdata.h"
#include "staticelectricdb_p.h"
#include "staticelectricdb.h"
#include "database.h"

StaticElectricDB::StaticElectricDB(const DatabaseInfo& info)
    : Database(info)
    , d_ptr(new StaticElectricDBP)
{
    d_ptr->parent = this;
}

StaticElectricDB::~StaticElectricDB()
{
    d_ptr->parent = NULL;

    delete d_ptr;
    d_ptr = NULL;
}

QStringList StaticElectricDB::essentialTables() const
{
    return QStringList() << kTableStaticElectricData;
}

bool StaticElectricDB::createTable(const QString& tableName)
{
    if (tableName == kTableStaticElectricData) {
        return d_ptr->createStaticElectricDataTable();
    } else {
        return false;
    }
}

bool StaticElectricDB::setStaticElectricData(const StaticElectricData& data)
{
    if (!open()) {
        qWarning() << Q_FUNC_INFO << "Unable to open database." << database().lastError();
        close();
        return false;
    }

    QSqlQuery query = d_ptr->staticElectricDataQuery();
    if (query.isValid()) {
        qDebug() << "Replacing existing static prices";
        QString values = QString("%1 = '%2', %3 = '%4', %5 = '%6', %7 = '%8', %9 = '%10', %11 = '%12', %13 = '%14', %15 = '%16', %17 = '%18', %19 = '%20'")
                                .arg(kFieldStaticElectricDataEnergyTax)
                                .arg(data.energyTax())
                                .arg(kFieldStaticElectricDataTransfer)
                                .arg(data.transfer())
                                .arg(kFieldStaticElectricDataTransferTax)
                                .arg(data.transferTax())
                                .arg(kFieldStaticElectricDataPremium)
                                .arg(data.premium())
                                .arg(kFieldStaticElectricDataPremiumTax)
                                .arg(data.premiumTax())
                                .arg(kFieldStaticElectricDataSwitchDevice)
                                .arg(data.switchDevice())
                                .arg(kFieldStaticElectricDataSwitchValue)
                                .arg(data.switchValue())
                                .arg(kFieldStaticElectricDataPowerDevice)
                                .arg(data.powerDevice())
                                .arg(kFieldStaticElectricDataPowerValue)
                                .arg(data.powerValue())
                                .arg(kFieldStaticElectricDataArea)
                                .arg(data.area());
        QString insert = QString("UPDATE %1 SET %2").arg(kTableStaticElectricData).arg(values);
        QSqlQuery query(database());
        if (!query.exec(insert)) {
            qWarning() << Q_FUNC_INFO << "Unable to update static electric data:" << query.lastError();
            close();
            return false;
        }

        // TODO: WTF are these here for???
        query.record().setValue(kFieldStaticElectricDataEnergyTax, data.energyTax());
        query.record().setValue(kFieldStaticElectricDataTransfer, data.transfer());
        query.record().setValue(kFieldStaticElectricDataTransferTax, data.transferTax());
        query.record().setValue(kFieldStaticElectricDataPremium, data.premium());
        query.record().setValue(kFieldStaticElectricDataPremiumTax, data.premiumTax());
        query.record().setValue(kFieldStaticElectricDataSwitchDevice, data.switchDevice());
        query.record().setValue(kFieldStaticElectricDataSwitchValue, data.switchValue());
        query.record().setValue(kFieldStaticElectricDataPowerDevice, data.powerDevice());
        query.record().setValue(kFieldStaticElectricDataPowerValue, data.powerValue());
        query.record().setValue(kFieldStaticElectricDataArea, data.area());
    } else {
        qDebug() << "Inserting new static price entry";
        QString values = QString("'%1', '%2', '%3', '%4', '%5', '%6', '%7', '%8', '%9', '%10'")
                                .arg(data.energyTax())
                                .arg(data.transfer())
                                .arg(data.transferTax())
                                .arg(data.premium())
                                .arg(data.premiumTax())
                                .arg(data.switchDevice())
                                .arg(data.switchValue())
                                .arg(data.powerDevice())
                                .arg(data.powerValue())
                                .arg(data.area());
        QString insert = QString("INSERT INTO %1 VALUES (%2)").arg(kTableStaticElectricData).arg(values);
        QSqlQuery query(database());
        if (!query.exec(insert)) {
            qWarning() << Q_FUNC_INFO << "Unable to insert static electric data:" << query.lastError();
            close();
            return false;
        }
    }

    close();
    return true;
}

StaticElectricData StaticElectricDB::staticElectricData()
{
    if (!open()) {
        qWarning() << Q_FUNC_INFO << "Unable to open database." << database().lastError();
        close();
        return StaticElectricData();
    }

    QSqlQuery query = d_ptr->staticElectricDataQuery();
    if (query.isValid()) {
        StaticElectricData prices = d_ptr->staticElectricDataFromRecord(query.record());
        close();
        return prices;
    } else {
        close();
        return StaticElectricData();
    }
}

//
// Private implementation
//

QSqlQuery StaticElectricDBP::staticElectricDataQuery()
{
    QString select = QString("SELECT * FROM %1;").arg(kTableStaticElectricData);

    QSqlQuery query(parent->database());
    if (!query.exec(select)) {
        qWarning() << Q_FUNC_INFO << "Unable to execute query:" << query.lastError();
    }

    if (!query.isValid()) {
        query.next();
    }

    return query;
}

StaticElectricData StaticElectricDBP::staticElectricDataFromRecord(const QSqlRecord& record) const
{
    return StaticElectricData(record.value(kFieldStaticElectricDataEnergyTax).toString(),
                              record.value(kFieldStaticElectricDataTransfer).toString(),
                              record.value(kFieldStaticElectricDataTransferTax).toString(),
                              record.value(kFieldStaticElectricDataPremium).toString(),
                              record.value(kFieldStaticElectricDataPremiumTax).toString(),
                              record.value(kFieldStaticElectricDataSwitchDevice).toString(),
                              record.value(kFieldStaticElectricDataSwitchValue).toString(),
                              record.value(kFieldStaticElectricDataPowerDevice).toString(),
                              record.value(kFieldStaticElectricDataPowerValue).toString(),
                              record.value(kFieldStaticElectricDataArea).toString());
}

bool StaticElectricDBP::createStaticElectricDataTable()
{
    qDebug() << Q_FUNC_INFO;

    QSqlQuery query(parent->database());
    if (!query.prepare("CREATE TABLE " kTableStaticElectricData "("
                       kFieldStaticElectricDataEnergyTax " TEXT NOT NULL, "
                       kFieldStaticElectricDataTransfer " TEXT NOT NULL, "
                       kFieldStaticElectricDataTransferTax " TEXT NOT NULL, "
                       kFieldStaticElectricDataPremium " TEXT NOT NULL, "
                       kFieldStaticElectricDataPremiumTax " TEXT NOT NULL, "
                       kFieldStaticElectricDataSwitchDevice " TEXT NOT NULL, "
                       kFieldStaticElectricDataSwitchValue " TEXT NOT NULL, "
                       kFieldStaticElectricDataPowerDevice " TEXT NOT NULL, "
                       kFieldStaticElectricDataPowerValue " TEXT NOT NULL, "
                       kFieldStaticElectricDataArea " TEXT NOT NULL);")) {
        qWarning() << "Unable to prepare query for " kTableStaticElectricData " table creation:" << query.lastError();
        return false;
    }

    if (!query.exec()) {
        qWarning() << "Unable to create table:" << kTableStaticElectricData << query.lastError();
        return false;
    }

    // Insert some default values to begin with
    StaticElectricData e("24", "6.1", "0", "0.3", "0", "Electric", "Switch", "Monitor", "Power", "FI");
    return parent->setStaticElectricData(e);
}
