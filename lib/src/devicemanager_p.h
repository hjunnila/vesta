#pragma once

class NodeValue;
class DeviceNode;
class ZWEvent;
class DeviceManagerP : public QObject
{
    Q_OBJECT
    Q_DISABLE_COPY(DeviceManagerP)

public:
    DeviceManagerP(QObject* parent);
    ~DeviceManagerP();

    // Properties
    QString device;
    QString configPath;

    bool running;
    uint32 home;
    uint8 node;

    QMap <uint8, DeviceNode*> devices;

public:
    // Methods
    bool event(QEvent* event);
    void onNotification(ZWEvent* event);

public slots:
    void onDeviceNodeValueChanged(DeviceNode* node, NodeValue* value);

signals:
    void ready();
};
