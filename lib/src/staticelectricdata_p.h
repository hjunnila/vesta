#pragma once

class StaticElectricDataP
{
public:
    QString energyTax;
    QString transfer;
    QString transferTax;
    QString premium;
    QString premiumTax;

    QString switchDevice;
    QString switchValue;

    QString powerDevice;
    QString powerValue;

    QString area;
};
