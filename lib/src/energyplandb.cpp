#include "energyplanentry.h"
#include "energyplandb_p.h"
#include "energyplandb.h"

EnergyPlanDB::EnergyPlanDB(const DatabaseInfo& info)
    : Database(info)
    , d_ptr(new EnergyPlanDBP)
{
    d_ptr->parent = this;
}

EnergyPlanDB::~EnergyPlanDB()
{
    d_ptr->parent = NULL;

    delete d_ptr;
    d_ptr = NULL;
}

QStringList EnergyPlanDB::essentialTables() const
{
    return QStringList() << kTableEnergyPlan;
}

bool EnergyPlanDB::createTable(const QString& tableName)
{
    qDebug() << Q_FUNC_INFO << tableName;

    if (tableName == kTableEnergyPlan) {
        return createEnergyPlanTable();
    } else {
        return false;
    }
}

bool EnergyPlanDB::createEnergyPlanTable()
{
    QSqlQuery query(database());
    if (!query.prepare("CREATE TABLE " kTableEnergyPlan "("
                       kFieldEnergyPlanStart " DATETIME NOT NULL,"
                       kFieldEnergyPlanEnd " DATETIME NOT NULL,"
                       kFieldEnergyPlanOil " DOUBLE NOT NULL,"
                       kFieldEnergyPlanEnergy " DOUBLE,"
                       kFieldEnergyPlanTransfer " DOUBLE,"
                       kFieldEnergyPlanPremium " DOUBLE);")) {
        qWarning() << "Unable to prepare query for " kTableEnergyPlan " table creation:" << query.lastError();
        return false;
    }

    if (!query.exec()) {
        qWarning() << "Unable to create table:" << kTableEnergyPlan << query.lastError();
        return false;
    }

    return true;
}

QList <EnergyPlanEntry> EnergyPlanDB::energyPlanEntriesForDate(const QDate& date)
{
    qDebug() << Q_FUNC_INFO << date;

    QList <EnergyPlanEntry> entries;
    if (!open()) {
        qWarning() << Q_FUNC_INFO << "Unable to open energy plan database connection:" << database().lastError();
    } else {
        QSqlQuery query = d_ptr->energyPlanQueryForDate(date);
        if (query.first()) {
            do {
                EnergyPlanEntry entry = EnergyPlanDBP::energyPlanEntryFromRecord(query.record());
                if (entry.isValid()) {
                    entries << entry;
                }
            } while (query.next());
        }
    }

    close();
    return entries;
}

EnergyPlanEntry EnergyPlanDB::energyPlanEntryForDate(const QDateTime& date)
{
    EnergyPlanEntry entry;

    if (!open()) {
        qWarning() << Q_FUNC_INFO << "Unable to open energy plan database connection:" << database().lastError();
    } else {
        QSqlRecord rec = d_ptr->energyPlanRecordForDate(date);
        if (!rec.isEmpty()) {
            entry = EnergyPlanDBP::energyPlanEntryFromRecord(rec);
        }
    }

    close();
    return entry;
}

bool EnergyPlanDB::storeEnergyPlanEntries(const QList <EnergyPlanEntry>& planEntries)
{
    bool result = true;

    if (!open()) {
        qWarning() << Q_FUNC_INFO << "Unable to open energy plan database connection:" << database().lastError();
        result = false;
    } else {
        for (QList <EnergyPlanEntry>::const_iterator it = planEntries.begin(); it != planEntries.end(); ++it) {
            if (!d_ptr->storeEnergyPlanEntry(*it)) {
                result = false;
                break;
            }
        }
    }

    close();
    return result;
}

//
// Private implementation
//

bool EnergyPlanDBP::storeEnergyPlanEntry(const EnergyPlanEntry& entry)
{
    if (!entry.isValid()) {
        qWarning() << "Not inserting an invalid entry!";
        return false;
    }

    bool result = false;

    QSqlRecord existingRecord = energyPlanRecordForDate(entry.start());
    if (existingRecord.isEmpty()) {
        QString values = QString("'%1', '%2', '%3', '%4', '%5', '%6'")
                                .arg(entry.start().toString(Qt::ISODate))
                                .arg(entry.end().toString(Qt::ISODate))
                                .arg(entry.oil())
                                .arg(entry.energy())
                                .arg(entry.transfer())
                                .arg(entry.premium());
        QString insert = QString("INSERT INTO %1 VALUES (%2)").arg(kTableEnergyPlan).arg(values);

        QSqlQuery query(parent->database());
        if (!query.exec(insert)) {
            qWarning() << Q_FUNC_INFO << "Unable to insert energy plan data:" << query.lastError();
            result = false;
        } else {
            result = true;
        }
    } else {
        // No need to insert a new plan over an existing one.
        result = true;
    }

    return result;
}

QSqlQuery EnergyPlanDBP::energyPlanQueryForDate(const QDate& date)
{
    QDateTime start(date);
    start.setTime(QTime(0, 0, 0));
    QDateTime end(date.addDays(1));
    end.setTime(QTime(0, 0, 0));

    QSqlQuery query(parent->database());
    QString where = QString("WHERE (%1 >= '%2' AND %3 <= '%4')").arg(kFieldEnergyPlanStart)
                                                                .arg(start.toString(Qt::ISODate))
                                                                .arg(kFieldEnergyPlanEnd)
                                                                .arg(end.toString(Qt::ISODate));
    QString sql = QString("SELECT * FROM %1 %2;").arg(kTableEnergyPlan).arg(where);
    if (!query.exec(sql)) {
        qWarning() << "Unable to execute query:" << query.lastError();
    }

    return query;
}

QSqlRecord EnergyPlanDBP::energyPlanRecordForDate(const QDateTime& date)
{
    QSqlRecord record;

    QSqlQuery query(parent->database());
    QString where = QString("WHERE (%1 <= '%2' AND %3 > '%2')").arg(kFieldEnergyPlanStart)
                                                               .arg(date.toString(Qt::ISODate))
                                                               .arg(kFieldEnergyPlanEnd);
    QString sql = QString("SELECT * FROM %1 %2;").arg(kTableEnergyPlan).arg(where);
    if (!query.exec(sql)) {
        qWarning() << "Unable to execute query:" << query.lastError();
    }

    if (query.first()) {
        record = query.record();
    }

    return record;
}

EnergyPlanEntry EnergyPlanDBP::energyPlanEntryFromRecord(const QSqlRecord& record)
{
    QDateTime start;
    QSqlField field = record.field(kFieldEnergyPlanStart);
    if (field.isValid()) {
        start = field.value().toDateTime();
    }

    QDateTime end;
    field = record.field(kFieldEnergyPlanEnd);
    if (field.isValid()) {
        end = field.value().toDateTime();
    }

    double oil = -1;
    field = record.field(kFieldEnergyPlanOil);
    if (field.isValid()) {
        oil = field.value().toDouble();
    }

    double energy = -1;
    field = record.field(kFieldEnergyPlanEnergy);
    if (field.isValid()) {
        energy = field.value().toDouble();
    }

    double transfer = -1;
    field = record.field(kFieldEnergyPlanTransfer);
    if (field.isValid()) {
        transfer = field.value().toDouble();
    }

    double premium = -1;
    field = record.field(kFieldEnergyPlanPremium);
    if (field.isValid()) {
        premium = field.value().toDouble();
    }

    if (start.isValid() && end.isValid() && oil >= 0 && energy >= 0 && transfer >= 0 && premium >= 0) {
        return EnergyPlanEntry(start, end, oil, energy, transfer, premium);
    } else {
        qWarning() << Q_FUNC_INFO << "Missing data from energy plan entry." << record;
        return EnergyPlanEntry();
    }
}
