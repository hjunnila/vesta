#pragma once

class NordPoolP
{
public:
    /** The instance that does the actual network accessing. */
    QNetworkAccessManager* manager;

    /** Specifies the URL to poll for price data updates */
    QString url;

    /** Specifies the area (FI, SE, NO...) whose price data to parse. */
    QString area;

    /** Start polling for network after this time daily (default is 15:00), until prices have been successfully fetched. */
    QTime dailyPollTime;

    /** Currently-running network request. NULL if no request is currently running. */
    QNetworkReply* currentReply;

    /**
     * Timer that periodically checks if:
     * 1. The latest price data has been fetched.
     * 2. If not, whether it's time to fetch the data.
     */
    QTimer* pollTimer;

    /** A timer that fires after a pre-set time to cancel the current network request if data has not been fetched by then. */
    QTimer* replyTimeoutTimer;

    /** Date&time when the last successful price update was performed. */
    QDateTime lastSuccessfulUpdate;

    /** Because NordPool announces prices in stockholm time, all dates must be converted from stockholm time to local time using this object */
    QTimeZone stockholm;
};
