include(../../coverage.pri)

TEMPLATE         = lib
TARGET           = vesta
QT              += network sql dbus
QT              -= gui
CONFIG          += precompile_header debug
CONFIG          -= app_bundle

PRECOMPILED_HEADER = vesta-pc.h
OBJECTS_DIR     = obj

# Linux open-zwave location. Adjust if necessary.
INCLUDEPATH     += /usr/include/openzwave /usr/include/openzwave/command_classes

# Homebrew open-zwave location. Adjust if necessary.
macx:LIBS           += -L/usr/local/Cellar/open-zwave/1.2.919/lib
macx:INCLUDEPATH    += /usr/local/Cellar/open-zwave/1.2.919/include/openzwave
macx:INCLUDEPATH    += /usr/local/Cellar/open-zwave/1.2.919/include/openzwave/command_classes

LIBS            += -lopenzwave
LIBS            += -lstdc++

# Comment this line if you get whining about C++11
DEFINES += _GLIBCXX_USE_CXX11_ABI=0

# Private headers that contain a QObject-based class
PRIVATE_HEADERS += energymanager_p.h devicemanager_p.h devicenode_p.h server_p.h nodevalue_p.h

PUBLIC_HEADERS +=   \
                    database.h \
                    devicemanager.h \
                    devicemanagerservice.h \
                    devicenode.h \
                    fileservice.h \
                    electric.h \
                    elpricedb.h \
                    energymanager.h \
                    energymanagerservice.h \
                    energyplandb.h \
                    energyplanentry.h \
                    energysource.h \
                    energyusageentry.h \
                    energyusagedb.h \
                    macros.h \
                    nodevalue.h \
                    nordpool.h \
                    oil.h \
                    pricedata.h \
                    pricedataentry.h \
                    response.h \
                    request.h \
                    server.h \
                    service.h \
                    settings.h \
                    staticelectricdata.h \
                    staticelectricdb.h \
                    staticoildata.h \
                    staticoildatadb.h \
                    zwevent.h

SOURCES += \
            database.cpp \
            devicemanager.cpp \
            devicemanagerservice.cpp \
            devicenode.cpp \
            electric.cpp \
            elpricedb.cpp \
            energymanager.cpp \
            energymanagerservice.cpp \
            energyplandb.cpp \
            energyplanentry.cpp \
            energysource.cpp \
            energyusagedb.cpp \
            energyusageentry.cpp \
            fileservice.cpp \
            nodevalue.cpp \
            nordpool.cpp \
            oil.cpp \
            pricedata.cpp \
            pricedataentry.cpp \
            response.cpp \
            request.cpp \
            server.cpp \
            service.cpp \
            settings.cpp \
            staticelectricdata.cpp \
            staticelectricdb.cpp \
            staticoildata.cpp \
            staticoildatadb.cpp \
            zwevent.cpp

HEADERS += $$PUBLIC_HEADERS $$PRIVATE_HEADERS

DBUS_ADAPTORS   += org.vesta.elpricedb.xml
DBUS_INTERFACES += org.vesta.elpricedb.xml

target.path = /usr/lib
INSTALLS += target

headers.path = /usr/include/vesta
headers.files = $$PUBLIC_HEADERS
INSTALLS += headers
