#include "energymanagerservice_p.h"
#include "energymanagerservice.h"
#include "staticelectricdata.h"
#include "energyplanentry.h"
#include "energymanager.h"
#include "staticoildata.h"
#include "response.h"
#include "electric.h"
#include "request.h"
#include "server.h"
#include "http.h"
#include "oil.h"

#define TYPE        "type"
#define ELECTRIC    "electric"
#define OIL         "oil"

#define FORMAT      "format"
#define CANVASJS    "canvasjs"
#define Y_AXIS      "y"
#define LABEL       "label"

#define DATE        "date"
#define START       "start"
#define END         "end"

#define ENERGY      "energy"
#define ENERGYTAX   "energyTax"
#define TRANSFER    "transfer"
#define TRANSFERTAX "transferTax"
#define PREMIUM     "premium"
#define PREMIUMTAX  "premiumTax"

#define LITREPRICE  "litrePrice"
#define EFFICIENCY  "efficiency"
#define KWHPERLITRE "kWhPerLitre"

#define SWITCHDEVICE "switchDevice"
#define SWITCHVALUE "switchValue"
#define POWERDEVICE "powerDevice"
#define POWERVALUE  "powerValue"
#define AREA        "area"

#define MANUAL_ELECTRIC_OVERRIDE "manualElectricOverride"

typedef enum DataFormat {
    DataFormatPlain,
    DataFormatCanvasJS
} DataFormat;

EnergyManagerService::EnergyManagerService(EnergyManager* energyManager)
    : Service(energyManager)
    , d_ptr(new EnergyManagerServiceP)
{
    Q_ASSERT(energyManager);
    d_ptr->energyManager = energyManager;

    const QRegExp rxGetEnergyPlan("/energyPlan");
    addService(HTTP_GET, rxGetEnergyPlan, "getEnergyPlan");

    const QRegExp rxStaticData("/static_data");
    addService(HTTP_GET, rxStaticData, "getStaticData");
    addService(HTTP_POST, rxStaticData, "postStaticData");

    const QRegExp rxManualElectricPlan("/manualElectricPlan");
    addService(HTTP_GET, rxManualElectricPlan, "getManualElectricPlan");
    addService(HTTP_POST, rxManualElectricPlan, "postManualElectricPlan");
}

EnergyManagerService::~EnergyManagerService()
{
    delete d_ptr;
    d_ptr = NULL;
}

Response EnergyManagerService::getEnergyPlan(const Request& request)
{
    qDebug() << Q_FUNC_INFO;

    Response response = Response::notFound();

    QString dateString = request.formData()[DATE];
    DataFormat format;
    if (request.formData()[FORMAT] == CANVASJS) {
        format = DataFormatCanvasJS;
    } else {
        format = DataFormatPlain;
    }

    QDate date = QDate::fromString(dateString, Qt::ISODate);
    if (date.isValid()) {
        QList <EnergyPlanEntry> entries(d_ptr->energyManager->energyPlanEntriesForDate(date));

        if (format == DataFormatCanvasJS) {
            QDateTime start(date);
            start.setTime(QTime(0, 0, 0));
            QDateTime end(date.addDays(1));
            end.setTime(QTime(0, 0, 0));

            QJsonArray energyArray;
            QJsonArray transferArray;
            QJsonArray premiumArray;
            QJsonArray oilArray;

            for (QList <EnergyPlanEntry>::const_iterator it = entries.begin(); it != entries.end(); ++it) {
                EnergyPlanEntry entry = *it;

                const QString hh = QString("%1:00").arg(entry.start().time().hour(), 2, 10, QChar('0'));

                QJsonObject energy;
                energy[Y_AXIS] = entry.energy();
                energy[LABEL] = hh;
                energyArray.append(energy);

                QJsonObject transfer;
                transfer[Y_AXIS] = entry.transfer();
                transfer[LABEL] = hh;
                transferArray.append(transfer);

                QJsonObject premium;
                premium[Y_AXIS] = entry.premium();
                premium[LABEL] = hh;
                premiumArray.append(premium);

                QJsonObject oil;
                oil[Y_AXIS] = entry.oil();
                oil[LABEL] = hh;
                oilArray.append(oil);
            }

            QJsonObject root;
            root[START] = start.toString(Qt::RFC2822Date);
            root[END] = end.toString(Qt::RFC2822Date);
            root[ENERGY] = energyArray;
            root[TRANSFER] = transferArray;
            root[PREMIUM] = premiumArray;
            root[OIL] = oilArray;

            QJsonDocument doc(root);
            response.setJsonBody(doc);
        } else {
            QJsonArray array;
            for (QList <EnergyPlanEntry>::const_iterator it = entries.begin(); it != entries.end(); ++it) {
                QJsonObject object = d_ptr->energyPlanEntryAsJson(*it);
                array.append(object);
            }

            QJsonDocument doc(array);
            response.setJsonBody(doc);
        }

        response.setStatus(200);
    }

    return response;
}

Response EnergyManagerService::getStaticData(const Request& request)
{
    Response response;

    QString typeString = request.formData()[TYPE];
    if (typeString == ELECTRIC) {
        QJsonObject object = d_ptr->staticElectricDataAsJson();
        QJsonDocument doc(object);
        response.setJsonBody(doc);
        response.setStatus(200);
    } else if (typeString == OIL) {
        QJsonObject object = d_ptr->staticOilDataAsJson();
        QJsonDocument doc(object);
        response.setJsonBody(doc);
        response.setStatus(200);
    } else {
        return Response::notFound();
    }

    return response;
}

Response EnergyManagerService::postStaticData(const Request& request)
{
    Response response;

    qDebug() << Q_FUNC_INFO << request.body();

    QJsonParseError error;
    QJsonDocument doc = QJsonDocument::fromJson(request.body().toUtf8(), &error);
    if (error.error != QJsonParseError::NoError) {
        qDebug() << error.errorString();
        Response resp;
        resp.setStatus(400);
        return resp;
    }

    QJsonObject root = doc.object();
    if (root[ELECTRIC].isObject()) {
        if (d_ptr->setStaticElectricDataAsJson(root[ELECTRIC].toObject())) {
            response.setStatus(200);
        } else {
            response.setStatus(400);
        }
    } else if (root[OIL].isObject()) {
        if (d_ptr->setStaticOilDataAsJson(root[OIL].toObject())) {
            response.setStatus(200);
        } else {
            response.setStatus(400);
        }
    }

    return response;
}

Response EnergyManagerService::getManualElectricPlan(const Request& request)
{
    qDebug() << Q_FUNC_INFO;

    Q_UNUSED(request);

    QJsonObject object;
    object[MANUAL_ELECTRIC_OVERRIDE] = d_ptr->energyManager->isManualElectricOverride();
    QJsonDocument doc(object);

    Response response;
    response.setJsonBody(doc);
    response.setStatus(200);

    return response;
}

Response EnergyManagerService::postManualElectricPlan(const Request& request)
{
    qDebug() << Q_FUNC_INFO << request.body();

    Response response;

    QJsonParseError error;
    QJsonDocument doc = QJsonDocument::fromJson(request.body().toUtf8(), &error);
    if (error.error != QJsonParseError::NoError) {
        qDebug() << error.errorString();

        response.setStatus(400);
        return response;
    }

    QJsonObject root = doc.object();
    QJsonValue val = root[MANUAL_ELECTRIC_OVERRIDE];
    if (val.isBool()) {
        bool enable = val.toBool();
        d_ptr->energyManager->setManualElectricOverride(enable);
        response.setStatus(200);
    } else {
        qWarning() << "Malformed input:" << root;
        response.setStatus(400);
    }

    return response;
}

//
// Private implementation
//

QJsonObject EnergyManagerServiceP::energyPlanEntryAsJson(const EnergyPlanEntry& entry) const
{
    QJsonObject object;
    object[START] = entry.start().toString(Qt::RFC2822Date);
    object[END] = entry.end().toString(Qt::RFC2822Date);
    object[ENERGY] = entry.energy();
    object[TRANSFER] = entry.transfer();
    object[PREMIUM] = entry.premium();
    object[OIL] = entry.oil();
    return object;
}

QJsonObject EnergyManagerServiceP::staticElectricDataAsJson()
{
    Electric* electric = energyManager->electric();
    Q_ASSERT(electric);

    QJsonObject object;
    if (electric) {
        StaticElectricData data = electric->staticElectricData();
        object[ENERGYTAX] = data.energyTax();
        object[TRANSFER] = data.transfer();
        object[TRANSFERTAX] = data.transferTax();
        object[PREMIUM] = data.premium();
        object[PREMIUMTAX] = data.premiumTax();
        object[SWITCHDEVICE] = data.switchDevice();
        object[SWITCHVALUE] = data.switchValue();
        object[POWERDEVICE] = data.powerDevice();
        object[POWERVALUE] = data.powerValue();
        object[AREA] = data.area();
    }

    return object;
}

bool EnergyManagerServiceP::setStaticElectricDataAsJson(const QJsonObject& object)
{
    Electric* electric = energyManager->electric();
    Q_ASSERT(electric);

    QJsonValue energyTax = object[ENERGYTAX];
    QJsonValue transfer = object[TRANSFER];
    QJsonValue transferTax = object[TRANSFERTAX];
    QJsonValue premium = object[PREMIUM];
    QJsonValue premiumTax = object[PREMIUMTAX];
    QJsonValue switchDevice = object[SWITCHDEVICE];
    QJsonValue switchValue = object[SWITCHVALUE];
    QJsonValue powerDevice = object[POWERDEVICE];
    QJsonValue powerValue = object[POWERVALUE];
    QJsonValue area = object[AREA];

    if (electric &&
        energyTax.isString() && energyTax.toVariant().canConvert(QMetaType::Double) &&
        transfer.isString() && transfer.toVariant().canConvert(QMetaType::Double) &&
        transferTax.isString() && transferTax.toVariant().canConvert(QMetaType::Double) &&
        premium.isString() && premium.toVariant().canConvert(QMetaType::Double) &&
        premiumTax.isString() && premiumTax.toVariant().canConvert(QMetaType::Double) &&
        switchDevice.isString() && switchValue.isString() && powerDevice.isString() &&
        powerValue.isString() && area.isString()) {
        StaticElectricData data(energyTax.toString(), transfer.toString(),
                                transferTax.toString(), premium.toString(),
                                premiumTax.toString(),
                                switchDevice.toString(), switchValue.toString(),
                                powerDevice.toString(), powerValue.toString(),
                                area.toString());
        electric->setStaticElectricData(data);
        return true;
    } else {
        return false;
    }
}

QJsonObject EnergyManagerServiceP::staticOilDataAsJson()
{
    Oil* oil = energyManager->oil();
    Q_ASSERT(oil);

    QJsonObject object;
    if (oil) {
        StaticOilData data = oil->staticOilData();
        object[LITREPRICE] = data.litrePrice();
        object[EFFICIENCY] = data.efficiency();
        object[KWHPERLITRE] = data.kWhPerLitre();
        object[SWITCHDEVICE] = data.switchDevice();
        object[SWITCHVALUE] = data.switchValue();
        object[POWERDEVICE] = data.powerDevice();
        object[POWERVALUE] = data.powerValue();
    }

    return object;
}

bool EnergyManagerServiceP::setStaticOilDataAsJson(const QJsonObject& object)
{
    Oil* oil = energyManager->oil();
    Q_ASSERT(oil);

    QJsonValue litrePrice = object[LITREPRICE];
    QJsonValue efficiency = object[EFFICIENCY];
    QJsonValue kWhPerLitre = object[KWHPERLITRE];
    QJsonValue switchDevice = object[SWITCHDEVICE];
    QJsonValue switchValue = object[SWITCHVALUE];
    QJsonValue powerDevice = object[POWERDEVICE];
    QJsonValue powerValue = object[POWERVALUE];

    if (oil &&
        litrePrice.isString() && litrePrice.toVariant().canConvert(QMetaType::Double) &&
        efficiency.isString() && efficiency.toVariant().canConvert(QMetaType::Double) &&
        kWhPerLitre.isString() && kWhPerLitre.toVariant().canConvert(QMetaType::Double) &&
        switchDevice.isString() && switchValue.isString() &&
        powerDevice.isString() && powerValue.isString() ) {
        StaticOilData data(litrePrice.toString(), efficiency.toString(), kWhPerLitre.toString(),
                           switchDevice.toString(), switchValue.toString(),
                           powerDevice.toString(), powerValue.toString());
        oil->setStaticOilData(data);
        return true;
    } else {
        return false;
    }
}
