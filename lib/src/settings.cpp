#include "settings.h"
#include "settings_p.h"

#define DEFAULT_CONFIG_PATH "/etc/vesta"
#define VESTA_CONFIG_ENV    "VESTA_CONFIG"

Settings::Settings(const QString& fileName)
    : d_ptr(new SettingsP)
{
    d_ptr->fileName = fileName;
}

Settings::~Settings()
{
    delete d_ptr;
    d_ptr = NULL;
}

Settings::Settings(const Settings& settings)
    : d_ptr(new SettingsP)
{
    d_ptr->fileName = settings.d_ptr->fileName;
    d_ptr->doc = settings.d_ptr->doc;
}

Settings& Settings::operator=(const Settings& settings)
{
    if (this != &settings) {
        d_ptr->fileName = settings.d_ptr->fileName;
        d_ptr->doc = settings.d_ptr->doc;
    }

    return *this;
}

bool Settings::load()
{
    QString confPath(getenv(VESTA_CONFIG_ENV));
    if (confPath.isEmpty()) {
        confPath = QString(DEFAULT_CONFIG_PATH);
    }

    if (!confPath.endsWith("/")) {
        confPath += "/";
    }

    QFile file(confPath + d_ptr->fileName);
    if (file.exists() && file.open(QIODevice::ReadOnly)) {
        QJsonParseError error;
        d_ptr->doc = QJsonDocument::fromJson(file.readAll(), &error);
        file.close();

        if (error.error != QJsonParseError::NoError) {
            qWarning() << d_ptr->fileName << "JSON parse error:" << error.errorString();
            return false;
        }

        return true;
    } else {
        qWarning() << "Settings file" << file.fileName() << "is not readable";
        return false;
    }

}

QVariant Settings::valueForKeyList(const QStringList& keyList) const
{
    if (d_ptr->doc.isNull() || !d_ptr->doc.isObject()) {
        qWarning() << "Settings from" << d_ptr->fileName << "have not been loaded.";
        return QVariant();
    }

    QJsonObject object = d_ptr->doc.object();
    for (int i = 0; i < keyList.count(); i++) {
        if (object.contains(keyList[i])) {
            QJsonValue value = object[keyList[i]];
            if (keyList.count() == i+1) {
                return value.toVariant();
            } else if (value.isObject()) {
                object = value.toObject();
                continue;
            } else {
                qWarning() << "No settings key found with keylist:" << keyList << "from" << d_ptr->fileName;
                return QVariant();
            }
        } else {
            qWarning() << "No settings key found with keylist:" << keyList << "from" << d_ptr->fileName;
            return QVariant();
        }
    }

    return QVariant();
}
