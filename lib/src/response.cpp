#include "response_p.h"
#include "response.h"
#include "http.h"

Response::Response()
    : d_ptr(new ResponseP)
{
    d_ptr->status = 404;
    d_ptr->updateContentLength();
}

Response::Response(const Response& response)
    : d_ptr(new ResponseP)
{
    d_ptr->status = response.status();
    d_ptr->body = response.body();
    d_ptr->headers = response.headers();
}

Response::~Response()
{
    delete d_ptr;
    d_ptr = NULL;
}

Response& Response::operator=(const Response& response)
{
    if (this != &response) {
        d_ptr->status = response.status();
        d_ptr->body = response.body();
        d_ptr->headers = response.headers();
    }

    return *this;
}

void Response::setStatus(int status)
{
    d_ptr->status = status;
}

int Response::status() const
{
    return d_ptr->status;
}

void Response::setBody(const QString& body)
{
    d_ptr->body = body;
    d_ptr->updateContentLength();
}

QString Response::body() const
{
    return d_ptr->body;
}

void Response::setJsonBody(const QJsonDocument& doc)
{
    QByteArray ba(doc.toJson(QJsonDocument::Compact));
    setBody(ba);
    setHeader(HTTP_HEADER_CONTENT_TYPE, HTTP_APPLICATION_JSON);
}

void Response::setHeader(const QString& key, const QString& value)
{
    d_ptr->headers[key] = value;
}

QMap<QString,QString> Response::headers() const
{
    return d_ptr->headers;
}

Response Response::notFound()
{
    Response resp;
    resp.setStatus(404);
    return resp;
}

Response Response::badRequest()
{
    Response resp;
    resp.setStatus(400);
    return resp;
}

QByteArray Response::responseData() const
{
    QString buf;
    buf.append(QString("%1 %2 %3%4").arg(HTTP_PROTOCOL_11).arg(status()).arg(d_ptr->statusText()).arg(HTTP_CRLF));
    buf.append(d_ptr->headerData());
    buf.append(HTTP_CRLF);
    if (body().length()) {
        buf.append(body());
    }
    return buf.toUtf8();
}

void Response::setError(int status, const QString& error, const QString& errorDescription)
{
    setStatus(status);
    QJsonObject root;
    root[RESPONSE_ERROR] = QJsonValue(error);
    root[RESPONSE_ERROR_DESCRIPTION] = QJsonValue(errorDescription);
    QJsonDocument doc(root);
    setJsonBody(doc);
}

//
// Private implementation
//

void ResponseP::updateContentLength()
{
    headers[HTTP_HEADER_CONTENT_LENGTH] = QString::number(body.length());
}

QString ResponseP::statusText() const
{
    QString text;
    switch (status) {
        case 200:
            text = "OK";
            break;
        case 204:
            text = "No Content";
            break;
        case 400:
            text = "Bad Request";
            break;
        case 404:
            text = "Not Found";
            break;
        case 500:
        default:
            text = "Internal Server Error";
            break;
    }

    return text;
}

QString ResponseP::headerData() const
{
    QString buf;

    QMap<QString,QString>::const_iterator it(headers.constBegin());
    while (it != headers.constEnd()) {
        buf.append(QString("%1: %2%3").arg(it.key()).arg(it.value()).arg(HTTP_CRLF));
        ++it;
    }

    return buf;
}
